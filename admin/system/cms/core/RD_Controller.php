<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Code here is run before admin controllers
class RD_Controller extends Admin_Controller
{
	//public $module_name;
	/**
	 * Validation rules for creating a new client
	 *
	 * @var array
	 * @access protected
	 */
	protected $validation_rules = array();
	protected $_uri_n = 4;
	public $numrows = 10;
	public $_returnUrl = '';
	public $title = '';
	public $single = '';

	public function __construct($module_name, $title=NULL, $single=NULL)
	{
		parent::__construct();

		$this->title = $title;
		$this->single = $single;

		// Load all the required classes

		$this->module_name = $module_name;

		if(!defined('MODULE_NAME')) define('MODULE_NAME', $module_name);

		$this->data = new stdClass();
		$this->data->module_name = $this->module_name;
		$this->data->_module_title = is_null($title) ? $module_name : $this->title;
		$this->data->_module_title_single = is_null($single) ? $module_name : $this->single;

		$this->load->model($this->module_name.'_m');

		$this->template
				->append_css('rd/application.css')
				->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				->set_layout('default', 'rd')
				->set('title', $this->title)
				->set('single', $this->single)

				;
		$this->validation_rules = $this->{$this->module_name . '_m'}->get_Validation_Rules();

		/*$this->load->helper(array('html','form','rdactions','text'));
		$this->load->library(array('form_validation','session'));
		$this->template->set_partial('shortcuts', 'rd/partials/shortcuts');

		if($this->_is_ajax())
		{
			$this->template->set_layout(FALSE);
		}*/
	}

	/**
	 * List all existing client
	 *
	 * @author Actualsales
	 * @access public
	 * @return void
	 */
	public function index()
	{
		//no access
		$this->lista();
	}

	/**
	 * List all existing client
	 *
	 * @author Actualsales
	 * @access public
	 * @return void
	 */
	public function lista($btnAdd=true)
	{
		$params = $this->uri->uri_to_assoc( $this->_uri_n );

		if($this->session->userdata('return_url'))
		{
			$this->session->unset_userdata('return_url');
		}

		$filter_key = isset($params['filter']) ? $params['filter'] : NULL;
		$offset = isset($params['offset']) ? $params['offset'] : 0;
		$offset = ((int)$offset > 0 ? $offset : 1);
		$pag_index = isset($params['offset']) ? $this->uri->total_segments() : $this->uri->total_segments()+1;

		$this->load->helper('pagination');

		$m = $this->{$this->module_name.'_m'};

		$filter = $this->_get_filter($filter_key);
		$filter_uri = $filter ? "/filter/{$filter_key}" : '';

		$res = $m->get_all($this->numrows, $offset * $this->numrows - $this->numrows, $filter);

		$count = $m->count_all($filter);

		#$this->data->nrecords = $count;

		if($count){
			//on delete fix
			//if($count<=($offset)){
			//	$offset -= $this->numrows;
			//	redirect('admin/'.$this->module_name.'/lista'.$filter_uri.'/offset/'.$offset);
			//}

			$this->data->pagination = create_pagination('admin/'.$this->module_name.'/lista'.$filter_uri.'/offset/', $count, $this->numrows,$pag_index);
			$this->data->list =& $res;
		}

		$this->session->set_userdata('offset', $offset);
		$this->data->offset = $offset;

		$this->data->module_fields = $m->get_fields('r');
		$this->data->module_filters = $m->get_filters();
		$this->data->primaryCol = $m->get_primaryKey();
		$this->data->module_actions = $this->{$this->module_name.'_m'}->get_actions();
		$this->data->module_actions_common = $this->{$this->module_name.'_m'}->get_actions_common();
		$this->data->module_actions_bottom = $this->{$this->module_name.'_m'}->get_actions_bottom();


		$this->data->active_filters = $filter===FALSE ? array() : $filter;

		$this->data->filter_key = $filter_key;


		if($btnAdd) $this->template->add_action('add', $this->single);
		$this->template->build('rd/index', $this->data);
	}

	/*public function listatab()
	{
		$this->template->set_layout('tabs');
		$this->lista();
	}*/

	public function export($type='csv',$filter_key=null)
	{

		$m = $this->{$this->module_name.'_m'};
		$filter = $this->_get_filter($filter_key);

		switch($type){
			case "csv":
				$filename = $this->module_name.'_export.csv';
				header('Content-Type: application/csv');
				header('Content-Disposition: attachement; filename="' . $filename . '"');
				echo $m->export_csv($filter);
				exit();
				break;
			case "xls":
				$filename = $this->module_name.'_export.xls';
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachement; filename="' . $filename . '"');
				echo $m->export_xls($filter);
				exit();
				break;
			case "list":
				$filename = $this->module_name.'_export.xls';
				//header('Content-Type: application/vnd.ms-excel');
				//header('Content-Disposition: attachement; filename="' . $filename . '"');
				echo $m->export_xls($filter);
				exit();
				break;
		}
	}


	/**
	 * Create new
	 */
	public function create()
	{
		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		//prep data
		$this->data->module_fields = $this->{$this->module_name . '_m'}->get_fields('c');

		if($this->form_validation->run()){
			//insert data
			list($res, $res_extra) = $this->{$this->module_name . '_m'}->create();
			if($res){
				//success message
				$message = 'Entrada inserida com sucesso.';
				$status = 'success';
				$this->session->set_flashdata($status, $message);
				if(empty($this->_returnUrl)){
					if(isset($this->returnID)){
						return $this->db->insert_id();
					}else if(isset($this->goToEdit)){
						redirect('admin/'.$this->module_name.'/edit/'.$this->db->insert_id());
					}else{
						redirect('admin/'.$this->module_name);
					}
				}else{
					redirect('admin/'.$this->_returnUrl);
				}

			}
			else{
				$error = !is_null($res_extra) ? '<br/>'.$this->upload->display_errors() : '';

				//failure message
				$message= 'Erro ao tentar guardar.'. $error;
				$status = 'error';
				$this->session->set_flashdata($status, $message);
			}
			$this->template->build('rd/create', $this->data);
		}

		if($this->_is_ajax() && strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
			$errors = validation_errors();
			echo json_encode(array('status' => 'error','message' => $errors ));
		}
		else{
			// Load the view
			$this->template
					->add_action('back', $this->single)
					->build('rd/create', $this->data);
		}
	}


	/**
	 * Edit an existing item
	 *
	 * @author Actualsales
	 * @param id the ID to edit
	 * @access public
	 * @return void
	 */
	public function edit($id)
	{
		$module = new $this->{$this->module_name.'_m'}();
		$id_rule = array( 	'field' => $module->get_PrimaryKey(),
							'label' => 'Primary Col',
							'rules' => 'required|is_numeric|trim' );

		array_push($this->validation_rules, $id_rule);

		//get the product we want to edit
		$item = $module->get($id);
		$this->data->item =& $item;

		if($this->session->userdata('return_url')) $this->_returnUrl = $this->session->userdata('return_url');

		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		//prep data
		$this->data->module_fields = $this->{$this->module_name . '_m'}->get_fields('u');

		if($this->form_validation->run()){
			//insert data
			$data = $module->get_Input_Data('u');

			if($module->update($this->input->post('id'), $data, TRUE)){
				//success message
				$message = 'Entrada editada com sucesso.';
				$status = 'success';
				$this->session->set_flashdata($status, $message);
				if(empty($this->_returnUrl)){
					redirect('admin/'.$this->module_name);
				}else{
					if($this->session->userdata('return_url')){
						redirect($this->_returnUrl);
					}else{
						redirect('admin/'.$this->_returnUrl);
					}
				}
				//redirect($this->_get_last_page());
			}
			else{
				//failure message
				$message= 'Erro ao tentar editar.';
				$status = 'error';
				$this->session->set_flashdata($status, $message);
				$this->template->build('rd/edit', $this->data);
			}
		}else{
			if(isset($_SERVER['HTTP_REFERER']) && !$this->session->userdata('return_url')){
				$this->session->set_userdata('return_url', $_SERVER['HTTP_REFERER']);
				$this->_returnUrl = $_SERVER['HTTP_REFERER'];
			}
		}

		if($this->_is_ajax() && strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
			$errors = validation_errors();
			echo json_encode(array( 'status' => 'error', 'message' => $errors ));
		}
		else{
			// Load the view

			$this->template
					->add_action('back', $this->single)
					->build('rd/edit', $this->data);
		}
	}


	/**
	 * View an existing item
	 *
	 * @author Actualsales
	 * @param id the ID to edit
	 * @access public
	 * @return void
	 */
	public function view($id)
	{
		$module = new $this->{$this->module_name."_m"}();
		$id_rule = array(
				'field' => $module->get_PrimaryKey(),
				'label' => "Primary Col",
				'rules' => 'required|is_numeric|trim'
			);
		array_push($this->validation_rules, $id_rule);

		//get the product we want to edit
		$item = $module->get($id);
		$this->data->item =& $item;

		//prep data
		$this->data->module_fields = $this->{$this->module_name . "_m"}->get_fields("i");

		// Load the view
		$this->template
					->add_action('back', $this->single)
					->build('rd/view', $this->data);
	}



	/**
	 * Delete an single or many items
	 *
	 * @author Actualsales
	 * @access public
	 * @return void
	 */
	public function delete($id=null)
	{
		if(is_null($id)){
			$ids = $this->input->post('action_to');
		}else{
			$ids = array($id);
		}

		if(!empty($ids)){
			$i = 0;
			$count = count($ids);

			//loop through each id and try to delete
			foreach($ids as $id){
				//delete success
				if($this->{$this->module_name . '_m'}->delete($id)){
					$i++;
				}
			}
			if($i>0){
				$this->session->set_flashdata('success', sprintf('%1$d de %2$d produtos foram removidos com sucesso.', $i, $count));
			}
		}
		else{
			//oops no ids.. ids required here.  GTFO.
			$this->session->set_flashdata('notice', 'Tentou fazer uma acção sem ter seleccionado um item.');
		}
		//no need to keep hanging around here,  redirect back to faq list
		redirect($this->_get_last_page());
	}

	public function action($action, $id)
	{
		$this->{$this->module_name . '_m'}->{$action}($id);
		if(isset($_SERVER['HTTP_referer']) && $_SERVER['HTTP_referer'] != ''){
			redirect($_SERVER['HTTP_referer']);
		}else{
			redirect($this->_get_last_page());
		}
	}

	public function filter()
	{
		$filter_json = $this->input->post('fq');
		$filter = json_decode($filter_json);
		$filter_key = sha1($filter_json);
		$_SESSION['filter_'.$filter_key] = $filter;
		redirect('admin/'.$this->module_name.'/lista/filter/'.$filter_key);
	}

	public function ajaxaction($action, $id){
		$this->{$this->module_name . '_m'}->{$action}($id);
	}

	/* Aux functions */
	protected function _get_last_page()
	{
		if(false){
			//todo, session last url
		}elseif (isset($_SERVER['HTTP_REFERER'])){
		    return $_SERVER['HTTP_REFERER'];
		}else{
		    return 'admin/'.$this->module_name;
		}
	}

	// get filters if any
	protected function _get_filter($filter_key)
	{
		$filter = FALSE;
		if(!is_null($filter_key)){
			if(isset($_SESSION['filter_'.$filter_key])){
				$filter = $_SESSION['filter_'.$filter_key];
			}
		}

		return $filter;
	}

	public function thumb2(){
		//http://localhost/garcias/www/thumb?src=uploads/products/prod1.jpg&h=220&w=90&a=c&zc=2
		//http://localhost/garcias/www/admin/admin/products/thumb/?path=../uploads/products/6a013e3dd012dfad9a4df5cff509b511.png&width=100&height=100&crop=1


		$thumb = new timthumb();
		$thumb->start();
	}
	public function thumb()
	{
		$path = BASEPATH.'../../';

		$_file_path = '';
		$_width = 100;
		$_height = 100;
		$_crop = 0;

		if(isset($_GET) && isset($_GET['path'])){
			$_file_path = $_GET['path'];
			$_width = $_GET['width'];
			$_height = $_GET['height'];
			$_crop = $_GET['crop'];
			$file = '';
			$thumbfile = '';

			if($_file_path != ''){
				$file = $path . $_file_path;
				$thumbfile = $path.'../uploads/thumbs/'.substr(basename($_file_path),0,-4).'_'.$_width.'x'.$_height.'-c'.$_crop.'.'.substr(strrchr($_file_path,'.'),1);//substr(basename($_file_path),-4);
			}


			if(file_exists($file) && !file_exists($thumbfile)){

				$this->load->library('image_lib');
				$config['image_library']    = 'gd2';
				$config['source_image']     = $file;
				$config['new_image']        = $thumbfile;
				$config['maintain_ratio']   = true;//is_null($mode);
				$config['width']     = ($_GET['width']>0?$_GET['width']:10);
				$config['height']   = ($_GET['height']>0?$_GET['height']:10);
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

				/*
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file;
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = TRUE;
				$config['quality'] = '80%';
				$config['new_image'] = $thumbfile;
				$config['width']     = ($_GET['width']>0?$_GET['width']:10);
				$config['height']   = ($_GET['height']>0?$_GET['height']:10);
				$config['dynamic_output'] = false;
				$config['thumb_marker'] = false;
				$this->load->library('image_lib');
				$this->image_lib->clear();
				$this->image_lib->initialize($config);

				$res = $this->image_lib->resize();*/

				if ( !$this->image_lib->resize()) {
					echo $this->image_lib->display_errors();
				}

				$this->image_lib->clear();
			}

			if(file_exists($thumbfile)){
				header('Content-Type: image/jpeg');
			    //header('Content-Length: ' . filesize($thumbfile));
			    //echo file_get_contents($thumbfile);
				readfile($thumbfile);
			}
		}
		exit;
	}

	/*public function download_file(){

		$route = $this->uri->segments;

		$path = "";
		foreach($route as $key=>$value){
			if($key >= 4){
				$path .= "/". $value;
			}
		}

		$mainpath = BASEPATH.'../../../uploads';
		if(file_exists($mainpath . $path)){
			$this->load->helper(array('download'));

			$data = file_get_contents($mainpath.$path); // Read the file's contents
			$name = basename($path);
			force_download($name, $data);
			//echo $path;
			//download($path);
		}else{
			if(isset($_SERVER["HTTP_REFERER"])){
				$this->session->set_flashdata('error', 'Erro ao tentar aceder ao ficheiro');
				redirect($_SERVER["HTTP_REFERER"]);
			}
		}
		return false;
	}*/

	/*public function ajaxGetDependencies($field_name, $parent_val = null)
	{
		$res = array('field'=>$field_name,'values'=>array());
		$f = $this->{$this->module_name . "_m"}->get_field($field_name);
		if(!is_null($parent_val) && isset($f['parent'])){
			$table = $f['parent']['table'];
			$where = $f['parent']['parent_field'] . ' = "' . $parent_val . '"';
			$id_field = $f['parent']['id_field'];
			$name_field = $f['parent']['name_field'];
			$sort_by = (isset($f['parent']['sort_by']) ? $f['parent']['sort_by'] : $f['parent']['name_field'] );

			$values = $this->{$this->module_name . "_m"}->dropdown($id_field, $name_field, array("table"=>$table, "sort_by"=> $sort_by,"where"=>$where));

			$res['values'][] = array("key"=>'', "val"=>'Seleccione');
			foreach($values as $key=>$val){
				$res['values'][] = array("key"=>$key, "val"=>$val);
			}
		}
		echo json_encode($res);
		die;
	}*/

	protected function _is_ajax()
	{
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') ? TRUE : FALSE;
	}
}