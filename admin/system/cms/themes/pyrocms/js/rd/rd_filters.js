/**
 * @author JoaoTeixeira
 */
var filter_id = 0;
$(document).ready(function(){
	if(typeof _filter != 'undefined') filters_init();
});

function filters_init(){
	add_filter();
	
	$("#filters_form").submit(function(){
		var el = $(this);
		var opts = get_filter_options();
		opts_str = JSON.stringify(opts);
		$("[name=fq]",el).val(opts_str);
	});
	if(_filter_active && _filter_active.length){
		filter_reconstruct();
	}
	
}

function selector_change(el){
	var fitler_type = $("option:selected",el).val();
	var id = el.parents(".filter_li").data("id");
	change_filter(id,fitler_type);

	$('.date').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
	$(".datetime").datetimepicker({
		dateFormat: "yy-mm-dd",
		timeFormat: 'hh:mm:ss',
		changeMonth: true,
		changeYear: true
	});
}

function joiner_change(el){
	var joiner_type = $("option:selected",el).val();
	var parent = el.parents(".filter_li");
	if(joiner_type == ""){
		parent.nextAll().hide();
	}else{
		var next_li = parent.next();
		if(next_li.length){
			next_li.show();
			$(".filter_joiner",next_li).change();
		}else{
			add_filter();
		}
	}
}

function change_filter(id,ftype){
	
	var new_filter = create_filter(ftype,id); 
	var parent_el = $("#filter_"+id); 
	var l = $("#filter_"+id+" > *").length;
	var filter_el = $(".filter_single",parent_el);
	var selector_el = parent_el.children(":first-child");
	
	if(l>=3) selector_el = parent_el.children().eq(1);
	
	filter_el.remove();
	new_filter.insertAfter(selector_el);
	
}

function add_filter(){
	var id = filter_id++;
	var new_selector = create_selector(id);
	var new_joiner = create_joiner();
	
	$("<li class='filter_li' id='filter_"+id+"'></li>")
		.data("id",id)
		.append(new_selector)
		.append(new_joiner)
		.appendTo("#filters_list");
		
	new_selector.change();
}

function create_joiner(){
	var new_joiner = $(_filter.joiner);
	new_joiner.removeAttr("id");
	new_joiner.change(function(){joiner_change($(this));});
	return new_joiner;
}

function create_selector(){
	var new_selector = $(_filter.selector);
	new_selector.removeAttr("id");
	new_selector.change(function(){selector_change($(this));});
	return new_selector;
}

function create_filter(ftype, id){
	var new_filter = $(_filter["master_"+ftype]);
	new_filter.removeAttr("id");
	
	return new_filter;
}

function get_filter_options(){
	var res = [];
	$("#filters_list > li:visible").each(function(){
		var el = $(this);
		var ftype = $(".filter_selector",el).val();
		var filter_el = $(".filter_single",el);
		var how = $(".filter_how",el);
		var values = {};
		$("input,textarea,select:not(.filter_how,.filter_joiner)",filter_el).each(function(){
			var input=$(this),
				name = input.attr("name");
			if(typeof name != 'undefined') values[input.attr("name")]=input.val(); 
			
		});
		var joiner = $(".filter_joiner",el);
		
		if(joiner.length){
			j_value = joiner.val();
		}
		h_value = how.val();
		
		opts = {
			ftype: ftype,
			how: h_value, 
			values: values,
			joiner: j_value
		}
		res.push(opts);
	});
	
	return res;
}

function filter_reconstruct(){
	var cur_f = 0;
	
	for(i in _filter_active){
		var cur_filter = $("#filters_list .filter_li:eq("+(cur_f++)+")");
		
		var f = _filter_active[i];
		
		$(".filter_selector",cur_filter).val(f.ftype).change().change();
		$(".filter_how",cur_filter).val(f.how).change();
		for(k in f.values){
			var v = f.values[k];
			$("[name="+k+"]",cur_filter).val(v).change();
		}
		$(".filter_joiner",cur_filter).val(f.joiner).change();
	}
}
    