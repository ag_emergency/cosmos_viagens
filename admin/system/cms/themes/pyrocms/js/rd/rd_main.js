$(document).ready(function(){
	
	/* ***
	   ORDER
	*** */
	$('.change_order').bind('click',function(){
		$bt = $(this).parent().hide();
		$form = $($bt).next();
		$form.show();

		$('.bt_change_order',$form).bind('click',function(){
			$idToOrder = $('input[name=new_order]', $form).attr('rel');
			$neworder = $('input[name=new_order]', $form).val();
			$url = BASE_MODULE + 'ajaxaction/changeOrder/' + $idToOrder;
			$.ajax({
				url: $url,
				type: 'POST',
				data:{'neworder':$neworder, 'id':$idToOrder},
				dataType: 'json',
				success: function(data){
					if(data.res){
						window.location = data.redirect;
					}else{
						$form.hide();
						$bt.show();
					}
					return true;
				}
			});
		});
	});

	$('.truncate').livequery(function () {
		if(!$('.truncate_more',$(this)).length){
			$(this).jTruncate({  
		        length: 100,  
		        minTrail: 0,  
		        moreText: "[mais]",  
		        lessText: "[menos]",  
		        ellipsisText: " ...",  
		        moreAni: "slow",  
		        lessAni: "fast"  
	    	}); 
		}
    });
	
	/*$('.fancybox').livequery(function () {
		$(".fancybox").fancybox();
	});*/	
    	
	/* ***
	   WYSIWYG
	*** */
	if (CKEDITOR.instances['html']) { delete CKEDITOR.instances['html'] };
	$('textarea.wysiwyg-simple').ckeditor({
		toolbar: [
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink']
		],
		width: '99%',
		height: 100,
		dialog_backgroundCoverColor: '#000',
		contextmenu: { options: 'Context Menu Options' }
	});
	$('textarea.wysiwyg-advanced').ckeditor({
		width: '99%',
		height: 130,
		dialog_backgroundCoverColor: '#000',
		contextmenu: { options: 'Context Menu Options' }
	});
	/* ***
	   DATEPICKER
	*** */
	$.datepicker.setDefaults($.datepicker.regional['pt']);
	//$('.date').datepicker();
	//$('.datetime').datepicker({ dateFormat: 'yy-mm-dd'+' 00:00:00', changeMonth: true, changeYear: true });
	$('.date').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
	$(".datetime").datetimepicker({
		dateFormat: "yy-mm-dd",
		timeFormat: 'hh:mm:ss',
		changeMonth: true,
		changeYear: true
	});
	
    

	/* ***
	   COLORPICKER
	*** */
	var hideit = function(e, ui) { console.log(e,ui);$(this).val('#'+ui.hex); $('.ui-colorpicker').css('display', 'none'); };
	$('.colorpicker').colorpicker({ hide: hideit, submit: hideit });
	/* ***
	   SELECTBOXES WITH DEPENDENCIES
	*** */
	$('.rd_dependency').each(function(){
		var $el = $(this)
			$parent = $("#f_create_" + $el.attr('rel')),
			rd_dependencies = $parent.data('rd_dependencies') || [];
		
		rd_dependencies.push($(this).attr('name'));
		$parent.data('rd_dependencies', rd_dependencies);
		
		$el.data('orival', $el.attr('orival'));

		$parent.change(function(){
			var $parent = $(this),
				parent_val = $parent.val(),
				dep_ids = $parent.data('rd_dependencies');

			for(i in dep_ids){
				updateDependencies(dep_ids[i], parent_val);
			}
		}).change();
	});

});
/* ***
   HELPERS
*** */
function gMap()
{
	var myLatlng = new google.maps.LatLng($('#gMap').data('coord-lat'),$('#gMap').data('coord-lng'));
	var myOptions = {
		zoom: 17,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById('gMap'), myOptions);
	
	var marker = new google.maps.Marker({
		position: myLatlng,
		draggable: true,
    	animation: google.maps.Animation.DROP,
		map: map
  	});
	
	google.maps.event.addListener(marker, 'dragend', function() {
		$('#gMap').siblings('input').val(marker.position.lat()+', '+marker.position.lng());
	});
	google.maps.event.addListener(map, 'click', function(event) {
		marker.setPosition(event.latLng);
		$('#gMap').siblings('input').val(marker.position.lat()+', '+marker.position.lng());
	});
}
function updateDependencies(dep_id, parent_val){
	url = BASE_MODULE + 'ajaxGetDependencies/' + encodeURI(dep_id) + '/' + encodeURI(parent_val);

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(data){
			if(data.field){
				var id = '#f_create_' + data.field,
					$el = $(id),
					opts = '';
				for(i in data.values){
						var key = data.values[i]['key'];
						var val = data.values[i]['val'];

					opts += '<option value="'+key+'" ' + ($el.data('orival') == key?'selected="selected"':'') + ' >' + val + '</option>';
					if($el.data('orival') == key){ $el.data('orival', null); }
				}

				$el.html(opts).change();
				$.uniform.update(id); 
			}
		}
	});
}
$.datepicker.regional['pt'] = {
	monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
	monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
	dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
	dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
	dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
	dateFormat: 'yy-mm-dd',
	firstDay: 0,
	prevText: 'Ante.',
	prevStatus: '',
	prevJumpText: '&#x3c;&#x3c;',
	prevJumpStatus: '',
	nextText: 'Pr&oacute;x.',
	nextStatus: '',
	nextJumpText: '&#x3e;&#x3e;',
	nextJumpStatus: '',
	currentText: 'Hoje',
	currentStatus: '',
	todayText: 'Hoje',
	todayStatus: '',
	clearText: 'Limpar',
	clearStatus: '',
	closeText: 'Fechar',
	closeStatus: '',
	yearStatus: '',
	monthStatus: '',
	weekText: 'Sm',
	weekStatus: '',
	dayStatus: 'D, M d',
	defaultStatus: '',
	isRTL: false
};