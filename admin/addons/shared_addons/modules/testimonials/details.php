<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Testimonials extends Module {

	public  $version = '1.0.0',
			$tablename = '_testimonials', // nome da tabela
			$modulename = 'testimonials', // nome do modulo
			$sectionname = "Testemunhos"; // nome do elemento no menu

	public function info(){
		return array(
			'name' => array(
				'pt' => $this->sectionname
			),
			'description' => array(
				'pt' => 'Gestão da página de Testemunhos' // descricao auxiliar do modulo
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => 'modulos', // nome do elemento "pai" no menu, traduzir no ficheiro "admin_lang.php"

			// adicionar tabs
			/*'sections' => array(
				'items' => array(
					'name' 	=> $this->sectionname,
					'uri' 	=> 'admin/'.$this->modulename,
					'shortcuts' => array(
						'create' => array(
							'name' 	=> 'cp:actions_add',
							'uri' 	=> 'admin/'.$this->modulename.'/create',
							'class' => 'add'
							)
						)
					)
				)*/
		);
	}

	public function install(){
		// instalar tabela do modulo
		// http://ellislab.com/codeigniter/user-guide/database/
		$this->dbforge->drop_table($this->tablename);
		$this->db->delete('settings', array('module' => $this->modulename));

		$table_fields = array(
			'id' => array(  'type' => 'INT', 'constraint' => '11', 'unsigned' => TRUE, 'auto_increment' => TRUE ),
			'section' => array(	'type' => "SET('sports', 'corporate', 'incentives', 'leisure')", 'default'=>'sports' ),
			'title' => array('type' => 'VARCHAR', 'constraint' => '255' ),
			'url' => array(	'type' => 'VARCHAR', 'constraint' => '255' ),
			'image_url_big' => array(	'type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE ),
			'image_url_small' => array(	'type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE ),
			'lang' => array(  'type' => "SET('pt', 'en')", 'default'=>'pt' ),
			'ord' => array( 'type' => 'INT', 'constraint' => '11', 'unsigned' => TRUE, 'default'=>'1000' ),
			'active' => array(  'type' => 'TINYINT', 'constraint' => '1', 'default'=>'1' )
		);


		$this->dbforge->add_field($table_fields);
		$this->dbforge->add_key('id', TRUE);

		// Let's try running our DB Forge Table and inserting some settings
		//if ( ! $this->dbforge->create_table($this->tablename) OR ! $this->db->insert('settings', $table_setting) )
		if ( ! $this->dbforge->create_table($this->tablename) )
		{
			return FALSE;
		}

		// No upload path for our module? If we can't make it then fail
		if ( ! is_dir($this->upload_path.'sample') AND ! @mkdir($this->upload_path.'sample',0777,TRUE))
		{
			return FALSE;
		}

		// We made it!
		return TRUE;
	}

	public function uninstall(){
		$this->dbforge->drop_table($this->tablename);

		//$this->db->delete('settings', array('module' => 'sample'));

		// Put a check in to see if something failed, otherwise it worked
		return TRUE;
	}

	public function upgrade($old_version){
		// Your Upgrade Logic
		return TRUE;
	}

	public function help(){
		// Return a string containing help info
		return '<h4>Ajuda</h4>Este módulo permite fazer a gestão dos testemunhos.'; // descricao auxiliar para o utilizador

		// You could include a file and return it here.
		return $this->load->view('help', NULL, TRUE); // loads modules/sample/views/help.php
	}
}
?>