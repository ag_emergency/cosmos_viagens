<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Mailinglist_m extends RD_Model {

	protected 	$modulename = 'mailinglist', // nome do modulo
				$tablename = '_mailinglist'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(
				'field'=>'id',
				'where'=>'uh',
				'type'=>'number'));

		$this->add_field(array(
				'field'=>'email',
				'where'=>'cruf',
				'type'=>'email',
				'label'=>'Email',
				'rules'=>'required|valid_email'));

		$this->add_field(array(
				'field'=>'section',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Secção',
				'rules'=>''));

		$this->add_field(array(
				'field'=>'created_at',
				'where'=>'cruhf',
				'type'=>'date',
				'default'=>date('Y-m-d H:i:s')));

		$this->db->dbprefix = null;

		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}");
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url

		//$this->add_action_common('Exportar','link',"admin/{$this->_module_name}/export/{id}"); // accao, tipo, url
		$this->add_action_bottom('Export Excel', 'button',"admin/{$this->_module_name}/export/xls/{filter_key}" );
		$this->add_action_common('Exportar', 'link',"admin/{$this->_table}/export/csv/{filter_key}" );

	}

}