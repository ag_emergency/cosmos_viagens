<?php defined('BASEPATH') OR exit('No direct script access allowed');

function action_create($a,$item=NULL,$primaryCol=NULL,$extra_format=array()){
	$html = '';
	switch($a['type']){
		case 'toggle':
			$link = sformat(sformat($a['value'],$item), $extra_format);
			$cur_value = $item->{$a['col']};
			$text = $cur_value ? 'Sim' : 'Não';
			$class = isset($a['class']) && $a['class'] ? $a['class'] : '';
			$html = anchor($link,$text,"class=' {$class}'");
			break;
		case 'button':
			$link = sformat(sformat($a['value'],$item), $extra_format);
			$text = $a['display'];
			$class = isset($a['confirm']) && $a['confirm'] ? 'confirm' : '';
			$html = anchor($link,$text ,"class='button {$class}'");
			break;
		case 'order':
			$id = sformat(sformat($a['value'],$item), $extra_format);
			$cur_value = $item->{$a['col']};
			$html = '
				<div style="width:80px">
				<div class="ordertext"><span style="margin-right:5px">'.$cur_value.'</span><a href="javascript:;" class="edit change_order">ok</a></div>
				<div class="ordering" style="display:none;">
					<input rel="'.$item->id.'" type="text" name="new_order" class="text" value="'.$cur_value.'" style="width:30px;text-align:center;margin-right:5px;"/>
					<a href="javascript:;" class="bt_change_order">ok</a>
				</div>
				</div>
			';
			break;
		case 'modal':
			$link = sformat(sformat($a['value'],$item), $extra_format);
			$text = $a['display'];
			$class = isset($a['confirm']) && $a['confirm'] ? 'confirm' : '';
			$html = anchor($link,$text ,"class='button modal {$class}'");
			break;
		case 'previewIframe':
			$link = sformat(sformat($a['value'],$item), $extra_format);
			$text = $a['display'];
			$html = anchor($link,$text ,"class='button magnificPopupFrame'");
			break;
		case 'link':
		default:
			$link = sformat(sformat($a['value'],$item), $extra_format);
			$text = $a['display'];
			$class = isset($a['confirm']) && $a['confirm'] ? 'confirm' : '';
			$html = anchor($link,$text ,"class='button {$class}'");
			break;
	}

	return $html;
}


function rdform_create($f,$item=null,$index=null){

	if(!empty($f['label']) && !$f['_hidden']){

		$rules = $f['rules'];
		$req = '';
		if(isset($rules) && !is_null($rules) && strpos($rules, 'required') !== false) $req = ' <span>*</span>';

		$html_label = form_label($f['label'].$req,'f_create_'.$f['field']);
	}else{
		$html_label = '';
	}

	$html = '<div class="input">'.rd_form_input_create($f,$item).'</div>';

	$class= !is_null($index) && $index%2 ? 'even' : '';

	if($f['_hidden']){
		$html="<li style='display:none'>{$html_label}{$html}</li>";
	}else{
		$html="<li class='{$class}' >{$html_label}{$html}</li>";
	}


	return $html;
}

function rd_form_input_create($f,$item=NULL,$is_filter=false){

	if($is_filter){
		$default = NULL;
	}else{
		$default = is_null($item) ? $f['default'] : $item->{$f['field']};
	}
	$html = '';
	if(!$is_filter && $f['_hidden']){
		$html .= form_hidden(
				$f['field'],
				set_value($f['field'], $default),
				"id=f_create_{$f['field']}",
				$is_filter
			);
		return $html;
	}
	switch($f['type']){
		case 'htmlAdv':
			$f_class = 'wysiwyg-advanced';
		case 'html':
			if(!isset($f_class)){$f_class = 'wysiwyg-simple';}
		case 'textarea':
			if(!isset($f_class)){$f_class = '';}
			$html .= form_textarea(
					$f['field'],
					set_value($f['field'],$default),
					"id=f_create_{$f['field']} class='{$f_class}'",
					$is_filter
				);
			break;
		case 'select':
			$extra = "id=f_create_{$f['field']}";

			if(isset($f['parent'])){
				$values = array('' => '-');
				$val = set_value($f['field'],$default);
				$extra .= ' rel="' . $f['parent']['parent_field'] . '" class="rd_dependency" orival="' . $val . '"';
			}else{
				$values = $f['values'];
			}
			$html .= form_dropdown(
					$f['field'],
					$values,
					set_value($f['field'], $default),
					$extra,
					$is_filter
				);
			break;
		case 'image':
			$html.= (isset($f['form_type']) && $f['form_type'] == 'u' && $default != '' ? '<a class="fancybox" href="'.base_url().$f['path'].$default.'"><img src="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path='.$f['path'].$default.'&width=100&height=100&crop=1"/></a> ' : '');
			if($f['form_type'] != 'f'){
				$html .= form_upload(
					$f['field'],
					set_value($f['field'], $default),
					"id=f_create_{$f['field']}",
					$is_filter
				);
			}else{
				$html .= form_dropdown(
						$f['field'],
						array('0'=>'Não', '1'=>'Sim'),
						set_value($f["field"],$default),
						"id=f_create_{$f['field']}",
						$is_filter
				);
			}
			break;
		case 'file':
			$html .= (isset($f['form_type']) && $f['form_type'] == 'u' && $default != '' ? '<a href="'.base_url().$f['path'].$default.'" target="_blank">Download</a> ' : '');
			if($f['form_type'] != 'f'){
				$html .= form_upload(
					$f['field'],
					set_value($f['field'], $default),
					"id=f_create_{$f['field']}",
					$is_filter
				);
			}else{
				$html .= form_dropdown(
						$f['field'],
						array('0'=>'Não', '1'=>'Sim'),
						set_value($f['field'],$default),
						"id=f_create_{$f['field']}",
						$is_filter
				);
			}
			break;
		case 'googleMap':
			if(isset($item->gMap))
				{
					$temp = explode(', ', $item->gMap);
					$lat = $temp[0];
					$lng = $temp[1];
				}
			else
				{
					$lat = '38.70768335958255';
					$lng = '-9.136558606109588';
				}
			$html .= '<div id="gMap" data-coord-lat="'.$lat.'" data-coord-lng="'.$lng.'"></div>';
			$html .= '<input type="hidden" name="'.$f['field'].'" ';
				if(isset($item->gMap)) { $html .= 'value="'.$item->gMap.'"'; }
			$html .= ' />';
			$html .= '<script src="//maps.google.com/maps/api/js?sensor=false"></script>';
			$html .= '<script>$(document).ready(function(){ gMap(); });</script>';
			break;
		case 'date':
			$html .= form_input(
					$f['field'],
					set_value($f['field'], $default),
					"id=f_create_{$f['field']} class='date'",
					$is_filter
				);
			break;
		case 'datetime':
			$html .= form_input(
					$f['field'],
					set_value($f['field'], $default),
					"id=f_create_{$f['field']} class='datetime'",
					$is_filter
				);
			break;
		case 'colorpicker':
			//$cor = 'ffffff';
			$html .= '<input type="text" name="'.$f['field'].'" id="id_'.$f['field'].'" class="colorpicker" />';
			break;
		case 'checkbox':
			$html .= form_checkbox(
					$f['field'],
					$f['value'],
					set_value($f['field'], $default),
					"id=f_create_{$f['field']}",
					$is_filter
				);
			break;
		case 'info':
			$html .= (isset($f['form_type']) && $f['form_type'] == 'u' && $default != '' ? '<div>'.$f['values'][$default].'</div>' : '');
			break;
		case 'password':
			$html .= form_password(
					$f['field'],
					set_value($f['field'], $default),
					"id=f_create_{$f['field']}",
					$is_filter
				);
			break;
		case 'number':
		case 'text':
		default:
			$html .= form_input(
					$f['field'],
					set_value($f['field'], $default),
					"id=f_create_{$f['field']}",
					$is_filter
				);
			break;
	}
	return $html;
}


function filter_create($f){
	$html  = filter_how_create($f['type'],$f['field']);
	$html .= rd_form_input_create($f,NULL,true);
	return "<div class='filter_single' id='filter_master_{$f['field']}'>{$html}</div>";
}

function filter_how_create($type,$field){
	switch($type){
		case 'datetime':
		case 'date':
		case 'time':
			$values = array(
					'eq'=>'=',
					'big'=>'>',
					'bigeq'=>'>=',
					'less'=>'<',
					'lesseq'=>'<=',
					'noteq'=>'!='
				);
			break;
		case 'select':
			$values = array(
					'like'=>'Igual',
					'notlike'=>'Diferente'
				);
			break;
		case 'number':
			$values = array(
					'eq'=>'=',
					'big'=>'>',
					'bigeq'=>'>=',
					'less'=>'<',
					'lesseq'=>'<=',
					'noteq'=>'!='
				);
			break;
		case 'checkbox':
		case 'text':
		default:
			$values = array(
					'like'=>'Igual',
					'notlike'=>'Diferente'
				);
	}
	return form_dropdown("_fh_{$field}", $values, set_value("_fh_{$field}"), "class='filter_how'", true);
}

function filter_selector_create($fields){
	$values = array();
	foreach($fields as $name => $f){
		if(!empty($f['label'])){
			$values[$name] = $f['label'];
		}
	}
	return form_dropdown('fs', $values, set_value('fs'),'id="filter_selector" class="filter_selector"', true);
}

function filter_joiner_create(){
	$values = array(
			''=>'Adicionar Filtro',
			'and'=>'E',
			'or'=>'Ou'
		);

	return form_dropdown('fj', $values, set_value('fj'),'id="filter_joiner" class="filter_joiner"', true);
}

function form_how_translate($how){
	$map = array(
			'eq' => '=',
			'noteq' => '!=',
			'big' => '>',
			'bigeq' => '>=',
			'less' => '<',
			'lesseq' => '<=',
			'like' => 'LIKE',
			'notlike' => 'NOT LIKE'
		);

	if(!isset($map[$how])){
		throw new Exception('RDActions Helper: Can\'t translate this HOW option: "'.$how.'"');
	}

	return $map[$how];
}

function filter_value_parse($value, $type){
	switch($type){
		case 'number':
			$value_new = floatval($value);
			break;
		case 'datetime':
			$value_new = '"'.date('Y-m-d H:i:s',strtotime($value)).'"';
			break;
		case 'date':
			$value_new = '"'.date('Y-m-d',strtotime($value)).'"';
			break;
		default:
			$value_new = "'{$value}'";
	}
	return $value_new;
}

function replace_newline($string) {
	return (string)str_replace(array('\r', '\r\n', '\n'), '', $string);
}

function sformat($template, $params){
    $output = $template;
    foreach($params as $key => $value){
        $output = str_replace('{'.$key.'}', $value, $output);
    }
    return $output;
}


/**
 * Prepare to List
 *
 * This function clean tags and javascript to show in list
 *
 * @access	public
 * @param	string	the text string
 * @return	string
 */
/*function _e($str){
	return nl2br(htmlentities($str, ENT_QUOTES, 'UTF-8'));
}*/

/*function permalink($str) {
	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace('/[^a-zA-Z0-9\/_| -]/', '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace('/[\/_| -]+/', '-', $clean);

	return $clean;
}*/