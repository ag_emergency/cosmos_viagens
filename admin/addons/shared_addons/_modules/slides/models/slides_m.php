<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Slides_m extends RD_Model {

	protected 	$modulename = 'slides', // nome do modulo
				$tablename = '_slides'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada

		$this->add_field(array('field'=>'id',
								'where'=>'uh',
								'type'=>'number'));

		$this->add_field(array(	'field'=>'type',
								'where'=>'rcuf',
								'type'=>'select',
								'label'=>'Tipo',
								'default'=>'0',
								'values'=>array('0'=>'Slider',
												'1'=>'Banners')));

		$this->add_field(array(	'field'=>'title',
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Título',
								'rules'=>'required'));

		$this->add_field(array('field'=>'url',
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Url',
								'rules'=>'required'));

		$this->add_field(array('field'=>'image_url_big',
								'where'=>'rcu',
								'label'=>'Imagem (1400x673)',
								'path'=>'../uploads/slides/',
								'type'=>'image',
								'url'=>'Download',
								'allowed_types'=>'jpg|jpeg|gif|png' ));

		$this->add_field(array(	'field'=>'image_url_small',
								'where'=>'cu',
								'label'=>'Imagem (960x259)',
								'path'=>'../uploads/slides/',
								'type'=>'image',
								'url'=>'Download',
								'allowed_types'=>'jpg|jpeg|gif|png' ));

		$this->add_field(array(	'field'=>'ord'
								,'where'=>'h'
								,'type'=>'number'
								,'default'=>'1000'
								,'label'=>'Ordem'));

		$this->add_field(array(	'field'=>'active',
								'where'=>'rcuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));


		// adicionar links para accoes
		$this->add_action('Ordem','order',"admin/{$this->_module_name}/action/changeOrderToggle/{id}",array('col'=>'ord','module'=>$this->modulename)); // title, tipo, url, [coluna,modulo]
		//se tem "actions:"Editar é porque está na lang
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Eliminar','link',"admin/{$this->_module_name}/delete/{id}",array("confirm"=>TRUE));


	}

	public function get_all($limit=0, $offset=0, $filters=FALSE){
		$this->db->order_by('ord ASC');
		return parent::get_all($limit,$offset,$filters);
  	}

	public function changeOrder($id){
		return parent::changeMainOrder($id, 'ord');
	}

}