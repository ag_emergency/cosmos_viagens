<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author      Actualsales
 * @package     PyroCMS
 * @subpackage  Press Module
 * @category    Modules
 * @license     Apache License v2.0
 */
 
class Todos_m extends RD_Model {
    
    protected   $modulename = 'todos', // nome do modulo
                $tablename = '_todos'; // nome da tabela
            
    public function __construct()
    {
        parent::__construct($this->modulename, $this->tablename);
        
        // Load all required classes
        $this->load->helper('debug');
        
        // same old same old... quando tiver tempo faco documentacao mais detalhada

        $this->add_field(array( 'field'=>'id',
                                'where'=>'uh',
                                'type'=>'number'));

        $this->add_field(array( 'field'=>'textarea', 
                                'where'=>'cu',
                                'type'=>'textarea',
                                'label'=>'TEXTAREA',
                                'rules'=>'required'));

        $this->add_field(array( 'field'=>'html', 
                                'where'=>'cu',
                                'type'=>'html',
                                'label'=>'HTML',
                                'rules'=>'required'));

        $this->add_field(array( 'field'=>'htmladv', 
                                'where'=>'cu',
                                'type'=>'htmlAdv',
                                'label'=>'HTML ADV',
                                'rules'=>'required'));

        $this->add_field(array( 'field'=>'select',
                                'where'=>'cu',
                                'type'=>'select',
                                'label'=>'SELECT',
                                'rules'=>'required',
                                'default'=>'1',
                                'values'=>array('1'=>':)',
                                                '0'=>':(')));

        $this->add_field(array( 'field'=>'selectdb',
                                'where'=>'cu',
                                'type'=>'select',
                                'label'=>'SELECT DB',
                                'rules'=>'required',
                                'values'=>$this->dropdown(    
                                            'id',
                                            'titulo',
                                            array(  'table'=>'default__todos_categorias',
                                                    'where'=>'active = 1',
                                                    'sort_by'=>'titulo')) ));

        $this->add_field(array( 'field'=>'image', 
                                'where'=>'cu',
                                'label'=>'IMAGE', 
                                'path'=>'../uploads/todos/', 
                                'type'=>'image', 
                                'url'=>'Download', 
                                'allowed_types'=>'jpg|jpeg|gif|png'));

        $this->add_field(array( 'field'=>'file', 
                                'where'=>'cu',
                                'label'=>'FILE', 
                                'path'=>'../uploads/todos/', 
                                'type'=>'file', 
                                'url'=>'Download', 
                                'allowed_types'=>'zip|pdf|doc|xls|txt'));
                                
        $this->add_field(array( 'field'=>'gmap', 
                                'where'=>'cu',
                                'type'=>'googleMap',
                                'label'=>'GOOGLE MAP',
                                'rules'=>'required'));

        $this->add_field(array( 'field'=>'date', 
                                'where'=>'cu',
                                'type'=>'date',
                                'label'=>'DATE',
                                'rules'=>'required'));

        $this->add_field(array( 'field'=>'datetime', 
                                'where'=>'cu',
                                'type'=>'datetime',
                                'label'=>'DATETIME',
                                'rules'=>'required'));

        $this->add_field(array( 'field'=>'colorpicker', 
                                'where'=>'cu',
                                'type'=>'colorpicker',
                                'label'=>'COLORPICKER',
                                'rules'=>'required'));

        $this->add_field(array( 'field'=>'checkbox', 
                                'where'=>'cu',
                                'type'=>'checkbox',
                                'value'=>'1',
                                'label'=>'CHECKBOX',
                                'rules'=>'required'));
                                
        $this->add_field(array( 'field'=>'text', 
                                'where'=>'rcuf',
                                'type'=>'text',
                                'label'=>'TEXT',
                                'rules'=>'required'));
                                
        $this->add_field(array( 'field'=>'permalink', 
                                'where'=>'cuh',
                                'type'=>'text'));

        $this->add_field(array( 'field'=>'active',
                                'where'=>'rcuf',
                                'type'=>'select',
                                'label'=>'ACTIVE',
                                'default'=>'1',
                                'values'=>array('1'=>'Sim',
                                                '0'=>'Não')));
                            
        
        // adicionar links para accoes
        $this->add_action('Ordem','order',"admin/{$this->_module_name}/action/changeOrderToggle/{id}",array('col'=>'ord','module'=>$this->modulename)); // title, tipo, url, [coluna,modulo]
        // se tem "actions:"Editar é porque está na lang
        $this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
        $this->add_action_common('Eliminar','link',"admin/{$this->_module_name}/delete/{id}",array("confirm"=>TRUE));

        
    }
    
    public function get_all($limit=0, $offset=0, $filters=FALSE){
        $this->db->order_by('ord ASC');
        return parent::get_all($limit,$offset,$filters);
    }
    
    public function changeOrder($id){
        return parent::changeMainOrder($id, 'ord');
    }
    
}