<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Pedidos_info_m extends RD_Model {
	
	protected 	$modulename = 'pedidos_info', // nome do modulo
				$tablename = '_pedidos_info'; // nome da tabela
	
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'nome', 
								'where'=>'ruhf',
								'type'=>'text',
								'label'=>'Nome',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'email', 
								'where'=>'uhf',
								'type'=>'email',
								'label'=>'Email',
								'rules'=>'required|valid_email'));
								
		$this->add_field(array(	'field'=>'msg', 
								'where'=>'uh',
								'type'=>'textarea',
								'label'=>'Mensagem',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'dt',
								'where'=>'ruhf',
								'type'=>'date',
								'label'=>'Data de registo',
								'default'=>date('Y-m-d H:i:s')));
								
		$this->add_field(array(	'field'=>'estado',
								'where'=>'uf',
								'type'=>'select',
								'label'=>'Lido',
								'default'=>'0',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));
												
		// adicionar accoes especificas
		$this->add_action('Contactado','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'estado')); // title, tipo, url, coluna
							
		// adicionar links para accoes
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url
		
	}
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('estado', '1-estado', false);
		return $this->db->update($this->_table);
	}
	
	public function get_all($limit = 0, $offset = 0, $filters = false){
		$this->db->order_by('estado ASC, dt ASC');
		return parent::get_all($limit,$offset,$filters);
  	}
	
}