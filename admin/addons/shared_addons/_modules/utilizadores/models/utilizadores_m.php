<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Utilizadores_m extends RD_Model {
	
	protected 	$modulename = 'utilizadores', // nome do modulo
				$tablename = '_utilizadores'; // nome da tabela
	
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uhf',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'first_name',
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Nombre',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'last_name',
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Apellido',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'city_id', 
								'where'=>'cu',
								'type'=>'select',
								'label'=>'Ciudad',
								'rules'=>'required',
                                'values'=>$this->dropdown(    
                                            'id',
                                            'name',
                                            array( 'table'=>'default__config_cities',
                                                   'where'=>'active = 1',
                                                   'sort_by'=>'name')) ));
								
		$this->add_field(array(	'field'=>'email',
								'where'=>'rcuf',
								'type'=>'email',
								'label'=>'Email',
								'rules'=>'required|valid_email'));
								
		$this->add_field(array(	'field'=>'password',
								'where'=>'c',
								'type'=>'password',
								'label'=>'Contraseña',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sí',
												'0'=>'No')));
								
		$this->add_field(array(	'field'=>'creation_date',
								'where'=>'cuh',
								'type'=>'date',
								'default'=>date('Y-m-d H:i:s')));
		
		
		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active'));
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}");
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE));
		
		$this->add_action_bottom('Export Excel', 'button',"admin/{$this->_module_name}/export/xls/{filter_key}" );
	}
	
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}
}