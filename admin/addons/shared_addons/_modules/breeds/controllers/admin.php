<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	
	public 	$modulename = 'breeds', // nome do modulo
			$plural = 'Razas', // item plural
			$single = 'Raza'; // item singular
	
	public function __construct()
	{
		
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);
			
		// Load all required classes
		$this->load->helper('debug');
	}
	
	public function create()
	{
		
		if(isset($_POST['name']))
		{
			$_POST['permalink'] = $this->permalink($_POST['name']);
		}
		
		parent::create();
	}
	
	public function edit($id)
	{
		
		if(isset($_POST['name']))
		{
			$_POST['permalink'] = $this->permalink($_POST['name']);
		}
		
		parent::edit($id);
	}
	
}