<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Breeds_m extends RD_Model {
	
	protected 	$modulename = 'breeds', // nome do modulo
				$tablename = '_breeds'; // nome da tabela
			
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada

		$this->add_field(array( 'field'=>'id',
                                'where'=>'uh',
                                'type'=>'number'));

		$this->add_field(array(	'field'=>'specie_id',
                                'where'=>'cuf',
                                'type'=>'select',
                                'label'=>'Especie',
                                'rules'=>'required',
                                'values'=>$this->dropdown(    
                                            'id',
                                            'name',
                                            array( 	'table'=>'default__species',
                                            		'where'=>'active = 1',
                                                    'sort_by'=>'name')) ));
								
		$this->add_field(array(	'field'=>'permalink', 
                                'where'=>'cuh',
                                'type'=>'text'));
								
		$this->add_field(array(	'field'=>'name', 
                                'where'=>'rcuf',
                                'type'=>'text',
                                'label'=>'Nombre',
                                'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'scientific_name', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Nombre Cientifico',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'weight', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Peso'));
								
		$this->add_field(array(	'field'=>'height', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Altura'));

		$this->add_field(array(	'field'=>'characteristics', 
								'where'=>'cu',
								'type'=>'textarea',
								'label'=>'Características'));

		$this->add_field(array(	'field'=>'character', 
								'where'=>'cu',
								'type'=>'textarea',
								'label'=>'Carácter'));

		$this->add_field(array(	'field'=>'image_url', 
								'where'=>'rcu',
								'label'=>'Imagen (428x268)', 
								'path'=>'../uploads/species/', 
								'type'=>'image', 
								'url'=>'Download', 
								'allowed_types'=>'jpg|jpeg|gif|png'));

		$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sí',
												'0'=>'No') ));
                                                

								
		$this->add_field(array(	'field'=>'link_1', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 1 (URL)'));
		$this->add_field(array(	'field'=>'link_1_title', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 1 (Título)'));
                                
		$this->add_field(array(	'field'=>'link_2', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 2 (URL)'));
		$this->add_field(array(	'field'=>'link_2_title', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 2 (Título)'));
                                
		$this->add_field(array(	'field'=>'link_3', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 3 (URL)'));
		$this->add_field(array(	'field'=>'link_3_title', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 3 (Título)'));
                                
		$this->add_field(array(	'field'=>'link_4', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 4 (URL)'));
		$this->add_field(array(	'field'=>'link_4_title', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Link relacionado 4 (Título)'));
                                                

								
		$this->add_field(array(	'field'=>'prod_1', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Producto 1 (URL)'));
		$this->add_field(array(	'field'=>'prod_1_img', 
                                'where'=>'cu',
                                'label'=>'Producto 1 (Imagen, 98x73)', 
                                'path'=>'../uploads/products/', 
                                'type'=>'image', 
                                'url'=>'Download', 
                                'allowed_types'=>'jpg|jpeg|gif|png'));
								
		$this->add_field(array(	'field'=>'prod_2', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Producto 2 (URL)'));
		$this->add_field(array(	'field'=>'prod_2_img', 
                                'where'=>'cu',
                                'label'=>'Producto 2 (Imagen, 98x73)', 
                                'path'=>'../uploads/products/', 
                                'type'=>'image', 
                                'url'=>'Download', 
                                'allowed_types'=>'jpg|jpeg|gif|png'));
								
		$this->add_field(array(	'field'=>'prod_3', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Producto 3 (URL)'));
		$this->add_field(array(	'field'=>'prod_3_img', 
                                'where'=>'cu',
                                'label'=>'Producto 3 (Imagen, 98x73)', 
                                'path'=>'../uploads/products/', 
                                'type'=>'image', 
                                'url'=>'Download', 
                                'allowed_types'=>'jpg|jpeg|gif|png'));
								
		$this->add_field(array(	'field'=>'prod_4', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Producto 4 (URL)'));
		$this->add_field(array(	'field'=>'prod_4_img', 
                                'where'=>'cu',
                                'label'=>'Producto 4 (Imagen, 98x73)', 
                                'path'=>'../uploads/products/', 
                                'type'=>'image', 
                                'url'=>'Download', 
                                'allowed_types'=>'jpg|jpeg|gif|png'));
								
		$this->add_field(array(	'field'=>'prod_5', 
                                'where'=>'cu',
                                'type'=>'text',
                                'label'=>'Producto 5 (URL)'));
		$this->add_field(array(	'field'=>'prod_5_img', 
                                'where'=>'cu',
                                'label'=>'Producto 5 (Imagen, 98x73)', 
                                'path'=>'../uploads/products/', 
                                'type'=>'image', 
                                'url'=>'Download', 
                                'allowed_types'=>'jpg|jpeg|gif|png'));
							
		
		// adicionar links para accoes
		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active'));
		 
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Eliminar','link',"admin/{$this->_module_name}/delete/{id}",array("confirm"=>TRUE));

		
	}
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}
	
}