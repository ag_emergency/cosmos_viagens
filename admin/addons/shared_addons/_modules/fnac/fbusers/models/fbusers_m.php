<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Fbusers_m extends RD_Model {
	
	protected 	$modulename = 'fbusers', // nome do modulo
				$tablename = '_users'; // nome da tabela
	
	public function __construct()
	{

		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uhf',
								'type'=>'number'));

		$this->add_field(array(	'field'=>'facebook_id',
								'where'=>'uhf',
								'label'=>"FB ID",
								'type'=>'number'));

		$this->add_field(array(	'field'=>'name',
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Nome',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'',
								'where'=>'r',
								'type'=>'src_image',
								'link'=>null,
								'display' =>"http://graph.facebook.com/{facebook_id}/picture",
								'label'=>'FB Link',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'link',
								'where'=>'rcuf',
								'type'=>'link',
								'display' =>"FB User Profile",
								'label'=>'FB Link',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'email',
								'where'=>'rcuf',
								'type'=>'email',
								'label'=>'Email',
								'rules'=>'required|valid_email'));

		$this->add_field(array(	'field'=>'birthday',
								'where'=>'cu',
								'type'=>'datetime',
								'label'=>'Aniversário',
								'rules'=>''));

		$this->add_field(array(	'field'=>'location',
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Localização',
								'rules'=>''));

		$this->add_field(array(	'field'=>'ip',
								'where'=>'cu',
								'type'=>'text',
								'label'=>'IP',
								'rules'=>''));

		$this->add_field(array(	'field'=>'user_agent',
								'where'=>'cu',
								'type'=>'textarea',
								'label'=>'Browser',
								'rules'=>''));

		$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));
								
		$this->add_field(array(	'field'=>'created_at',
								'where'=>'cuh',
								'type'=>'date',
								'default'=>date('Y-m-d H:i:s')));

		$this->add_field(array(	'field'=>'updated_at',
								'where'=>'cuh',
								'type'=>'date',
								'default'=>date('Y-m-d H:i:s')));
		
		
		//enable tables without prefix /** BR */
		$this->db->dbprefix = null;
		

		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active'));
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}");
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE));
		
		$this->add_action_bottom('Export Excel', 'button',"admin/{$this->_module_name}/export/xls/{filter_key}" );
	}
	
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}
}