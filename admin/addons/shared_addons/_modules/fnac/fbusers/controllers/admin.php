<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	
	public 	$modulename = 'fbusers', // nome do modulo
			$plural = 'Users', // item plural
			$single = 'User'; // item singular
	
	public function __construct(){
		
		$dbprefix = $this->db->dbprefix;
		$this->db->set_dbprefix(null);
		// Add code for working with no dbprefix here.
		$this->db->set_dbprefix($dbprefix);
		
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);
			
		// Load all required classes
		$this->load->helper('debug');
	}

	public function lista($btnAdd = false){
		parent::lista(false);
	}
	
}