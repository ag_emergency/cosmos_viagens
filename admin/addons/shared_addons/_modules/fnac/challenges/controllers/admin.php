<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	
	public 	$modulename = 'challenges', // nome do modulo
			$plural = 'Passatempos', // item plural
			$single = 'Passatempo'; // item singular
	
	public function __construct(){		
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);

		$this->data->tabs = array(
			//'Quiz' => array("url"=>'#quiz', "template"=>'rd/lista'),
			'Quiz' => array("url"=>'#quiz', "template"=>null),
			'Share' => array("url"=>'#share', "template"=>null)
		);

		// Load all required classes
		$this->load->helper('debug');

		$this->template
				->append_css('rd/application.css')
				->append_js('module::magnificpopup.min.js')
				->append_css('module::magnific-popup.css')
				->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				->set_layout('default', 'rd')
				->set('title', $this->title)
				->set('single', $this->single);
	}
	
	public function create(){
		$this->goToEdit = true;
		parent::create();
	}

	public function edit($id)
	{
		//after submit goes to
		$this->session->set_userdata('return_url', 'admin/'.$this->module_name.'/edit/'.$id);

		$this->data->tabs["Quiz"]["url"] = base_url()."admin/quiz/edit/".$id;
		$this->data->tabs["Share"]["url"] = base_url()."admin/".$this->module_name."/shareurl/".$id;

		$this->data->tab = 'edit';
		$session_tab = $this->session->userdata('tab');
		
		if(null != $session_tab){
			$this->data->tab = $session_tab;
			$this->session->set_userdata('tab',null);
		}
		parent::edit($id);
	}

	public function shareurl($id)
	{
		$module = new $this->{$this->module_name.'_m'}();
		$id_rule = array( 	'field' => $module->get_PrimaryKey(),
							'label' => 'Primary Col',
							'rules' => 'required|is_numeric|trim' );

		array_push($this->validation_rules, $id_rule);

		//get the product we want to edit
		$item = $module->get($id);
		
		$FBAPP = "https://apps.facebook.com/fnacpassatempos/passatempo/".$item->id."/".permalink($item->title);
		$html = <<<EOT
<section class="item">
	<div class="content">
		<h2>Link direto para o passatempo.</h2>
		<a href="{$FBAPP}" target="_blank">{$FBAPP}</a>
	</div>
</section>
EOT;

	echo $html;
		exit;
	}
	
}

function permalink($str)
{
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace('/[^a-zA-Z0-9\/_| -]/', '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace('/[\/_| -]+/', '-', $clean);
    
    return $clean;
}