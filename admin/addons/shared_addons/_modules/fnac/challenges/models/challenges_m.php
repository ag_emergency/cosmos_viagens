<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Challenges_m extends RD_Model {
	
	protected 	$modulename = 'challenges', // nome do modulo
				$tablename = '_challenges'; // nome da tabela
	
	public function __construct()
	{

		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'ruhf',
								'type'=>'number'));

		$this->add_field(array(	'field'=>'type',
								'where'=>'rf',
								'type'=>'select',
								'label'=>'Tipo',
								'table'=>'_questions',
								'values'=>array('0'=>'Texto','1'=>'Múltiplas&nbsp;(1)', '4'=>'Múltiplas&nbsp;(3)', '2'=>'Video&nbsp;URL','3'=>'Upload&nbsp;Imagem')));

		$this->add_field(array(	'field'=>'title',
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Título',
								'rules'=>'required'));
		
		$this->add_field(array(	'field'=>'summary',
								'where'=>'cuf',
								'type'=>'textarea',
								'label'=>'Resumo',
								'rules'=>''));

		$this->add_field(array( 'field'=>'banner_img',
                                'where'=>'rcu',
                                'label'=>'Banner (600x300)',
                                'path'=>'../uploads/passatempos/',
                                'type'=>'image',
                                'url'=>'Download',
                                'allowed_types'=>'jpg|jpeg|gif|png'));

		$this->add_field(array( 'field'=>'header_img',
                                'where'=>'cu',
                                'label'=>'Header Img (810x400)',
                                'path'=>'../uploads/passatempos/',
                                'type'=>'image',
                                'url'=>'Download',
                                'allowed_types'=>'jpg|jpeg|gif|png'));

		$this->add_field(array(	'field'=>'start_date',
								'where'=>'rcuf',
								'type'=>'datetime',
								'label'=>'Data Inicio',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'end_date',
								'where'=>'rcuf',
								'type'=>'datetime',
								'label'=>'Data Fim',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'description',
								'where'=>'cuf',
								'type'=>'htmlAdv',
								'label'=>'Descrição',
								'rules'=>'skip_xss'));


		$this->add_field(array(	'field'=>'winners',
								'where'=>'cuf',
								'type'=>'htmlAdv',
								'label'=>'Vencedores',
								'rules'=>''));

		$this->add_field(array(	'field'=>'winners_date',
								'where'=>'cuf',
								'type'=>'datetime',
								'label'=>'Data Vencedores',
								'rules'=>'',
								'default'=>null));

		$this->add_field(array( 'field'=>'rules_pdf',
                                'where'=>'cu',
                                'label'=>'Regulamento (pdf)',
                                'path'=>'../uploads/passatempos/',
                                'type'=>'file',
                                'url'=>'Download',
                                'rules'=>'',
                                'allowed_types'=>'pdf'));

		
		$this->add_field(array(	'field'=>'approve_require',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Requer Aprovação',
								'default'=>'1',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));

		$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));
								
		$this->add_field(array(	'field'=>'created_at',
								'where'=>'cuh',
								'type'=>'date',
								'default'=>date('Y-m-d H:i:s')));

		$this->add_field(array(	'field'=>'num',
								'where'=>'rf',
								'type'=>'button',
								'display'=>'Listagem:&nbsp;{num}',
								'url'=>"admin/participations/listByChallenge/id/{id}",
								'label'=>'Participações'));
		
		
		//enable tables without prefix /** BR */
		$this->db->dbprefix = null;
		
		//$this->add_action('Participações','link',"admin/participations/listByChallenge/id/{id}");
		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active'));
		
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}");
		$this->add_action_common('Preview','previewIframe',substr(base_url(),0,-6)."passatempo/preview/{id}");
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE));
		
		$this->add_action_bottom('Export Excel', 'button',"admin/{$this->_module_name}/export/xls/{filter_key}" );
	}
	
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}


	public function get_all($limit = 0, $offset = 0, $filters = FALSE){
		$this->set_filters($filters);
		$this->db->select('_challenges.*, _questions.type, count(_participations.challenge_id) as num');
		if($limit>0){
			$this->db->limit($limit, $offset);
		}
		$this->db->from('_challenges');
		$this->db->join("_participations", "_challenges.id = _participations.challenge_id", "left");
		$this->db->join("_questions", "_challenges.id = _questions.challenge_id");
		$this->db->group_by("_challenges.id"); 
		$this->db->order_by("_challenges.start_date desc");
		$result = $this->db->get()->result();
		//dump($this->db->get());
		//dump($result);
		//dump($this->db->last_query());
		//dump($result);
		//exit;
		//dump($this->db->get_compiled_select());exit;
       return $result;//$this->db->get($this->_table)->result();
   }

   	public function count_all($filters=FALSE){
		$this->set_filters($filters);
		$this->db->select('count(_challenges.id) as num');
		$this->db->from('_challenges');
		$this->db->join("_questions", "_challenges.id = _questions.challenge_id");
		$this->db->order_by("_challenges.start_date desc");
		$result = $this->db->get()->row();
		return $result->num;
    }

   /*
	 * Set search filters for the next DB query
	 */
	protected function set_filters($filters){
		if(!$filters){return;}
		//dump($filters);
		$filters_main = $this->get_filters();
		foreach($filters as $f){
			$how = form_how_translate($f->how); 
			//dump($how);
			$filter_main = $filters_main[$f->ftype];
			$type = $filter_main['type'];
			if(isset($filter_main['table'])){
				$this->_table_alias = $filter_main['table'];
			}else{
				$this->_table_alias = $this->tablename;
			}

			foreach($f->values as $k => $v){
				if($type == 'file' || $type == 'image'){
					if($v == '0'){
						$how = 'is';
					}else{
						$how = 'is not';
					}
					$v = 'null';
				}else if($type == 'select'){
					$v = filter_value_parse($v, $type);
				}else if($f->how == 'like' || $f->how == 'notlike'){
					$v = "'%{$v}%'";
				}else{
					$v = filter_value_parse($v, $type);
				}
				//dump($this->_table_alias.'.'.$k.' '.$how.' ');
				//dump($f);
				if(isset($joiner) && $joiner == 'or'){
					$this->db->or_where($this->_table_alias.'.'.$k.' '.$how.' ', $v, false);
				}else{
					$this->db->where($this->_table_alias.'.'.$k.' '.$how.' ', $v, false);
				}
				
				
			}
			$joiner = $f->joiner;
		}
	}
}

