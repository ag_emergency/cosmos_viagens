<section class="item">
	<div class="content">

		<?php 
			if(!$haveParticipations){
				echo form_open_multipart();
			}

		?>
			<div class="form_inputs">
					<?if($haveParticipations){?>
					<p><b style="color:red">O passatempo tem participações, não pode ser editado.</b></p>
					<?}?>

					<ul>
				    	<?php
							$i = 0;
							foreach($module_fields as $f){
								
								//if field is the extra_resps, we need to build the html from json 
								if($f["field"] == "extra_resps"){
									if(isset($extraresps) && sizeof($extraresps)>0){
										
										// OPTIONS
										// ## '0'=>'Texto','1'=>'Múltiplas','4'=>'Múltiplas (3)','2'=>'Video URL','3'=>'Upload Imagem'
										
										// Template for '1'=>'Múltiplas (1)'
										?>
										<li id="option_type_1" class="" style="<?if($item->type != 1){?>display:none;<?}?>">
											<div>
											<p>Insira as opções de resposta nas caixas em baixo:</p>
											<?
											foreach($extraresps[1] as $index=>$field){
												?>
													<label for="f_create_extra_type1_resp_<?=$index?>"><?=$field["label"]?></label>
													<div class="input">
														<input type="text" class="<?=isset($field["rules"])?$field["rules"]:''?>" name="type1_resp_<?=$index?>" id="f_create_extra_type1_resp_<?=$index?>" data-error="O campo <?=$field["label"]?> é obrigatório" value='<?=isset($field["value"])?$field["value"]:''?>' style="width:50%;" />
													</div>
												<?
											}
											?>
											</div>
										</li>

										<?
										// Template for '4'=>'Múltiplas (3)'
										?>
										<li id="option_type_4" class="" style="<?if($item->type != 4){?>display:none;<?}?>">
											<div>
											<p><b>Insira as perguntas e respostas do Quiz:</b></p>
											
											<?
											foreach($extraresps[4] as $quizid=>$quiz){
												?><br /><br /><?
												foreach($quiz as $index=>$field){
													if($field["type"] == "textarea"){
														?>
														<label for="f_create_extra_type4_<?=$quizid?>_<?=$index?>"><?=$field["label"]?></label>
														<div class="input">
															<textarea name="type4_<?=$quizid?>_<?=$index?>" cols="20" rows="5" id="f_create_extra_type4_<?=$quizid?>_<?=$index?>" class="wysiwyg-advanced" data-error="O campo <?=$field["label"]?> é obrigatório"><?=isset($field["value"])?$field["value"]:''?></textarea>
														</div><br />
														<?
													}

													if($field["type"] == "text"){
														?>
														<label for="f_create_extra_type4_<?=$quizid?>_<?=$index?>"><?=$field["label"]?></label>
														<div class="input">
															<input type="text" class="<?=isset($field["rules"])?$field["rules"]:''?>" name="type4_<?=$quizid?>_<?=$index?>" id="f_create_extra_type4_<?=$quizid?>_<?=$index?>" data-error="O campo <?=$field["label"]?> é obrigatório" value='<?=isset($field["value"])?$field["value"]:''?>' style="width:50%;" />
														</div>
														<?
													}
												}
											}
											?>
											</div>
										</li>
										<?
									}
								}else{
									echo rdform_create($f,$item,$i);
									
								}
								if(!$f['_hidden']) $i++;
								
							}
						?>
					</ul>
				
			</div>
			<?if(!$haveParticipations){?>
			<div class="buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
			</div>
			<?}?>
		<?php if(!$haveParticipations){echo form_close();} ?>
	</div>
</section>