<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Quiz_m extends RD_Model {
	
	protected 	$modulename = 'quiz', // nome do modulo
				$tablename = '_questions'; // nome da tabela
	
	public function __construct()
	{

		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'ruhf',
								'type'=>'number'));

		$this->add_field(array(	'field'=>'challenge_id',
								'where'=>'uhf',
								'type'=>'number'));
 		
 		$this->add_field(array(	'field'=>'type',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Tipo',
								'values'=>array('0'=>'Texto','1'=>'Múltiplas (1)', '4'=>'Múltiplas (3)', '2'=>'Video URL','3'=>'Upload Imagem')));

 		$this->add_field(array(	'field'=>'question',
								'where'=>'cuf',
								'type'=>'htmlAdv',
								'label'=>'Questão',
								'rules'=>'skip_xss'));

 		$this->add_field(array(	'field'=>'extra_resps',
								'where'=>'uh',
								'type'=>'textarea',
								'label'=>'Extra_resps'));

 		$this->add_field(array(	'field'=>'extrafields',
								'where'=>'cuf',
								'type'=>'textarea',
								'label'=>'Campos Extra',
								'rules'=>'skip_xss'));

		/*$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));*/
								
		$this->add_field(array(	'field'=>'created_at',
								'where'=>'cuh',
								'type'=>'date',
								'default'=>date('Y-m-d H:i:s')));

		
		
		//enable tables without prefix /** BR */
		$this->db->dbprefix = null;
		

		//$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active'));
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}");
		//$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE));
		
		//$this->add_action_bottom('Export Excel', 'button',"admin/{$this->_module_name}/export/xls/{filter_key}" );
	}
	
	
	/*public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}*/
}