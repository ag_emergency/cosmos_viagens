jQuery(function($){
	$("select[name=type]").on("change",function(){
		//'0'=>'Texto','1'=>'Múltiplas','2'=>'Video URL','3'=>'Upload Imagem'
		//console.log($(this).val());
		$("#option_type_1").hide();
		$("#option_type_4").hide();
		$("#cke_f_create_question").parents("li").show();

		if($(this).val() == 1){
			$("#option_type_1").show();
		}

		if($(this).val() == 4){
			$("#option_type_4").show();
			$("#f_create_question").parents("li").hide();
		}
	}).change();
});

$("form:visible").data("cansubmit",false).bind("submit", function(e){
	if(!$("form:visible").data("cansubmit")){
		form_require_validation(e, this);	
	}
	return true;
});

function form_require_validation(e, form){
	e.stopPropagation();
	e.preventDefault();
	
	var errorMsg = "";
	pyro.clear_notifications();
	
	if($('input.required:visible').length){
		$('input.required:visible').each(function(i,iter){
			if(this.value == ""){
				errorMsg += "<p>" + $(this).data("error") + "</p>";
			}
		});
		if(errorMsg != ""){
			$("body").stop().animate({scrollTop:0}, '500', 'swing', function() {
				pyro.add_notification($('<div class="alert error">'+errorMsg+'</div>'));
			});
		}
		console.log(errorMsg);
	}

	if(errorMsg == ""){
		pyro.clear_notifications();
		$(form).data("cansubmit",true).submit();
	}
}