<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	
	public 	$modulename = 'quiz', // nome do modulo
			$plural = 'Questões', // item plural
			$single = 'Questão'; // item singular
	
	public function __construct(){		
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);

		// Load all required classes
		$this->load->helper('debug');
		//$this->template->set_layout(FALSE);
		//$this->template->set_layout('modal', 'admin');
		
		
		$this->template
				->append_js('module::question.js')
				->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				->set_layout('wysiwyg', 'admin')
				->set('title', $this->title)
				->set('single', $this->single)
				;

		//$this->template->enable_parser(true);
	}

	/*public function edit($challenge_id){
		
		$res = $this->db->get_where('_questions', array('challenge_id' => $challenge_id))->first_row();

		if(isset($_POST) && sizeof($_POST)>0){
			if(isset($_POST["type"]) && $_POST["type"] == 1){
				$resps = array();
				foreach($extra_resps as $index=>$resp){
					if(isset($_POST["resp_".$index])){
						$resps["resp_".$index] = $_POST["resp_".$index];
					}
				}
				$_POST["extra_resps"] =json_encode($resps);
			}
		}else{
			dump("ds");exit;
		}

		parent::edit($res->id);
	}*/


	public function edit($challenge_id)
	{
		
		$this->session->set_userdata('return_url', 'admin/challenges/edit/'.$challenge_id);
		//$this->session->set_userdata('tab', 'Quiz');

		//get question id from challenge id
		$res = $this->db->get_where('_questions', array('challenge_id' => $challenge_id))->first_row();
		
		//update parameter id with the question id
		$id = $res->id;

		$module = new $this->{$this->module_name.'_m'}();
		$id_rule = array( 	'field' => $module->get_PrimaryKey(),
							'label' => 'Primary Col',
							'rules' => 'required|is_numeric|trim' );

		array_push($this->validation_rules, $id_rule);

		//get the product we want to edit
		$item = $module->get($id);
		$this->data->item =& $item;

		//extra response for multiple option box
		$extra_resps = array(
			1=>array(
				1=>array("type"=>"text", "label"=>"Resposta 1 *", "rules"=>"required"),
				2=>array("type"=>"text", "label"=>"Resposta 2 *", "rules"=>"required"),
				3=>array("type"=>"text", "label"=>"Resposta 3 *", "rules"=>"required"),
				4=>array("type"=>"text", "label"=>"Resposta 4 (opcional)"),
				5=>array("type"=>"text", "label"=>"Resposta 5 (opcional)")
			),
			4=>array(
				1=>array(
					1=>array("type"=>"textarea", "label"=>"Questão 1 *", "rules"=>"required"),
					2=>array("type"=>"text", "label"=>"Questão 1: Resposta 1 *", "rules"=>"required"),
					3=>array("type"=>"text", "label"=>"Questão 1: Resposta 2 *", "rules"=>"required"),
					4=>array("type"=>"text", "label"=>"Questão 1: Resposta 3", "rules"=>"")
				),
				2=>array(
					1=>array("type"=>"textarea", "label"=>"Questão 2 *", "rules"=>"required"),
					2=>array("type"=>"text", "label"=>"Questão 2: Resposta 1 *", "rules"=>"required"),
					3=>array("type"=>"text", "label"=>"Questão 2: Resposta 2 *", "rules"=>"required"),
					4=>array("type"=>"text", "label"=>"Questão 2: Resposta 3", "rules"=>"")
				),
				3=>array(
					1=>array("type"=>"textarea", "label"=>"Questão 3 *", "rules"=>"required"),
					2=>array("type"=>"text", "label"=>"Questão 3: Resposta 1 *", "rules"=>"required"),
					3=>array("type"=>"text", "label"=>"Questão 3: Resposta 2 *", "rules"=>"required"),
					4=>array("type"=>"text", "label"=>"Questão 3: Resposta 3", "rules"=>"")
				)
			)
		);

		//put values in array in case of multioption type 1,
		// ## TYPE = 1 - MULTI OPTION (prepare info to template)
		if($item->type==1 || ($this->input->post('type') && $this->input->post('type') == 1)){
			//get values from database
			$resps = json_decode($item->extra_resps, true);

			//for each option try to get the value from database
			foreach($extra_resps[1] as $index=>$resp){
				
				//if has require on config add adicional validations to the form
				if(isset($resp["rules"])){
					//name of field 'resp_1', 'resp_2' ...
					$this->form_validation->set_rules("type1_resp_".$index, $resp["label"], $resp["rules"]);
				}
				
				//if index exists in database put value to template.
				if(isset($resps[$index])){
					$extra_resps[1][$index]["value"] = $resps[$index];
				}
			}
		}

		// ## TYPE = 4 - MULTI OPTION (prepare info to template)
		if($item->type==4 || ($this->input->post('type') && $this->input->post('type') == 4)){
			//get values from database
			$resps = json_decode($item->extra_resps, true);

			//for each option try to get the value from database
			foreach($extra_resps[4] as $quizid=>$quiz){
				foreach($quiz as $index=>$resp){
					//if has require on config add adicional validations to the form
					if(isset($resp["rules"])){
						//name of field 'type4_1_1', 'type_3_4' ...
						$this->form_validation->set_rules("type4_".$quizid."_".$index, $resp["label"], $resp["rules"]);
					}
					
					//if index exists in database put value to template.
					if(isset($resps[$quizid][$index])){
						$extra_resps[4][$quizid][$index]["value"] = $resps[$quizid][$index];
					}
				}
			}
		}

		//extraresps - variable to access in template to the info/config
		$this->data->extraresps = $extra_resps;
			
		if($this->session->userdata('return_url')){
			$this->_returnUrl = $this->session->userdata('return_url');
		}

		//get participations
		//in case of some participation block the editing quiz
		$this->db->select('count(_participations.id) as soma');
		$this->db->from('_participations');
		$this->db->where("_participations.challenge_id = ",$item->challenge_id);
		//$participations_list = $this->db->get()->results();
		$participations = $this->db->get()->row();
		$this->data->haveParticipations = false;
		if(isset($participations->soma) &&  $participations->soma > 0){
			$this->data->haveParticipations = true;
		}

		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		//prep data
		$this->data->module_fields = $this->{$this->module_name . '_m'}->get_fields('u');

		if($this->form_validation->run()){
			
			//insert data
			$data = $module->get_Input_Data('u');

			$data["extra_resps"] = null;

			//get multioption response and convert to single json
			// ## TYPE = 1 - MULTI OPTION (get info from template to store in database)
			if($this->input->post('type') && $this->input->post('type') == 1){
				//array to store the info from template
				$resps = array();
				foreach($extra_resps[1] as $index=>$resp){
					//get fields like 'resp_1', 'resp_2'
					if($this->input->post("type1_resp_".$index)){
						$resps[$index] = $this->input->post("type1_resp_".$index);
					}
				}
				//$data - store info in object linked to database (field extra_resps)
				$data["extra_resps"] = json_encode($resps);
			}

			// ## TYPE = 4 - MULTI OPTION (get info from template to store in database)
			if($this->input->post('type') && $this->input->post('type') == 4){
				//array to store the info from template
				$resps = array();
				foreach($extra_resps[4] as $quizid=>$quiz){
					foreach($quiz as $index=>$resp){
						//get fields like 'resp_1', 'resp_2'
						if($this->input->post("type4_".$quizid."_".$index)){
							$resps[$quizid][$index] = $this->input->post("type4_".$quizid."_".$index);
						}
					}
				}
				//$data - store info in object linked to database (field extra_resps)
				$data["extra_resps"] = json_encode($resps);
			}


			if($module->update($this->input->post('id'), $data, TRUE)){
				//success message
				$message = 'Entrada editada com sucesso.';
				$status = 'success';
				$this->session->set_flashdata($status, $message);
				if(empty($this->_returnUrl)){
					redirect('admin/'.$this->module_name);
				}else{
					if($this->session->userdata('return_url')){
						redirect($this->_returnUrl);
					}else{
						redirect('admin/'.$this->_returnUrl);
					}
				}
				//redirect($this->_get_last_page());
			}
			else{
				//failure message
				$message= 'Erro ao tentar editar.';
				$status = 'error';
				$this->session->set_flashdata($status, $message);
				$this->template->build('rd/edit', $this->data);
			}
		}else{
			
			if(isset($_SERVER['HTTP_REFERER']) && !$this->session->userdata('return_url')){
				$this->session->set_userdata('return_url', $_SERVER['HTTP_REFERER']);
				$this->_returnUrl = $_SERVER['HTTP_REFERER'];
			}
			if($this->input->post() != null){
				$this->session->set_flashdata("error", validation_errors());
				redirect($this->session->userdata('return_url'));
				exit;	
			}
		}

		
		// Load the view

		//if(validation_errors()){
		//	$this->session->set_flashdata("error", validation_errors());
		//	redirect($this->session->userdata('return_url'));
		//}else{
			$this->template
				->add_action('back', $this->single)
				->build('rd/edit', $this->data);	
		//}
			
		
	}

	
}