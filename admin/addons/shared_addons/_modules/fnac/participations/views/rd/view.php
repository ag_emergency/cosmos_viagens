<section class="item">
	<div class="content">

		<div class="form_inputs">
			
			<ul>
		    	<?php
					$i = 0;
					foreach($module_fields as $f){
						?>
						<label><?=$f["label"]?></label>
						<div class="input">
							<?
								switch($f["type"]){
									case "select": 
										if(isset($f["values"]) && isset($f["values"][$item->$f["field"]])){
											if(isset($f["module"])){
												echo "<a href='".base_url()."admin/".$f["module"]."/edit/".$item->$f["field"]."'>".$f["values"][$item->$f["field"]]."</a>"; break;	
											}else{
												echo $f["values"][$item->$f["field"]]; break;	
											}
										}
									case "json": 
										$list = json_decode($item->$f["field"]);
										?><fieldset><?
										foreach($list as $label=>$value){
											if($label == "type"){continue;}
											echo "<b>$label</b>: $value<br />";
										}
										?></fieldset><?
										break;
									case "multi":
										//especifico para este modulo (o type vem da base de dados)
										if(isset($item->type)){
											$html = "";
											$value = $item->$f["field"];
											switch($item->type){
												//text
												case 0: 
												//option
												case 1: $html .= $value; break;
												//video
												case 2: 
													$matches = array();
													$youtube_id = "";
													preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $value, $matches);
													if(isset($matches[0])){
														$youtube_id = $matches[0];
														$youtube_tumb = '<img src="http://img.youtube.com/vi/'.$youtube_id.'/default.jpg"/>';
													}
													$html.= '<a class="popup-video" target="_blank" href="'.$value.'">'.($youtube_id!==""?$youtube_tumb:'Video').'</a>'; break;
												//upload
												case 3: $html.= '<a class="image-popup-vertical-fit" target="_blank" href="'.base_url().'../uploads/'.$value.'"><img src="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path=../uploads/'.$value.'&width=100&height=60&crop=1"/></a>'; break;
												case 4:
													$resp_json = json_decode($value, true);
													$text = "";
													foreach($resp_json as $k=>$r){
														$text .= "<b>Resp ".$k."</b>: ".strip_tags($r)."; <br />";
													}
													if(is_array($resp_json)){
														$html .= '<div class="truncate">'.$text.'</div>'; 	
													}
													break;
															
											} echo $html; break;
										}
									default: echo $item->$f["field"]; break;
								}
							?>

						</div>
						<br />
						<?
						if(!$f['_hidden']) $i++;
						
					}
				?>
			</ul>
			
		</div>

	</div>
</section>

<script>
	$(function(){
		$('.image-popup-vertical-fit').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			}
		});
		$('.popup-video, .popup-vimeo, .popup-gmaps').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});
	});
</script>