<fieldset id="filters">
	<legend><?php echo lang('global:filters') ?></legend>
    
    <script type="text/javascript">
		var _filter = {},
			_filter_active = <?=json_encode($active_filters)?>;
			
		<?php 
		foreach($module_filters as $f){
			$html = replace_newline(addslashes(filter_create($f)));
			echo "_filter.master_{$f['field']} = '{$html}';\n";
		}
			$html = replace_newline(addslashes(filter_selector_create($module_filters)));
			echo "_filter.selector = '{$html}';\n";
			
			$html = replace_newline(addslashes(filter_joiner_create()));
			echo "_filter.joiner = '{$html}';\n";
		?>
    </script>
	<?php 
		if(isset($filterChallenge_id) && $filterChallenge_id>0){
			echo form_open('admin/'.$module_name.'/filter/id/'.$filterChallenge_id, 'id="filters_form"'); 	
		}else{
			echo form_open('admin/'.$module_name.'/filter/', 'id="filters_form"');
		}
	?>
    	<?=form_hidden('fq')?>
        <ul id="filters_list"></ul>
        <ul>
			<li class="">
				<?php 
				$this->load->view('rd/partials/buttons', array(	'buttons' => array('filter', 'cancel'), 
																'button_type' => 'secondary')) 
				?>
			</li>
		</ul>
	<?php echo form_close() ?>
</fieldset>