<section class="title">
	<h4>Editar <?=$single?></h4>
</section>

<section class="item">
	<div class="content">

		<?if(isset($tabs) && is_array($tabs) && sizeof($tabs)>0){?>
			<div class="tabs">
				<ul class="tab-menu">
					<li <?if(isset($tab) && $tab == "edit"){?>class="active"<?}?>><a href="#edit-page"><span>Edit</span></a></li>
					<?foreach($tabs as $tab_name=>$stab){?>
						<li <?if($tab == $tab_name){?>class="active"<?}?>><a href="<?=$stab["url"]?>"><span><?=$tab_name?></span></a></li>
					<?}?>
				</ul>

				<div id="edit-page" class="form_inputs">
					<?php echo form_open_multipart() ?>

					<fieldset>
						<ul>
					    	<?php
								$i = 0;
								foreach($module_fields as $f){
									echo rdform_create($f,$item,$i);
									if(!$f['_hidden']) $i++;
								}
							?>
						</ul>
					</fieldset>

					<div class="buttons">
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
					</div>
				</div>

				<?foreach($tabs as $tab_name=>$stab){
					if($stab["template"] != null){?>
					<div id="<?=$tab_name?>" class="form_inputs">
						<? $this->load->view($stab["template"]); ?>
					</div>
					<?}
				}?>
				
			</div>

		<?}else{?>

		<?php echo form_open_multipart() ?>
			<div class="form_inputs">
				
				<ul>
			    	<?php
						$i = 0;
						foreach($module_fields as $f){
							echo rdform_create($f,$item,$i);
							if(!$f['_hidden']) $i++;
						}
					?>
				</ul>
			</div>

			<div class="buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
			</div>
		<?php echo form_close() ?>
		
		<?}?>
	</div>
</section>