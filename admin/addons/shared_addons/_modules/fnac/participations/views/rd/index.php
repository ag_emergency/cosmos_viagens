<?php
// Actions Common
$hasBtnDelete = false;
if(count($module_actions_common) > 0){
	foreach($module_actions_common as $a){
		if($a['label'] == 'Remover') $hasBtnDelete = true;
	}
}
?>
<div class="one_full">
	<section class="title">
		<h4>Lista de <?=$title?></h4>
	</section>

	<section class="item">
		<div class="content">
			 <?php if ( ! empty($list)) : ?>
				<?php if(count($module_filters)) echo $this->load->view('rd/partials/filters') ?>
	
				 <?php echo form_open('admin/'.$module_name.'/delete/') ?>
					<div id="filter-stage">
						
                        <table cellspacing="0">
                            <thead>
                                <tr>
                                    <?php if($hasBtnDelete){?>
                                    	<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
									<?php } ?>
                                    <?php
									// Fields
									foreach($module_fields as $field){
										$label = $field['label'];
										echo "<th>{$label}</th>";
									}
									
									//Actions
									foreach($module_actions as $action){
										$label = $action['label'];
										echo "<th>{$label}</th>";
									}
									
									// Actions Common
									if(count($module_actions_common)>0){
										switch(count($module_actions_common)){
											case 3: 	$w = 210; break;
											case 2: 	$w = 170; break;
											default: 	$w = 70; break;
										}
										echo "<th width='$w'>Acções</th>";
									}
									?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list as $l) : ?>
                                    <tr>
                                    	<?php if($hasBtnDelete){?>
                                        	<td><?=form_checkbox('action_to[]', $l->id)?></td>
                                        <?php } ?>
                                        <?php
										// Fields
										foreach($module_fields as $f){
											$html = '';
											if(isset($f['display'])){
												$value = sformat($f['display'],$l);
											}else{
												$value = $l->{$f['field']};
											}
											
											//$value = _e($value);
											switch($f['type']){
												case 'info':
												case 'select':
													if(isset($f['url'])){
														$value = sformat($f['url'],$l);
													}
													if(isset($f['module'])){
														//echo $value;
														//echo $f['values'][$value];
														$text = isset($f['values'][$value]) ? $f['values'][$value] : $value;
														$html .= "<a href='".base_url()."admin/".$f['module']."/edit/".$value."'>$text</a>";
													}else{
														$html .= isset($f['values'][$value]) ? $f['values'][$value] : $value;	
													}
													break;
												case 'email':
													$extra = array('target'=>'_blank');
													$html .= mailto($value, $value, $extra);
													break;
												case 'link':
													$extra = array('target'=>'_blank');
													$url = $value;
													if(isset($f['display'])){ $url = $l->{$f['field']}; }
													$html .= !empty($value) ? anchor($url, $value, $extra) : '';
													break;
												case 'image':
													if(isset($l->$f['field']) && !empty($l->$f['field']) && isset($f['path'])){
														$html.= '<a class="fancybox" href="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path='.$f['path'].$value.'&width=500&height=500&crop=0"><img src="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path='.$f['path'].$value.'&width=100&height=60&crop=1"/></a>';
														break;
													}
												case 'src_image':
													$link = sformat($value,$l);
													$html.= '<img src="'.$link.'" width="30" height="30" />';
													break;
												case 'file':
													if(isset($l->$f['field']) && !empty($l->$f['field'])){
														$link = sformat($f['path'].$value,$l);
														if(isset($f['url'])){
															$value = sformat($f['url'],$l);
														}
														$extra = array('target'=>'_blank');
														$html .= anchor($link, $value, $extra);
														
													}else{
														$html .= 'No file';
													}
													break;
												case 'htmlAdv':
												case 'html':
													$html .= '<div class="truncate">'.strip_tags($value).'</div>'; 
													break;
												case 'datetime':
												case 'date':
													$html .= date('d-m-Y H:i',DateTime::createFromFormat('Y-m-d H:i:s', date($value))->getTimestamp());
													break;
												case 'textarea': $html .= '<div class="truncate">'.strip_tags($value).'</div>'; 
													break;
												case 'currency':
													$html .= number_format($value, 0, ',', '.').'€';
													break;
												case 'term':
													$html .= $value.' meses';
													break;

												case "json": 
													$list = json_decode($value);
													$html .= "<div class='truncate'>";
													foreach($list as $label=>$val){
														if($label == "type"){continue;}
														$html .=  "<b>$label</b>: $val<br />";
													}
													$html .= "</div>";

													break;

												case 'multi':
													//especifico para este modulo (o type vem da base de dados)
													switch($l->type){
														//text
														case 0: 
														//option
														case 1: //$html .= $value; break;
															$html .= '<div class="truncate">'.strip_tags($value).'</div>'; 
															break;
														//video
														case 2: 
															$matches = array();
															$youtube_id = "";
															preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $value, $matches);
															if(isset($matches[0])){
																$youtube_id = $matches[0];
																$youtube_tumb = '<img src="http://img.youtube.com/vi/'.$youtube_id.'/default.jpg" width="100" height="60"/>';
																//$youtube_tumb = '<img src="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path=http://img.youtube.com/vi/'.$youtube_id.'/default.jpg&width=100&height=60&crop=1" width="100" height="60"/>';
															}
															$html.= '<a class="popup-video" target="_blank" href="'.$value.'">'.($youtube_id!==""?$youtube_tumb:'Video').'</a>'; 
															break;
														//upload
														case 3: 
															$html.= '<a class="image-popup-vertical-fit" target="_blank" href="'.base_url().'../uploads/'.$value.'"><img src="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path=../uploads/'.$value.'&width=100&height=60&crop=1"/></a>'; 
															break;
														case 4: 
															$resp_json = json_decode($value, true);
															$text = "";
															foreach($resp_json as $k=>$r){
																$text .= "<b>Resp ".$k."</b>: ".strip_tags($r)."; <br />";
															}
															if(is_array($resp_json)){
																$html .= '<div class="truncate">'.$text.'</div>'; 	
															}
															break;

													}
													break;
												case 'text':
												default:
													$html .= $value; 
													break;
											}
											
											if(isset($f['link'])){
											   $extra = array('target'=>'_self');
											   $link = sformat($f['link'], $l);
											   $html = anchor($link, $html, $extra);
											}
											
											$style = ' ';
											if(isset($f['style'])){
											   $style = $f['style'];
										   }
											
											$html = "<td style='$style'>{$html}</td>";
											echo $html;
										}
										
										// Actions
										foreach($module_actions as $a){
											$html = action_create($a,$l,$primaryCol);
											$html = "<td>{$html}</td>";
											echo $html;
										}
										
										// Actions Common
										$btnDelete = false;
										if(count($module_actions_common)>0){
											$html = array();
											foreach($module_actions_common as $a){
												$html[] = action_create($a,$l,$primaryCol);
												if($a['label'] == 'Remover') $btnDelete = true;
											}
											echo '<td>'.implode(' ',$html).'</td>';
										}
										?>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    
                        <?php $this->load->view('admin/partials/pagination') ?>
                    
                        <br>
                    
                        <div class="table_action_buttons">
                            <?php if($btnDelete) $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))) ?>
                            <?
                            	/*foreach($actions_bottom as $a){
									echo action_create($a,$l,$primaryCol);
								}*/
							?>
                        </div>
                        
				 <?php echo form_close() ?>
			 <?php else : ?>
				<div class="no_data">Não existem resultados no momento.</div>
			 <?php endif ?>
		</div>
	</section>
</div>
<script>
	$(function(){
		<?if(isset($challenge_id) && $challenge_id>0){?>
		$('a.cancel').bind("click",function(e) {
			window.location.href = SITE_URL + 'admin/<?=$module_name?>/listByChallenge/id/<?=$challenge_id?>';
			e.preventDefault();
		});
		<?}?>

		$('.image-popup-vertical-fit').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			}
		});
		$('.popup-video, .popup-vimeo, .popup-gmaps').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});
	});
</script>
