<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	
	public 	$modulename = 'participations', // nome do modulo
			$plural = 'Participações', // item plural
			$single = 'Participação'; // item singular
	
	public function __construct(){
		
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);
			
		// Load all required classes
		$this->load->helper('debug');

		$this->template
				->append_css('rd/application.css')
				->append_js('module::magnificpopup.min.js')
				->append_css('module::magnific-popup.css')
				->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				->set_layout('default', 'rd')
				->set('title', $this->title)
				->set('single', $this->single);
	}

	/**
	 * List all existing client
	 *
	 * @author Actualsales
	 * @access public
	 * @return void
	 */
	public function index()
	{
		//no access
		//$this->template->add_action('export', "Exportar", $this->module_name.'/export/list/');
		$this->template->add_action('export', "Exportar", $this->module_name.'/exportlistByChallenge/');
		parent::lista(false);
	}

	/**
	 * View an existing item
	 *
	 * @author Actualsales
	 * @param id the ID to edit
	 * @access public
	 * @return void
	 */
	public function view($id)
	{
		$module = new $this->{$this->module_name."_m"}();
		$id_rule = array(
				'field' => $module->get_PrimaryKey(),
				'label' => "Primary Col",
				'rules' => 'required|is_numeric|trim'
			);
		array_push($this->validation_rules, $id_rule);

		//get the product we want to edit
		$item = $module->customGet($id);
		$this->data->item =& $item;

		//prep data
		$this->data->module_fields = $this->{$this->module_name . "_m"}->get_fields("i");

		// Load the view
		$this->template
					//->set_layout('wysiwyg', 'admin')
					->add_action('back', $this->single)
					->build('rd/view', $this->data);
	}


	/**
	 * List by Challenge existing client
	 *
	 * @author Actualsales
	 * @access public
	 * @return void
	 */
	public function listByChallenge($challenge_id = 0){

		$params = $this->uri->uri_to_assoc( $this->_uri_n );
		if($this->session->userdata('return_url')){
			$this->session->unset_userdata('return_url');
		}

		$filter_key = isset($params['filter']) ? $params['filter'] : NULL;
		$offset = isset($params['offset']) ? $params['offset'] : 0;
		$offset = ((int)$offset > 0 ? $offset : 1);
		
		$pag_index = isset($params['offset']) ? $this->uri->total_segments() : $this->uri->total_segments()+1;

		$challenge_id = isset($params['id']) ? $params['id'] : 0;
		$challenge_id = ((int)$challenge_id > 0 ? $challenge_id : 1);
		$this->data->challenge_id = $challenge_id;
		$this->load->helper('pagination');
		$m = $this->{$this->module_name.'_m'};

		$this->template->add_action('export', "Exportar", $this->module_name.'/exportlistByChallenge/id/'.$challenge_id);

		$filter = $this->_get_filter($filter_key);
		$filter_uri = $filter ? "/filter/{$filter_key}" : '';

		$res = $m->get_allByChallenge($challenge_id, $this->numrows, $offset * $this->numrows - $this->numrows, $filter);
		$count = $m->count_allByChallenge($challenge_id, $filter);

		#$this->data->nrecords = $count;

		if($count){
			$this->data->pagination = create_pagination('admin/'.$this->module_name.'/listByChallenge/id/'.$challenge_id.'/'.$filter_uri.'/offset/', $count, $this->numrows,$pag_index);
			$this->data->list =& $res;
		}

		$this->session->set_userdata('offset', $offset);
		$this->data->offset = $offset;
		
		$this->data->filterChallenge_id = $challenge_id;

		$this->data->module_fields = $m->get_fields('r');
		$this->data->module_filters = $m->get_filters();
		$this->data->primaryCol = $m->get_primaryKey();
		$this->data->module_actions = $this->{$this->module_name.'_m'}->get_actions();
		$this->data->module_actions_common = $this->{$this->module_name.'_m'}->get_actions_common();
		$this->data->module_actions_bottom = $this->{$this->module_name.'_m'}->get_actions_bottom();


		$this->data->active_filters = $filter===FALSE ? array() : $filter;

		$this->data->filter_key = $filter_key;
		

		$this->template
				->append_css('rd/application.css')
				->append_js('module::magnificpopup.min.js')
				->append_css('module::magnific-popup.css')
				->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				->set_layout('default', 'rd')
				->add_action('back', $this->single, 'challenges')
				->set('title', $this->title)
				->set('single', $this->single)
				->build('rd/index', $this->data);
				//$template['actions']['back']['uri']
			//dump($this->template);
	}

	public function filter()
	{
		$params = $this->uri->uri_to_assoc( $this->_uri_n );
		$challenge_id = 0;
		if(isset($params["id"])){
			$challenge_id = (int)$params["id"];
		}

		$filter_json = $this->input->post('fq');
		$filter = json_decode($filter_json);
		$filter_key = sha1($filter_json);
		$_SESSION['filter_'.$filter_key] = $filter;
		
		if($challenge_id>0){
			redirect('admin/'.$this->module_name.'/listByChallenge/id/'.$challenge_id.'/filter/'.$filter_key);	
		}else{
			redirect('admin/'.$this->module_name.'/lista/filter/'.$filter_key);
		}
		
	}

	public function exportlistByChallenge($challenge_id = 0)
	{

		$exclude = array('ip', 'agent');


		$params = $this->uri->uri_to_assoc( $this->_uri_n );
		$challenge_id = 0;
		if(isset($params["id"])){
			$challenge_id = (int)$params["id"];
		}
		$m = $this->{$this->module_name.'_m'};


		if($challenge_id > 0){
			$data = $m->get_allByChallenge($challenge_id, 0, 0, false);
		}else{
			$data = $m->get_all(0, 0, false);
		}

		$type = 0;
		if(sizeof($data)>0){
			$type = isset($data[0]->type)?(int)$data[0]->type:0;
		}



		//$data = $this->query_all('e',$filter, false)->result();
		$fields = $m->get_fields();

		$html = '<TABLE border="1">';
		$header = 0;
		$line_number = 0;
		foreach($data as $line){
			if($header == 0){
				$html .= '<TR>';


				foreach($line as $key=>$cell){
					if(!in_array($key, $exclude)){ // apenas imprime os que não estão no array de exclusão
						
						if(!isset($fields[$key]["type"])){
							continue;
						}

						if($challenge_id>0 && $key=='extra_fields'){
							/* 
								para o caso de estarmos a imprimir uma lista de um passatempo especifico
								vamos dividir os campos "extra_fields" por colunas
							*/
							$list = json_decode($cell);
							foreach($list as $k=>$v){
								$html .= '<TH>';
								$html .= ucwords($k);
								$html .= '</TH>';
							}
							
						}else if($challenge_id>0 && $key=='response' && $line->type == 4){
							$html .= '<TH>Resposta 1</TH>';
							$html .= '<TH>Resposta 2</TH>';
							$html .= '<TH>Resposta 3</TH>';
						}
						else{

							// caso contrário colocamos numa só coluna
							$html .= '<TH>';
							if(isset($fields[$key]['label'])){
								$html .= _e($fields[$key]['label']);
							}else{
								$html .= _e($key);
							}
							$html .= '</TH>';
						}
					}
				}

				

				$html .= '</TR>';
				$header = 1;
			}

		

			$html .= '<TR>';
			$line_number++;
			foreach($line as $key=>$cell){
				if(!in_array($key, $exclude)){ // apenas imprime os que não estão no array de exclusão

					if(!isset($fields[$key]["type"])){
						continue;
					}

					$style='';
					if($line_number%2 != 0) $style = 'background:#eee;';
					
					if($challenge_id>0 && $key=='extra_fields'){
						/* 
							para o caso de estarmos a imprimir uma lista de um passatempo especifico
							vamos dividir os campos "extra_fields" por colunas
						*/
						$list = json_decode($cell);
						foreach($list as $k=>$v){
							$html .= '<TD valin="middle" style="'.$style.'">';
							$html .= $v;
							$html .= '</TD>';
						}
						
					}
					else if($challenge_id>0 && $key=='response' && $line->type == 4){
						$list = json_decode($cell);
						foreach($list as $k=>$v){
							$html .= '<TD valin="middle" style="'.$style.'">';
							$html .= $v;
							$html .= '</TD>';
						}
					}
					else{

						$html .= '<TD valin="middle" style="'.$style.'">';
				
						if(isset($fields[$key]) && $fields[$key]['type'] == 'select'){
							if(isset($fields[$key]['values'][$cell])){
								$html .= _e($fields[$key]['values'][$cell]);
							}else{
								$html .= '-';
							}
						}else{
							$value = $cell;
							
							switch($fields[$key]["type"]){
								case "json": 
									$list = json_decode($value);
									$html .= "";
									foreach($list as $label=>$val){
										if($label == "type"){continue;}
										$html .=  "<b>$label</b>: $val | ";
									}
									$html .= "";
									break;

								case 'multi':
									$type = $line->type;
									
									//especifico para este modulo (o type vem da base de dados)
									switch($type){
										//text
										case 0: 
										//option
										case 1: //$html .= $value; break;
											$html .= strip_tags($value); 
											break;
										//video
										case 2: 
											$matches = array();
											$youtube_id = "";
											preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $value, $matches);
											if(isset($matches[0])){
												$youtube_id = $matches[0];
												$youtube_tumb = '<img src="http://img.youtube.com/vi/'.$youtube_id.'/default.jpg" width="100" height="60"/>';
												//$youtube_tumb = '<img src="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path=http://img.youtube.com/vi/'.$youtube_id.'/default.jpg&width=100&height=60&crop=1" width="100" height="60"/>';
											}
											$html.= '<a class="popup-video" target="_blank" href="'.$value.'">Video</a>'; 
											break;
										//upload
										case 3: 
											$html.= '<a class="image-popup-vertical-fit" target="_blank" href="'.base_url().'../uploads/'.$value.'">Ver Imagem</a>'; 
											break;
										case 4:
											$resp_json = json_decode($value, true);
											$text = "";
											foreach($resp_json as $k=>$r){
												$text .= "<b>Resp ".$k."</b>: ".strip_tags($r)."; <br />";
											}
											if(is_array($resp_json)){
												$html .= $text;
											}
											break;

									}
									break;
								
								default: $html .= _e($cell);
							}
						}
					
						$html .= '</TD>';
					}

				} // in_array
			} // foreach cell
			$html .= '</TR>';
		} // foreach line
		$html .= '</TABLE>';
		

		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        header("Content-disposition: attachment; filename=listagem.xls");
		echo utf8_decode($html);
		exit;
	}

}