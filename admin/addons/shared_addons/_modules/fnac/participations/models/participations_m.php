<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Participations_m extends RD_Model {
	
	protected 	$modulename = 'participations', // nome do modulo
				$tablename = '_participations'; // nome da tabela
	
	public function __construct()
	{

		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uhf',
								'type'=>'number'));

		$this->add_field(array( 'field'=>'challenge_id',
                                'where'=>'r',
                                'type'=>'select',
                                'module'=>'challenges',
                                'label'=>'Passatempo',
                                'rules'=>'required',
                                //'url'=>'admin/admin/challenges/edit/{id}',
                                'values'=>$this->dropdown(
                                            'id',
                                            'title',
                                            array(  'table'=>'_challenges',
                                                    'where'=>'active = 1',
                                                    'sort_by'=>'title')) ));

		$this->add_field(array( 'field'=>'user_id',
                                'where'=>'rf',
                                'type'=>'select',
                                'module'=>'fbusers',
                                'label'=>'Utilizador',
                                'rules'=>'required',
                                'values'=>$this->dropdown(
                                            'id',
                                            'name',
                                            array(  'table'=>'_users',
                                                    'where'=>'active = 1',
                                                    'sort_by'=>'name')) ));

		$this->add_field(array(	'field'=>'response',
								'where'=>'rf',
								'type'=>'multi',
								'label'=>'Resposta',
								'rules'=>'required',
								'style'=>'width:200px;'));
		
		$this->add_field(array(	'field'=>'extra_fields',
								'where'=>'rf',
								'type'=>'json',
								'label'=>'Campos Extra',
								'rules'=>'required'));
		
		$this->add_field(array(	'field'=>'ip',
								'where'=>'uf',
								'type'=>'text',
								'label'=>'IP',
								'rules'=>''));

		$this->add_field(array(	'field'=>'agent',
								'where'=>'uf',
								'type'=>'textarea',
								'label'=>'Browser',
								'rules'=>''));

		$this->add_field(array(	'field'=>'is_public',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Público',
								'default'=>'0',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));

		$this->add_field(array(	'field'=>'is_winner',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Vencedor',
								'default'=>'0',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));

		$this->add_field(array(	'field'=>'created_at',
								'where'=>'rf',
								'type'=>'datetime',
								'label'=>'Data de Criação',
								'rules'=>''));
								
		
		
		//enable tables without prefix /** BR */
		$this->db->dbprefix = null;
		

		$this->add_action('Público','toggle',"admin/{$this->_module_name}/action/publicToggle/{id}",array('col'=>'is_public'));
		$this->add_action('Vencedor','toggle',"admin/{$this->_module_name}/action/winnerToggle/{id}",array('col'=>'is_winner'));
		//$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}");
		$this->add_action_common('View','button',"admin/{$this->_module_name}/view/{id}");
		//$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE));
		
		$this->add_action_bottom('Export Excel', 'button',"admin/{$this->_module_name}/export/xls/{filter_key}" );
	}
	
	
	public function publicToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('is_public', '1-is_public', false);
		return $this->db->update($this->_table);
	}

	public function winnerToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('is_winner', '1-is_winner', false);
		return $this->db->update($this->_table);
	}


	public function customGet($id){
		$this->db->select('_participations.*, _questions.type');
		$this->db->from('_participations');
		$this->db->join("_challenges", "_challenges.id = _participations.challenge_id");
		$this->db->join("_questions", "_challenges.id = _questions.challenge_id");
		$this->db->where("_participations.id = ",$id);
		$result = $this->db->get()->row();
       return $result;
   }

	public function get_all($limit = 0, $offset = 0, $filters = FALSE){
		$this->set_filters($filters);
		$this->db->select('_participations.*, _questions.type');
		if($limit>0){
			$this->db->limit($limit, $offset);
		}
		$this->db->from('_participations');
		$this->db->join("_challenges", "_challenges.id = _participations.challenge_id");
		$this->db->join("_questions", "_challenges.id = _questions.challenge_id");
		$this->db->order_by("_participations.id desc");
		$result = $this->db->get()->result();
		//dump($result);
		//dump($this->db->last_query());
		//dump($result);
		//exit;
		//dump($this->db->get_compiled_select());exit;
       return $result;//$this->db->get($this->_table)->result();
   }

	public function count_all($filters=FALSE){
		$this->set_filters($filters);
        return $this->db->count_all_results($this->_table);
    }


    public function get_allByChallenge($challenge_id = 0, $limit = 0, $offset = 0, $filters = FALSE){
		$this->set_filters($filters);
		$this->db->select('_participations.*, _questions.type');
		if($limit>0){
			$this->db->limit($limit, $offset);
		}
		$this->db->from('_participations');
		$this->db->join("_challenges", "_challenges.id = _participations.challenge_id");
		$this->db->join("_questions", "_challenges.id = _questions.challenge_id");
		$this->db->where("_challenges.id = ", $challenge_id);
		$this->db->order_by("_participations.id desc");
		$result = $this->db->get()->result();
		//dump($result);
		//dump($this->db->last_query());
		//dump($result);
		//exit;
		//dump($this->db->get_compiled_select());exit;
       return $result;//$this->db->get($this->_table)->result();
   }

   public function count_allByChallenge($challenge_id, $filters=FALSE){
		$this->set_filters($filters);
		$this->db->from('_participations');
		$this->db->select('_participations.id');
		$this->db->join("_challenges", "_challenges.id = _participations.challenge_id");
		$this->db->join("_questions", "_challenges.id = _questions.challenge_id");
		$this->db->where("_challenges.id = ", $challenge_id);
        return $this->db->count_all_results();
    }
    
}