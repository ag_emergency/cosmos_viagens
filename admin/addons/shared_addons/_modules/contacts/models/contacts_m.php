<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Contacts_m extends RD_Model {

	protected 	$modulename = 'contacts', // nome do modulo
				$tablename = '_contacts'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(
				'field'=>'id',
				'where'=>'uh',
				'type'=>'number'));

		$this->add_field(array(
				'field'=>'name',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Nome',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'phone',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Telefone',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'message',
				'where'=>'cruf',
				'type'=>'html',
				'label'=>'Mensagem',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'email',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Email',
				'rules'=>'required'));


		$this->add_field(array(
				'field'=>'created_date',
				'where'=>'cruhf',
				'type'=>'date',
				'default'=>date('Y-m-d H:i:s')));

		$this->add_field(array(	'field'=>'active',
								'where'=>'rcuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));

		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		//$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url
		#$this->add_action_common('Exportar','link',"admin/{$this->_module_name}/export/{id}"); // accao, tipo, url

	}

}