<?php
// Actions Common
$hasBtnDelete = false;
if(count($module_actions_common) > 0){
	foreach($module_actions_common as $a){
		if($a['label'] == 'Remover') $hasBtnDelete = true;
	}
}
?>
<div class="one_full">
	<section class="title">
		<h4>Lista de <?=$title?></h4>
	</section>

	<section class="item">
		<div class="content">
			 <?php if ( ! empty($list)) : ?>
				<?php if(count($module_filters)) echo $this->load->view('rd/partials/filters') ?>
	
				 <?php echo form_open('admin/'.$module_name.'/delete/') ?>
					<div id="filter-stage">
						
                        <table cellspacing="0">
                            <thead>
                                <tr>
                                    <?php if($hasBtnDelete){?>
                                    	<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
									<?php } ?>
                                    <?php
									// Fields
									foreach($module_fields as $field){
										$label = $field['label'];
										echo "<th>{$label}</th>";
									}
									
									//Actions
									foreach($module_actions as $action){
										$label = $action['label'];
										echo "<th>{$label}</th>";
									}
									
									// Actions Common
									if(count($module_actions_common)>0){
										echo '<th width="180">Acções</th>';
									}
									?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list as $l) : ?>
                                    <tr <?php if($l->estado==1)echo'class="dark"';?> >
                                    	<?php if($hasBtnDelete){?>
                                        	<td><?=form_checkbox('action_to[]', $l->id)?></td>
                                        <?php } ?>
                                        <?php
										// Fields
										foreach($module_fields as $f){
											$html = '';
											if(isset($f['display'])){
												$value = sformat($f['display'],$l);
											}else{
												$value = $l->{$f['field']};
											}
											
											//$value = _e($value);
											
											switch($f['type']){
												case 'info':
												case 'select':
													if(isset($f['url'])){
															$value = sformat($f['url'],$l);
														}
													$html .= isset($f['values'][$value]) ? $f['values'][$value] : $value;
													break;
												case 'email':
													$extra = array('target'=>'_blank');
													$html .= mailto($value, $value, $extra);
													break;
												case 'link':
													$extra = array('target'=>'_blank');
													$url = $value;
													if(isset($f['display'])){ $url = $l->{$f['field']}; }
													$html .= !empty($value) ? anchor($url, $value, $extra) : '';
													break;
												case 'image':
													if(isset($l->$f['field']) && !empty($l->$f['field']) && isset($f['path'])){
														$html.= '<a class="fancybox" href="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path='.$f['path'].$value.'&width=500&height=500&crop=0"><img src="'.base_url().'admin/'.MODULE_NAME.'/thumb/?path='.$f['path'].$value.'&width=100&height=60&crop=1"/></a>';
														break;
													}
												case 'file':
													if(isset($l->$f['field']) && !empty($l->$f['field'])){
														$link = sformat($f['path'].$value,$l);
														if(isset($f['url'])){
															$value = sformat($f['url'],$l);
														}
														$extra = array('target'=>'_blank');
														$html .= anchor($link, $value, $extra);
														
													}else{
														$html .= 'No file';
													}
													break;
												case 'htmlAdv':
												case 'html':
													$html .= '<div class="truncate">'.strip_tags($value).'</div>'; 
													break;
												case 'datetime':
												case 'date':
													$html .= format_date($value);
													break;
												case 'textarea': $html .= '<div class="truncate">'.strip_tags($value).'</div>'; 
													break;
												case 'text':
												default:
													$html .= $value; 
													break;
											}
											
											if(isset($f['link'])){
											   $extra = array('target'=>'_self');
											   $link = sformat($f['link'], $l);
											   $html = anchor($link, $html, $extra);
											}
											
											$style = ' ';
											if(isset($f['style'])){
											   $style = $f['style'];
										   }
											
											$html = "<td style='$style'>{$html}</td>";
											echo $html;
										}
										
										// Actions
										foreach($module_actions as $a){
											$html = action_create($a,$l,$primaryCol);
											$html = "<td>{$html}</td>";
											echo $html;
										}
										
										// Actions Common
										$btnDelete = false;
										if(count($module_actions_common)>0){
											$html = array();
											foreach($module_actions_common as $a){
												$html[] = action_create($a,$l,$primaryCol);
												if($a['label'] == 'Remover') $btnDelete = true;
											}
											echo '<td>'.implode(' ',$html).'</td>';
										}
										?>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    
                        <?php $this->load->view('admin/partials/pagination') ?>
                    
                        <br>
                    
                        <div class="table_action_buttons">
                            <?php if($btnDelete) $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))) ?>
                        </div>
					</div>
				 <?php echo form_close() ?>
			 <?php else : ?>
				<div class="no_data">Não existem resultados no momento.</div>
			 <?php endif ?>
		</div>
	</section>
</div>
