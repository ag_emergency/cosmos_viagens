<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Types_m extends RD_Model {

	protected 	$modulename = 'types', // nome do modulo
				$tablename = '_products_types'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(
				'field'=>'id',
				'where'=>'uh',
				'type'=>'number'));

		$this->add_field(array(
				'field'=>'title',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Título',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'slug',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Slug',
				'rules'=>''));

		$this->add_field(array(	'field'=>'active',
								'where'=>'rcuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));

		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		//$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url
		#$this->add_action_common('Exportar','link',"admin/{$this->_module_name}/export/{id}"); // accao, tipo, url

	}

	public function get_all($limit=0, $offset=0, $filters=FALSE){
        $this->db->order_by('title ASC');
        return parent::get_all($limit,$offset,$filters);
    }
    

}