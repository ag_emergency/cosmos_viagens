<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Destaques_m extends RD_Model {
	
	protected 	$modulename = 'destaques', // nome do modulo
				$tablename = '_destaques'; // nome da tabela
	
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'titulo', 
								'where'=>'rcu',
								'type'=>'text',
								'label'=>'Título',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'texto', 
								'where'=>'cu',
								'type'=>'textarea',
								'label'=>'Texto',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'link', 
								'where'=>'rcu',
								'type'=>'text',
								'label'=>'Link do destaque',
								'rules'=>'required'));
							
		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		
	}
	
}