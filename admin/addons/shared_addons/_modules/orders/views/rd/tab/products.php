<div class="form_inputs" id="user-details-tab">
    <fieldset id="address">
        <h2>Detalhe da Encomenda</h2>
        <p>Segue a lista de todos os produtos adicionados à encomenda.</p>

        <fieldset>
            <p>
                <b>Nome: </b> <?= $user->name?><br />
                <b>Email: </b> <?= $user->email?><br />
                <b>Telefone: </b> <?= $user->phone?><br />
                <b>NIF: </b> <?= $user->nif?>
                <br /><br />

                <b>Morada de Entrega</b><br />
                    <?= $item->delivery_address?>
                    <br /><br />
                <b>Morada de Faturação</b><br />
                    <?= $item->billing_address?>
                    <br /><br />

                <table width="" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>Valor Bruto</th>
                            <th>Valor Entrega</th>
                            <th>Valor Descontos</th>
                            <th>Valor Total</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr class="even">
                            <td>€ <?= round($item->order_price,2) ?></td>
                            <td>€ <?= round($item->order_shipping,2) ?></td>
                            <td>€ <?= round($item->order_discount,2) ?></td>
                            <td>€ <?= round($item->order_total,2) ?></td>
                        </tr>
                    </tbody>
                </table>

                <!--<b>Valor Bruto</b><br />
                <?= $item->order_price?>
                    <br /><br />

                <b>Valor Entrega</b><br />
                <?= $item->order_shipping?>
                    <br /><br />

                <b>Valor Descontos</b><br />
                <?= $item->order_discount?>
                    <br /><br />

                <b>Valor Total</b><br />
                <?= $item->order_total?>
                    <br /><br />-->

            </p>
        </fieldset>

        <h2>Produtos</h2>

        <ul>
            <li class="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>Referência</th>
                        <th>Nome</th>
                        <th>Quantidade</th>
                        <th>Promoção</th>
                        <th>Taxa</th>
                        <th>Preço</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?if(isset($list)){?>
                        <? foreach($list as $key=>$item){ ?>
                        <tr class="even">
                            <td><?= $item->reference ?></td>
                            <td><?= $item->name ?></td>
                            <td><?= $item->qtd ?></td>
                            <td><?= $item->promo ?></td>
                            <td><?= $item->tax ?></td>
                            <td><?= $item->price ?></td>
                        </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </li>
        </ul>
    </fieldset>
</div>

<style media="print">
    body{background:none;}
    header, footer{display:none;}
    #container{margin:0;width:100%;}
    section.title{display:none;}
    section.item{
        border: 0;
        float: none;
        box-shadow: none;
    }
    .tab-menu{display:none;}
    section#content{margin: 0!important;}
</style>