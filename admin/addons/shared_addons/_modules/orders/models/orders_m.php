<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Orders_m extends RD_Model {

	protected 	$modulename = 'orders', // nome do modulo
				$tablename = '_orders'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(
				'field'=>'id',
				'where'=>'uh',
				'type'=>'number'));

		$this->add_field(array("field"=>"user_id","label"=>"Utilizador","type"=>"select",
			"values"=>$this->dropdown("id","name",array("table"=>"default__users","where"=>"1=1","sort_by"=>"name","first_empty"=>false))));

		$this->add_field(array(
				'field'=>'billing_address',
				'where'=>'cuf',
				'type'=>'textarea',
				'label'=>'Morada de Faturação',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'delivery_address',
				'where'=>'cuf',
				'type'=>'textarea',
				'label'=>'Morada de Entrega',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'order_price',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Valor Bruto',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'order_shipping',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Valor Entrega',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'order_discount',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Valor Descontos',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'order_total',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Valor Total',
				'rules'=>'required'));


		$this->add_field(array( 'field'=>'status',
                                'where'=>'rcuf',
                                'type'=>'select',
                                'label'=>'Estado',
                                'default'=>'1',
								'values'=>array('0'=>'Desactivado',
                                                '1'=>'Em Validação', '2'=>'Pago', '3'=>'Entregue')));

		$this->add_field(array( 'field'=>'active',
                                'where'=>'rcuf',
                                'type'=>'select',
                                'label'=>'Active',
                                'default'=>'1',
                                'values'=>array('1'=>'Sim',
                                                '0'=>'Não')));

		/*$this->add_field(
				array(
					'field'=>'status',
					'where'=>'rcuf',
					'type'=>'select',
					'label'=>'Estado',
					'default'=>'0',
					'values'=>array(
						0 =>'Em Validação'
						1 =>'Em Expedição',
						2 =>'Entregue'
					)
				)
			);

		$this->add_field(
				array(
					'field'=>'active',
					'where'=>'rcuf',
					'type'=>'select',
					'label'=>'Estado',
					'default'=>'1',
					'values'=>array(
						'1'=>'Sim',
						'0'=>'Não'
					)
				)
			);*/

		$this->add_field(
				array(
					'field'=>'created_date',
					'where'=>'f',
					'type'=>'select',
					'label'=>'Estado',
					'default'=>'1',
					'values'=>array(
						'1'=>'Sim',
						'0'=>'Não'
					)
				)
			);

		// adicionar links para accoes
		$this->add_action('Activo','toggle',"admin/orders/changeActive/{id}",array('col'=>'active'));
		$this->add_action_common('Detalhe','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		#$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url
		#$this->add_action_common('Exportar','link',"admin/{$this->_module_name}/export/{id}"); // accao, tipo, url

	}

	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', FALSE);
		$res =  $this->db->update("default__orders");
		return $res;
	}

}


/*$this->add_field(array("field"=>"colaborador_id","label"=>"Colaborador","type"=>"select",
			"values"=>$this->dropdown("id","nome",array("table"=>"colaboradores","where"=>"activo=1","sort_by"=>"nome","first_empty"=>false))));

		$this->add_field(array("field"=>"candidatos", "label"=>"Candidaturas", "where"=>"rf", "type"=>"number"));

		$this->add_field(array("field"=>"homepage","label"=>"Homepage", "where"=>"rf","type"=>"select","default"=>"1","values"=>array("1"=>"Sim","0"=>"Não")));
		$this->add_field(array("field"=>"activo","label"=>"Activo", "notwhere"=>"r","type"=>"select","default"=>"1","values"=>array("1"=>"Sim","0"=>"Não")));
		$this->add_field(array("field"=>"data_criacao", "where"=>"crfh", "label"=>"Data de Criação", "type"=>"datetime", "default"=>date("Y-m-d H:i:s")));

		$this->add_action('Activo','toggle',"admin/{$this->_table}/changeActive/{id}",array('col'=>'activo'));
		$this->add_action_common('Editar','link',"admin/{$this->_table}/edit/{id}");
		if($this->user->group != "user"){
			$this->add_action_common('Remover','link',"admin/{$this->_table}/delete/{id}",array("confirm"=>TRUE));
		}
		*/