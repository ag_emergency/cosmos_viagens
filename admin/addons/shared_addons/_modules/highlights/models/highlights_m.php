<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Highlights_m extends RD_Model {
	
	protected 	$modulename = 'highlights', // nome do modulo
				$tablename = '_highlights'; // nome da tabela
			
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'name', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Nombre',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'text', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Texto',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'image_url', 
								'where'=>'rcu',
								'label'=>'Imagen (298x188)', 
								'path'=>'../uploads/highlights/', 
								'type'=>'image', 
								'url'=>'Download', 
								'allowed_types'=>'jpg|jpeg|gif|png'));

		$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sí',
												'0'=>'No')));
							
		
		// adicionar links para accoes
		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active'));
		
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}");
		
	}
	
	public function get_all($limit=0, $offset=0, $filters=FALSE){
		$this->db->order_by('active ASC');
		return parent::get_all($limit,$offset,$filters);
  	}
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}
	
}