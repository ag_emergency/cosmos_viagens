<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	public 	$modulename = 'simulacoes', // nome do modulo
			$plural = 'Simulações', // item plural
			$single = 'Simulação'; // item singular
			
	public function __construct()
	{
		
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);
			
		$this->data->products = array(	1 => 'Crédito Pessoal', 
										2 => 'Crédito Automóvel' );
		$this->data->tabs = array(  'Detalhes' => 'edit',
									'Histórico' => 'historico' );
		// Load all required classes
		$this->load->helper('debug');
	}
	
	public function lista($btnAdd=true)
	{
		parent::lista(false); // para bloquear o botao de adicionar
	}
	
	public function edit($id)
	{
		$this->data->tab = 'edit';
		parent::edit($id);
	}
	
	public function historico($id)
	{
		$this->data->tab = 'historico';
		$module = new $this->{$this->modulename.'_m'}();
		$item = $module->get($id);
		$this->data->item =& $item;
		
		$this->db->where('email',$this->data->item->email);
		$this->db->order_by('dt ASC, ordem ASC');
		$this->data->lista = $this->db->get('_simulacoes')->result();
		$this->template->build('rd/edit', $this->data);
	}
	
}