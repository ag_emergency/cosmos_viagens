<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Simulacoes_m extends RD_Model {
	
	protected 	$modulename = 'simulacoes', // nome do modulo
				$tablename = '_simulacoes'; // nome da tabela
	
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'nome', 
								'where'=>'ruhf',
								'type'=>'text',
								'label'=>'Nome',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'email', 
								'where'=>'uhf',
								'type'=>'email',
								'label'=>'Email',
								'rules'=>'required|valid_email'));
								
		$this->add_field(array(	'field'=>'telefone',
								'where'=>'uhf',
								'type'=>'text',
								'label'=>'Telefone',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'produto' 
								,'where'=>'uhf'
								,'type'=>'number'
								,'label'=>'Produto'
								,'rules'=>'required|integer'));
								
		$this->add_field(array(	'field'=>'plano',
								'where'=>'uhf',
								'type'=>'select',
								'label'=>'Plano',
								'default'=>'V',
								'values'=>array('V'=>'Plano Proteção Vida',
												'P'=>'Plano Proteção Total')));
								
		$this->add_field(array(	'field'=>'prazo',
								'where'=>'ruhf',
								'type'=>'term',
								'label'=>'Prazo',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'montante',
								'where'=>'ruhf',
								'type'=>'currency',
								'label'=>'Montante',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'tan',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'TAN',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'taeg',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'TAEG',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'prestacao',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'Prestação',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'portes',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'Portes',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'mtic',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'MTIC',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'is',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'IS',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'tot_creditar',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'Montante a Creditar',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'total',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'Montante Total',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppv',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'PPV',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppv_mes',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'PPV Mês',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppf',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'PPV',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppf_mes',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'PPF Mês',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'garantia',
								'where'=>'uh',
								'type'=>'select',
								'label'=>'Garantia',
								'values'=>array('Não aplicável'=>'Não aplicável',
												'Livrança'=>'Livrança')));
								
		$this->add_field(array(	'field'=>'juro_mora',
								'where'=>'uh',
								'type'=>'number',
								'label'=>'Juro Mora',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'dt',
								'where'=>'ruhf',
								'type'=>'date',
								'label'=>'Data de registo',
								'default'=>date('Y-m-d H:i:s')));
								
		$this->add_field(array(	'field'=>'estado',
								'where'=>'uf',
								'type'=>'select',
								'label'=>'Lido',
								'default'=>'0',
								'values'=>array('1'=>'Sim',
												'0'=>'Não')));
												
		// adicionar accoes especificas
		$this->add_action('Contactado','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'estado')); // title, tipo, url, coluna
							
		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE));
		
	}
	
	public function get_all($limit=0, $offset=0, $filters=FALSE){
		$this->db->group_by('email');
		$this->db->where('last',1);
		$this->db->order_by('dt ASC, last ASC');
		return parent::get_all($limit,$offset,$filters);
  	}
	
	public function count_all($filters=FALSE){
		$this->db->where('last',1);
        return parent::count_all($filters);
    }
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('estado', '1-estado', FALSE);
		return $this->db->update($this->_table);
	}
	
}