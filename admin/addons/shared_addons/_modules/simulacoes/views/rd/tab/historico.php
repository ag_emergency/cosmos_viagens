<div class="form_inputs" id="user-details-tab">
    <fieldset id="filters">
        <ul>
            <li class="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class="even">
                        <th scope="col">Montante</th>
                        <th scope="col">Prazo</th>
                        <th scope="col">TAN</th>
                        <th scope="col">TAEG</th>
                        <th scope="col">Prestação</th>
                        <th scope="col">Plano</th>
                        <th scope="col">Data</th>
                    </tr>
                    
                    <?php foreach($lista as $chave => $valor){?>
                        <tr <?= $valor->last==1 ? 'class="dark"' : ''?>>
                            <td scope="col"><?=number_format($valor->montante, 2, ',', '.')?>€</td>
                            <td scope="col"><?=$valor->prazo?> meses</td>
                            <td scope="col"><?=number_format($valor->tan, 3, ',', '.')?>%</td>
                            <td scope="col"><?=number_format($valor->taeg, 2, ',', '.')?>%</td>
                            <td scope="col"><?=number_format($valor->prestacao, 2, ',', '.')?>€</td>
                            <td scope="col"><?=$item->plano=='P'?'PPT':'PPV'?></td>
                            <td scope="col"><?=date('d-m-Y H:i',DateTime::createFromFormat('Y-m-d H:i:s', date($valor->dt))->getTimestamp())?></td>
                        </tr>
                    <?php }?>
                </table>
            </li>
        </ul>
    </fieldset>
</div>