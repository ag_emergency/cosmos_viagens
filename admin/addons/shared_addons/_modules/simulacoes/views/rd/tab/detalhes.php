<div class="form_inputs" id="user-details-tab">
    <fieldset id="filters">
        <ul>
            <li class="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Telefone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Data de registo</th>
                    </tr>
                    <tr>
                        <td><?=$item->nome?></td>
                        <td><?=$item->telefone?></td>
                        <td><a href="mailto:<?=$item->email?>"><?=$item->email?></a></td>
                        <td><?=date('d-m-Y H:i',DateTime::createFromFormat('Y-m-d H:i:s', date($item->dt))->getTimestamp())?></td>
                    </tr>
                </table>
            </li>
        </ul>
        
        <ul>
            <li class="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th scope="col">Produto</th>
                        <th scope="col">Plano</th>
                        <th scope="col">Montante</th>
                        <th scope="col">Prazo</th>
                        <th scope="col">TAN</th>
                        <th scope="col">TAEG</th>
                        <th scope="col">MTIC</th>
                    </tr>
                    <tr>
                        <td><?=$products[$item->produto]?></td>
                        <td><?=$item->plano=='P'?'PPT':'PPV'?></td>
                        <td><?=number_format($item->montante, 0, ',', '.')?>€</td>
                        <td><?=$item->prazo?> meses</td>
                        <td><?=number_format($item->tan, 3, ',', '.')?>%</td>
                        <td><?=number_format($item->taeg, 1, ',', '.')?>%</td>
                        <td><?=number_format($item->mtic, 2, ',', '.')?>€</td>
                    </tr>
                </table>
            </li>
        </ul>
        
        <ul>
            <li class="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th scope="col">Montante<br />a creditar</th>
                        <th scope="col">Montante<br />total</th>
                        <th scope="col">Prestação</th>
                        <th scope="col"><?=$item->plano=='P'?'PPF':'PPV'?></th>
                        <th scope="col"><?=$item->plano=='P'?'PPF':'PPV'?> Mês</th>
                        <th scope="col">Portes</th>
                        <th scope="col">IS</th>
                        <th scope="col">Garantia</th>
                        <th scope="col">Juros Mora</th>
                    </tr>
                    <tr>
                        <td><?=number_format($item->tot_creditar, 2, ',', '.')?>€</td>
                        <td><?=number_format($item->total, 2, ',', '.')?>€</td>
                        <td><?=number_format($item->prestacao, 2, ',', '.')?>€</td>
                        <td><?=$item->plano=='P'?number_format($item->ppf, 2, ',', '.'):number_format($item->ppv, 2, ',', '.')?>€</td>
                        <td><?=$item->plano=='P'?number_format($item->ppf_mes, 2, ',', '.'):number_format($item->ppv_mes, 2, ',', '.')?>€</td>
                        <td><?=number_format($item->portes, 2, ',', '.')?>€</td>
                        <td><?=number_format($item->is, 2, ',', '.')?>€</td>
                        <td><?=$item->garantia?></td>
                        <td><?=number_format($item->juro_mora, 3, ',', '.')?>€</td>
                    </tr>
                </table>
            </li>
        </ul>
    </fieldset>
</div>

<?php echo form_open_multipart() ?>

<div class="form_inputs">

    <ul>
        <?php
            $i = 0;
            foreach($module_fields as $f){
                echo rdform_create($f,$item,$i);
                if(!$f['_hidden']) $i++;
            }
        ?>
    </ul>

</div>

<div class="buttons">
    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
</div>
<?php echo form_close() ?>