<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Montantes_m extends RD_Model {
	
	protected 	$modulename = 'montantes', // nome do modulo
				$tablename = '_montantes'; // nome da tabela
	
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'produto' 
								,'where'=>'ruhf'
								,'type'=>'number'
								,'label'=>'Produto'
								,'rules'=>'required|integer'));
								
		$this->add_field(array(	'field'=>'plano',
								'where'=>'ruhf',
								'type'=>'select',
								'label'=>'Plano',
								'default'=>'V',
								'values'=>array('V'=>'Plano Proteção Vida',
												'P'=>'Plano Proteção Total')));
								
		$this->add_field(array(	'field'=>'prazo',
								'where'=>'ruhf',
								'type'=>'number',
								'label'=>'Prazo',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'montante',
								'where'=>'ruhf',
								'type'=>'number',
								'label'=>'Montante',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'tan',
								'where'=>'ru',
								'type'=>'number',
								'label'=>'TAN',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'taeg',
								'where'=>'ru',
								'type'=>'number',
								'label'=>'TAEG',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'prestacao',
								'where'=>'ru',
								'type'=>'number',
								'label'=>'Prestação',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'portes',
								'where'=>'u',
								'type'=>'number',
								'label'=>'Portes',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'mtic',
								'where'=>'ru',
								'type'=>'number',
								'label'=>'MTIC',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'is',
								'where'=>'u',
								'type'=>'number',
								'label'=>'IS',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'tot_creditar',
								'where'=>'u',
								'type'=>'number',
								'label'=>'Montante a Creditar',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'total',
								'where'=>'u',
								'type'=>'number',
								'label'=>'Montante Total',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppv',
								'where'=>'u',
								'type'=>'number',
								'label'=>'PPV',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppv_mes',
								'where'=>'u',
								'type'=>'number',
								'label'=>'PPV Mês',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppf',
								'where'=>'u',
								'type'=>'number',
								'label'=>'PPV',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'ppf_mes',
								'where'=>'u',
								'type'=>'number',
								'label'=>'PPF Mês',
								'rules'=>'required|numeric'));
								
		$this->add_field(array(	'field'=>'garantia',
								'where'=>'u',
								'type'=>'select',
								'label'=>'Garantia',
								'values'=>array('Não aplicável'=>'Não aplicável',
												'Livrança'=>'Livrança')));
								
		$this->add_field(array(	'field'=>'juro_mora',
								'where'=>'u',
								'type'=>'number',
								'label'=>'Juro Mora',
								'rules'=>'required|numeric'));
				
		
		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		
	}
	
}