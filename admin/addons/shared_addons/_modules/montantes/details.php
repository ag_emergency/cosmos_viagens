<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Montantes extends Module {
	
	public  $version = '2.0.1',
			$tablename = '_montantes', // nome da tabela
			$modulename = 'montantes', // nome do modulo
			$sectionname = 'Montantes'; // nome do elemento no menu
	
	public function info(){
		return array(
			'name' => array(
				'pt' => $this->sectionname
			),
			'description' => array(
				'pt' => 'Gestão dos valores do simulador' // descricao auxiliar do modulo
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => 'simulador', // nome do elemento "pai" no menu, traduzir no ficheiro "admin_lang.php"
			
			// adicionar tabs 
			/*'sections' => array(
				'items' => array(
					'name' 	=> $this->sectionname,
					'uri' 	=> 'admin/'.$this->modulename,
					'shortcuts' => array(
						'create' => array(
							'name' 	=> 'cp:actions_add',
							'uri' 	=> 'admin/'.$this->modulename.'/create',
							'class' => 'add'
							)
						)
					)
				)*/
		);
	}
	
	public function install(){
		// instalar tabela do modulo
		// http://ellislab.com/codeigniter/user-guide/database/
		$this->dbforge->drop_table($this->tablename);
		$this->db->delete('settings', array('module' => $this->modulename));
	 
		$table_fields = array(
			'id' => array(  'type' => 'INT',
							'constraint' => '11',
							'auto_increment' => TRUE ),
							
			'produto' => array(	'type' => 'TINYINT',
								'constraint' => '1',
								'default' => '1' ),
							
			'plano' => array(	'type' => 'VARCHAR',
								'constraint' => '5',
								'default' => 'V' ),
							
			'prazo' => array(	'type' => 'TINYINT',
								'constraint' => '2' ),
							
			'montante' => array('type' => 'INT',
								'constraint' => '11' ),
							
			'tan' => array(	'type' => 'DECIMAL',
							'constraint' => array(10,3) ),
							
			'taeg' => array('type' => 'DECIMAL',
							'constraint' => array(10,1) ),
							
			'prestacao' => array(	'type' => 'DECIMAL',
									'constraint' => array(10,2) ),
							
			'portes' => array(	'type' => 'DECIMAL',
								'constraint' => array(10,2),
								'default' => '1.56' ),
							
			'mtic' => array(	'type' => 'DECIMAL',
								'constraint' => array(10,2) ),
							
			'is' => array(	'type' => 'DECIMAL',
							'constraint' => array(10,2) ),
							
			'tot_creditar' => array('type' => 'DECIMAL',
									'constraint' => array(10,2) ),
							
			'total' => array(	'type' => 'DECIMAL',
								'constraint' => array(10,2) ),
							
			'ppv' => array(	'type' => 'DECIMAL',
							'constraint' => array(10,2) ),
							
			'ppv_mes' => array(	'type' => 'DECIMAL',
								'constraint' => array(10,2) ),
							
			'ppf' => array(	'type' => 'DECIMAL',
							'constraint' => array(10,2) ),
							
			'ppf_mes' => array(	'type' => 'DECIMAL',
								'constraint' => array(10,2) ),
							
			'garantia' => array('type' => 'VARCHAR',
								'constraint' => '20',
								'default' => 'Não aplicável' ),
							
			'juro_mora' => array(	'type' => 'DECIMAL',
									'constraint' => array(10,3) ),
		);
		
		// variaveis globais para o modulo
		/*$sample_setting = array(
			'slug' => 'sample_setting',
			'title' => 'Sample Setting',
			'description' => 'A Yes or No option for the Sample module',
			'`default`' => '1',
			'`value`' => '1',
			'type' => 'select',
			'`options`' => '1=Yes|0=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'sample'
		);*/
	 
		$this->dbforge->add_field($table_fields);
		$this->dbforge->add_key('id', TRUE);
	 	
		// Let's try running our DB Forge Table and inserting some settings
		//if ( ! $this->dbforge->create_table($this->tablename) OR ! $this->db->insert('settings', $table_setting) )
		if ( ! $this->dbforge->create_table($this->tablename) )
		{
			return FALSE;
		}
		
		// No upload path for our module? If we can't make it then fail
		if ( ! is_dir($this->upload_path.'sample') AND ! @mkdir($this->upload_path.'sample',0777,TRUE))
		{
			return FALSE;
		}
		
		// We made it!
		return TRUE;
	}
	
	public function uninstall(){
		$this->dbforge->drop_table($this->tablename);
		
		//$this->db->delete('settings', array('module' => 'sample'));
		
		// Put a check in to see if something failed, otherwise it worked
		return TRUE;
	}
	
	public function upgrade($old_version){
		// Your Upgrade Logic
		return TRUE;
	}
	 
	public function help(){
		// Return a string containing help info
		return '<h4>Ajuda</h4>Este modulo faz a gestão dos valores do simulador.'; // descricao auxiliar para o utilizador
		
		// You could include a file and return it here.
		return $this->load->view('help', NULL, TRUE); // loads modules/sample/views/help.php
	}
}
?>