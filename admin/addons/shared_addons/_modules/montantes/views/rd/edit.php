<section class="title">
	<h4>Editar <?=$single?></h4>
</section>

<section class="item">
<div class="content">

<fieldset id="filters">
    <ul>
        <li class="">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th scope="col">Montante</th>
                    <th scope="col">Prazo</th>
                    <th scope="col">Plano</th>
                    <th scope="col">Produto</th>
                </tr>
                <tr>
                    <td><?=number_format($item->montante, 0, ',', '.')?> €</td>
                    <td><?=$item->prazo?> meses</td>
                    <td><?= ($item->plano=='P') ? 'Plano Proteção Total' : 'Plano Proteção Vida'?></td>
                    <td><?=$item->produto?></td>
                </tr>
            </table>
        </li>
    </ul>
</fieldset>

<?php echo form_open_multipart() ?>

<div class="form_inputs">

	<ul>
    	<?php
			$i = 0;
			foreach($module_fields as $f){
				echo rdform_create($f,$item,$i);
				if(!$f['_hidden']) $i++;
			}
		?>
	</ul>

</div>

<div class="buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
</div>
<?php echo form_close() ?>

</div>
</section>