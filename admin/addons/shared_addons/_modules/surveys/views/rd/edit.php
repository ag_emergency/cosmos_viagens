<section class="title">
	<h4>Editar <?=$single?></h4>
</section>

<section class="item">
    <div class="content">
    
    <?php echo form_open_multipart() ?>
    
        <div class="form_inputs">
            <ol class="survey">
                <?php foreach($questions as $k => $v){ ?>
                    <li>
                        <div class="input">
                            <label class="label"><?=$v['text']?></label>
                            <a href="" class="edit"></a>
                            <input type="text" class="hidden" value="<?=$v['text']?>" data-id="<?=$k?>" data-type="<?=$v['type']?>"/>
                            <a href="" class="save btn blue hidden">Guardar</a>
                        </div>
                        <?php foreach($questions[$k]['answers'] as $k2 => $v2){ ?>
                            <div class="input">
                                <p class="label"><?=$v2['text']?></p>
                                <a href="" class="edit"></a>
                                <input type="text" class="hidden" value="<?=$v2['text']?>" data-id="<?=$k2?>" data-type="<?=$v2['type']?>"/>
                                <a href="" class="save btn blue hidden">Guardar</a>
                            </div>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ol>
        </div>
	
    <?php echo form_close() ?>
    </div>
</section>
<script>$(document).ready(function(){surveysEdit();});</script>