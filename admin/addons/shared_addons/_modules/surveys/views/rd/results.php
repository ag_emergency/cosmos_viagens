<section class="title">
	<h4>Resultados</h4>
</section>

<section class="item">
    <div class="content">
        <div class="form_inputs">
            <ul>
            	<?php foreach($questions as $k => $v){ ?>
                	<li>
                    	<label><?=$v['text'].' ('.$v['tot'].')'?></label>
                        <div class="input">
                        	<?php foreach($questions[$k]['answers'] as $k2 => $v2){ ?>
                            	<?=$v2['text'].' - '.$v2['perc'].'%('.$v2['tot'].')<br />'?>
							<?php } ?>
                        </div>
                    </li>
                <?php } ?>
        	</ul>
        </div>
    </div>
</section>