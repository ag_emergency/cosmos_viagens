<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	
	public 	$table_surveys = '_surveys', // nome da tabela
			$table_questions = '_surveys_questions',
			$table_answers = '_surveys_answers',
			$table_results = '_surveys_results',
			$modulename = 'surveys', // nome do modulo
			$plural = 'Encuestas', // item plural
			$single = 'Encuesta'; // item singular
	
	public function __construct()
	{
		
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);
		
		// Load all required classes
		$this->load->helper('debug');
	}
	
	public function create()
	{
		if(isset($_POST['name']))
		{
			/* INSERT SURVEY */
			$temp = str_replace(' ', '', $_POST['name']);
			if(strlen($temp)>0){
				$data = array(	'name' => $_POST['name'],
								'active' => $_POST['active'] );
				$this->db->insert($this->table_surveys, $data); 
				$SURVEY_ID = $this->db->insert_id();
				
				$ORD_QUESTION = 1;
				foreach($_POST as $chave => $valor)
				{
					// LIST QUESTIONS
					if(preg_match('/^pregunta_/', $chave) > 0 && $valor !='escriba la pregunta')
					{
						$temp = str_replace(' ', '', $valor);
						if(strlen($temp)>0)
						{
							$has_answers = false;
							$question_id_in_html = substr($chave, 9);
							
							/* CHECK IF QUESTION HAS ANSWERS */
							foreach($_POST as $k => $v)
							{
								if(preg_match('/^respuesta_'.$question_id_in_html.'_/', $k) > 0)
								{	
									$temp = str_replace(' ', '', $v);
									if(strlen($temp)>0)
									{
										$has_answers = true;
										break;
									}
								}
							}
							
							/* INSERT QUESTION */
							if($has_answers){
								$data = array(	'survey_id' => $SURVEY_ID,
												'text' => $valor,
												'ord' => $ORD_QUESTION,
												'type' => $_POST['preguntatipo_'.$question_id_in_html] );
								$this->db->insert($this->table_questions, $data); 
								$QUESTION_ID = $this->db->insert_id();
								
								++$ORD_QUESTION;
								
								
								$ORD_ANSWER = 1;
								/* INSERT ANSWERS */
								foreach($_POST as $k => $v)
								{
									if(preg_match('/^respuesta_'.$question_id_in_html.'_/', $k) > 0)
									{	
										$temp = str_replace(' ', '', $v);
										if(strlen($temp)>0)
										{
											$data = array(	'question_id' => $QUESTION_ID,
															'text' => NULL,
															'obs' => 0 );
											if($v=='si_no'){
												for($i=1;$i<=2;++$i){
													$data['ord'] = $ORD_ANSWER;
													$data['text'] = $i==1 ? 'Sí' : 'No' ;
													$this->db->insert($this->table_answers, $data);
													
													++$ORD_ANSWER;
												}
											}elseif($v=='1_5'){
												for($i=1;$i<=5;++$i){
													$data['ord'] = $ORD_ANSWER;
													if($i==1) $data['text'] = 'Muy Bueno';
													if($i==2) $data['text'] = 'Bueno';
													if($i==3) $data['text'] = 'Regular';
													if($i==4) $data['text'] = 'Malo';
													if($i==5) $data['text'] = 'Muy Malo';
													$this->db->insert($this->table_answers, $data);
													
													++$ORD_ANSWER;
												}
											}else{
												/* CHECK IF ANSWER IS TEXTAREA */
												$data['ord'] = $ORD_ANSWER;
												$data['text'] = $v;
												
												$tmp = str_replace('respuesta_', 'respuestatipo_', $k);
												if(isset($_POST[$tmp]) && $_POST[$tmp]=='textarea') $data['obs'] = 1;
												
												$this->db->insert($this->table_answers, $data);
												++$ORD_ANSWER;
											}
										}
									}
								
								/* /INSERT ANSWERS */}
							/* /INSERT QUESTION */}
						}
					/* /LIST QUESTIONS */}
					
				}
				
				$message = 'Entrada inserida com sucesso.';
				$status = 'success';
				$this->session->set_flashdata($status, $message);
				if(empty($this->_returnUrl)){
					if(isset($this->returnID)){
						return $this->db->insert_id();
					}else{
						redirect('admin/'.$this->module_name);
					}
				}else{
					redirect('admin/'.$this->_returnUrl);
				}
			/* /INSERT SURVEY */}
			
		}
		
		parent::create();
	}
	
	public function edit($id)
	{
		if(isset($_POST['id']) && isset($_POST['text']) && isset($_POST['type'])){
			$res = 0;
			$res = $this->db->query('UPDATE default__surveys_'.$_POST['type'].' SET text = "'.$_POST['text'].'" WHERE id = '.$_POST['id']);
			die($res);
		}
		
		$questions = $this->db->query('SELECT * FROM default_'.$this->table_questions.' WHERE survey_id = '.$id.' ORDER BY ord ASC');
		$questions = $questions->result_array();
		
		
		foreach($questions as $k => $v){
			$this->data->questions[$v['id']]['text'] = $v['text'];
			$this->data->questions[$v['id']]['type'] = 'questions';
			
			$answers = $this->db->query('SELECT * FROM default_'.$this->table_answers.' WHERE question_id = '.$v['id'].' AND obs = 0 ORDER BY ord ASC');
			$answers = $answers->result_array();
			foreach($answers as $k2 => $v2){
				$this->data->questions[$v['id']]['answers'][$v2['id']]['text'] = $v2['text'];
				$this->data->questions[$v['id']]['answers'][$v2['id']]['type'] = 'answers';
			}
		}
		
		
		$this->template	->add_action('back', $this->single)
						->build('rd/edit', $this->data);
	}
	
	public function results($id)
	{
		
		$questions = $this->db->query('SELECT * FROM default_'.$this->table_questions.' WHERE survey_id = '.$id.' ORDER BY ord ASC');
		$questions = $questions->result_array();
		$n=1;
		
		foreach($questions as $k => $v){
			$this->data->questions[$v['id']]['text'] = $n.'. '.$v['text'];
			
			$result = $this->db->query('SELECT COUNT(R.question_id) AS tot FROM default_'.$this->table_results.' R LEFT JOIN default__surveys_answers A ON A.id = R.answer_id WHERE R.survey_id = '.$id.' AND R.question_id = '.$v['id'].' AND A.obs = 0 ');
			$result = $result->result_array();
			$this->data->questions[$v['id']]['tot'] = $result[0]['tot'];
			
			$answers = $this->db->query('SELECT * FROM default_'.$this->table_answers.' WHERE question_id = '.$v['id'].' AND obs = 0 ORDER BY ord ASC');
			$answers = $answers->result_array();
			foreach($answers as $k2 => $v2){
				$this->data->questions[$v['id']]['answers'][$v2['id']]['text'] = $v2['text'];
				
				$result = $this->db->query('SELECT COUNT(answer_id) AS tot FROM default_'.$this->table_results.' WHERE survey_id = '.$id.' AND question_id = '.$v['id'].' AND answer_id = '.$v2['id']);
				$result = $result->result_array();
				
				$this->data->questions[$v['id']]['answers'][$v2['id']]['tot'] = $result[0]['tot'];
				$this->data->questions[$v['id']]['answers'][$v2['id']]['perc'] = floor(($this->data->questions[$v['id']]['answers'][$v2['id']]['tot']/$this->data->questions[$v['id']]['tot'])*100);
			}
			++$n;
		}
		
		$this->template	->add_action('back', $this->single)
						->build('rd/results', $this->data);
	}
	
}