<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Surveys extends Module {
	
	public  $version = '2.0.0',
			$table_surveys = '_surveys', // nome da tabela
			$table_questions = '_surveys_questions',
			$table_answers = '_surveys_answers',
			$table_results = '_surveys_results',
			$modulename = 'surveys', // nome do modulo
			$sectionname = 'Encuestas'; // nome do elemento no menu
	
	public function info(){
		return array(
			'name' => array(
				'pt' => $this->sectionname,
				'es' => $this->sectionname
			),
			'description' => array(
				'pt' => 'Gestão da página de Red de Veterinarios', // descricao auxiliar do modulo
				'es' => 'Gestión Página de Encuestas.'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => 'modulos', // nome do elemento "pai" no menu, traduzir no ficheiro "admin_lang.php"
			
		);
	}
	
	public function install(){
		// instalar tabela do modulo
		// http://ellislab.com/codeigniter/user-guide/database/
		
		// SURVEYS
		$this->dbforge->drop_table($this->table_surveys);
		$table_fields = array(
			'id' => array(  'type' => 'INT',
							'constraint' => '11', 
                            'unsigned' => TRUE,
							'auto_increment' => TRUE ),
							
			'name' => array('type' => 'VARCHAR',
							'constraint' => '255' ),

			'active' => array(  'type' => 'TINYINT',
								'constraint' => '1',
								'default'=>'0' ),
							
			'creation_date' => array( 'type' => 'TIMESTAMP' )
		);
		$this->dbforge->add_field($table_fields);
		$this->dbforge->add_key('id', TRUE);
		if ( ! $this->dbforge->create_table($this->table_surveys) ) return FALSE; 
	 
	 	// QUESTIONS
		$this->dbforge->drop_table($this->table_questions);
		$table_fields = array(
			'id' => array(  'type' => 'INT',
							'constraint' => '11', 
                            'unsigned' => TRUE,
							'auto_increment' => TRUE ),
							
			'survey_id' => array(  	'type' => 'INT',
									'constraint' => '11', 
                                    'unsigned' => TRUE),
							
			'text' => array('type' => 'VARCHAR',
							'constraint' => '255' ),
							
			'ord' => array(	'type' => 'INT',
							'constraint' => '11',
							'default'=>'10' ),
							
			/*'type' => array('type' => 'ENUM',
							'constraint' => '"radio","check"',
							'null' => TRUE ),*/

		);
		$this->dbforge->add_field($table_fields);
		$this->dbforge->add_field('type ENUM("radio","check") DEFAULT NULL');
		$this->dbforge->add_key('id', TRUE);
		if ( ! $this->dbforge->create_table($this->table_questions) ) return FALSE; 
	 
	 	// ANSWERS
		$this->dbforge->drop_table($this->table_answers);
		$table_fields = array(
			'id' => array(  'type' => 'INT',
							'constraint' => '11', 
                            'unsigned' => TRUE,
							'auto_increment' => TRUE ),
							
			'question_id' => array(	'type' => 'INT',
									'constraint' => '11', 
                                    'unsigned' => TRUE),
							
			'text' => array('type' => 'VARCHAR',
							'constraint' => '255' ),
							
			'ord' => array(	'type' => 'INT',
							'constraint' => '11',
							'default'=>'10' ),
							
			'obs' => array(	'type' => 'TINYINT',
							'constraint' => '1',
							'default'=>'0' ),
							
			'obs_value' => array(	'type' => 'TEXT',
									'null'=>true ),

		);
		$this->dbforge->add_field($table_fields);
		$this->dbforge->add_key('id', TRUE);
		if ( ! $this->dbforge->create_table($this->table_answers) ) return FALSE; 
	 
	 	// RESULTS
		$this->dbforge->drop_table($this->table_results);
		$table_fields = array(
			'id' => array(  'type' => 'INT',
							'constraint' => '11', 
                            'unsigned' => TRUE,
							'auto_increment' => TRUE ),
							
			'user_id' => array( 'type' => 'INT',
								'constraint' => '11', 
                                'unsigned' => TRUE, 
                                'null' => TRUE),
							
			'survey_id' => array(  	'type' => 'INT',
									'constraint' => '11', 
                                    'unsigned' => TRUE),
							
			'question_id' => array(	'type' => 'INT',
									'constraint' => '11', 
                                    'unsigned' => TRUE),
							
			'answer_id' => array(	'type' => 'INT',
									'constraint' => '11', 
                                    'unsigned' => TRUE),
							
			'email' => array(	'type' => 'VARCHAR',
								'constraint' => '255', 
                                'null' => TRUE ),
							
			'creation_date' => array( 'type' => 'TIMESTAMP' )

		);
		$this->dbforge->add_field($table_fields);
		$this->dbforge->add_key('id', TRUE);
		if ( ! $this->dbforge->create_table($this->table_results) ) return FALSE; 
		
		
		// We made it!
		return TRUE;
	}
	
	public function uninstall(){ 
	
		$this->dbforge->drop_table($this->table_surveys);
		$this->dbforge->drop_table($this->table_questions);
		$this->dbforge->drop_table($this->table_answers);
		$this->dbforge->drop_table($this->table_results);
		
		return TRUE; 
	}
	
	public function upgrade($old_version){ return TRUE; }
	 
	public function help(){
		// Return a string containing help info
		return '<h4>Ajuda</h4>Este módulo te permite gestionar las encuestas.'; // descricao auxiliar para o utilizador
		
		// You could include a file and return it here.
		return $this->load->view('help', NULL, TRUE); // loads modules/sample/views/help.php
	}
}
?>