<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Faqs_m extends RD_Model {
	
	protected 	$modulename = 'faqs', // nome do modulo
				$tablename = '_faqs'; // nome da tabela
			
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'titulo', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Título',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'texto', 
								'where'=>'cu',
								'type'=>'htmlAdv',
								'label'=>'Texto',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'ord' 
								,'where'=>'h'
								,'type'=>'text'
								,'label'=>'Ordem'));
							
		
		// adicionar links para accoes
		$this->add_action('Ordem','order',"admin/{$this->_module_name}/action/changeOrderToggle/{id}",array('col'=>'ord','module'=>$this->modulename)); // title, tipo, url, [coluna,modulo]
		
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}");
		
	}
	
	public function get_all($limit=0, $offset=0, $filters=FALSE){
		$this->db->order_by('ord ASC');
		return parent::get_all($limit,$offset,$filters);
  	}
	
	public function changeOrder($id){
		return parent::changeMainOrder($id, 'ord');
	}
	
}