<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Seguros_m extends RD_Model {
	
	protected 	$modulename = 'seguros', // nome do modulo
				$tablename = '_seguros'; // nome da tabela
	
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'titulo1',
								'where'=>'ru',
								'type'=>'text',
								'label'=>'Primeiro título',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'texto1',
								'where'=>'u',
								'type'=>'textarea',
								'label'=>'Primeiro texto',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'link_txt1',
								'where'=>'u',
								'type'=>'text',
								'label'=>'Texto do primeiro link',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'link_url1',
								'where'=>'u',
								'type'=>'text',
								'label'=>'Url do primeiro link',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'titulo2',
								'where'=>'ru',
								'type'=>'text',
								'label'=>'Segundo título',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'texto2',
								'where'=>'u',
								'type'=>'textarea',
								'label'=>'Segundo texto',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'link_txt2',
								'where'=>'u',
								'type'=>'text',
								'label'=>'Texto do segundo link',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'link_url2',
								'where'=>'u',
								'type'=>'text',
								'label'=>'Url do segundo link',
								'rules'=>'required'));
							
		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		
	}
	
}