<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Adswork_m extends RD_Model {
	
	protected 	$modulename = 'adswork', // nome do modulo
				$tablename = '_adswork'; // nome da tabela
			
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada

		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'title', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Título',
								'rules'=>'required'));
								
		$this->add_field(array(	'field'=>'permalink', 
								'where'=>'cuh',
								'type'=>'text'));

		$this->add_field(array(	'field'=>'job', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Cargo',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'section', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Sector',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'location', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Lugar de trabajo',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'hierarchy', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Jerarquía'));

		$this->add_field(array(	'field'=>'vacancies', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Vacantes'));

		$this->add_field(array(	'field'=>'text', 
								'where'=>'cu',
								'type'=>'htmlAdv',
								'label'=>'Texto',
								'rules'=>'',
								'style'=>'width:400px;'));

		$this->add_field(array(	'field'=>'image_url', 
								'where'=>'cu',
								'label'=>'Imagen (120x120)', 
								'path'=>'../uploads/adswork/', 
								'type'=>'image', 
								'url'=>'Download', 
								'allowed_types'=>'jpg|jpeg|gif|png'));

		$this->add_field(array(	'field'=>'url', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Link'));

		$this->add_field(array(	'field'=>'date', 
								'where'=>'rcuf',
								'type'=>'date',
								'label'=>'Fecha de anuncio',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sí',
												'0'=>'No')));
							
		
		// adicionar links para accoes
		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active')); 
		$this->add_action('Orden','order',"admin/{$this->_module_name}/action/changeOrderToggle/{id}",array('col'=>'ord','module'=>$this->modulename));
		//se tem "actions:"Editar é porque está na lang
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Eliminar','link',"admin/{$this->_module_name}/delete/{id}",array("confirm"=>TRUE));

		
	}
	
	public function get_all($limit=0, $offset=0, $filters=FALSE){
		$this->db->order_by('ord ASC');
		return parent::get_all($limit,$offset,$filters);
  	}
	
	public function changeOrder($id){
		return parent::changeMainOrder($id, 'ord');
	}
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}
	
}