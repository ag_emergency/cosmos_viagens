<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Maskotas_m extends RD_Model {
	
	protected 	$modulename = 'maskotas', // nome do modulo
				$tablename = '_maskotas'; // nome da tabela
	
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(	'field'=>'id',
								'where'=>'uhf',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'user_id', 
								'where'=>'r',
								'type'=>'link',
								'label'=>'Usuario'));
								
		$this->add_field(array(	'field'=>'name',
								'where'=>'rf',
								'type'=>'text',
								'label'=>'Nombre Maskota',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'image_url', 
								'where'=>'r',
								'label'=>'Imagen', 
								'path'=>'../uploads/maskotas/', 
								'type'=>'image', 
								'url'=>'Download', 
								'allowed_types'=>'jpg|jpeg|gif|png'));
								
		$this->add_field(array(	'field'=>'adopt', 
								'where'=>'rf',
								'type'=>'text',
								'label'=>'Adopta',
								'type'=>'select',
								'values'=>array('1'=>'Sí',
												'0'=>'No') ));
								
		$this->add_field(array(	'field'=>'cupid', 
								'where'=>'rf',
								'type'=>'text',
								'label'=>'Cupido',
								'type'=>'select',
								'values'=>array('1'=>'Sí',
												'0'=>'No') ));
								
		$this->add_field(array(	'field'=>'permalink', 
								'where'=>'f',
								'type'=>'text'));
								
		$this->add_field(array(	'field'=>'highlight', 
								'where'=>'f',
								'type'=>'select',
								'label'=>'Destacado',
								'values'=>array('1'=>'Sí',
												'0'=>'No') ));
								
								
								
								
		
		
		$this->add_action('Destacado','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'highlight'));
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE));
		
	}
	
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('highlight', '1-highlight', false);
		return $this->db->update($this->_table);
	}
}