<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Branches_m extends RD_Model {
	
	protected 	$modulename = 'branches', // nome do modulo
				$tablename = '_branches'; // nome da tabela
			
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada

		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'type',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Tipo',
								'default'=>'1',
								'values'=>array('1'=>'Sí',
												'0'=>'No')));

		$this->add_field(array(	'field'=>'category', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Categoria'));

		$this->add_field(array(	'field'=>'name', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Nombre',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'address', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Morada'));

		$this->add_field(array(	'field'=>'colony', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Colonia'));

		$this->add_field(array(	'field'=>'cp', 
								'where'=>'cuf',
								'type'=>'text',
								'label'=>'C.P.'));

		$this->add_field(array(	'field'=>'city', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Ciudad'));
								
		$this->add_field(array(	'field'=>'state_id', 
								'where'=>'cu',
								'type'=>'select',
								'label'=>'Estado',
								'rules'=>'required',
                                'values'=>$this->dropdown(    
                                            'id',
                                            'name',
                                            array( 'table'=>'default__config_states',
                                                   'where'=>'active = 1',
                                                   'sort_by'=>'name')) ));

		$this->add_field(array(	'field'=>'phone', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Teléfono'));

		$this->add_field(array(	'field'=>'schedule', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Horario'));

		$this->add_field(array(	'field'=>'email', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Email'));

		$this->add_field(array(	'field'=>'lat', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Latitude'));

		$this->add_field(array(	'field'=>'lng', 
								'where'=>'cu',
								'type'=>'text',
								'label'=>'Longitude'));

		$this->add_field(array(	'field'=>'image_url', 
								'where'=>'cu',
								'label'=>'Imagen (298x138)', 
								'path'=>'../uploads/branches/', 
								'type'=>'image', 
								'url'=>'Download', 
								'allowed_types'=>'jpg|jpeg|gif|png' ));
		
		$this->add_field(array(	'field'=>'active',
								'where'=>'rcuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sí',
												'0'=>'No')));


		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active'));
		
		
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Eliminar','link',"admin/{$this->_module_name}/delete/{id}",array("confirm"=>TRUE));

		
	}
	
	public function get_all($limit=0, $offset=0, $filters=FALSE){
		$this->db->order_by('name ASC');
		return parent::get_all($limit,$offset,$filters);
  	}
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}
	
}