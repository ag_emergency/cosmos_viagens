<div class="form_inputs" id="user-details-tab">
    <fieldset>
        <?php echo form_open_multipart() ?>

        <div class="form_inputs">
            <h2>Pesquisa de imagens via google images</h2>

            <ul style="height:auto">

                <li class="">
                    <label for="f_create_search">Pesquisar Imagens</label>
                    <div class="input"><input type="text" name="search" value="<?=$search?>" id="f_create_search"></div>
                </li>
                <button type="submit" name="btnAction" value="save" class="btn blue">
                    <span>Pesquisar</span>
                </button>
                <br /><br /><br />
                <?if( $type == "garcias.pt"){?>
                    <p>Segue listagem de imagens detactadas pelo google images para a referência em causa. Caso encontre a pretendida, basta seleccionar a imagem e clicar em Gravar</p>
                <?}?>

                <h3>Listagem</h3>

                <?php foreach($images as $key=>$image){?>
                <li class="">
                    <div class="input"><input type="radio" name="image[]" value="<?php echo $image["url"]; ?>" id="ck_<?=$key?>"> <label for="ck_<?=$key?>">Title: <?= $image["title"] ?> ( <?=$image["width"]?>x<?=$image["height"]?>) </label></div>
                    <img src="<?php echo $image["url"]; ?>" height="150px"/>
                </li>
                <?}?>
                <? if(sizeof($images) == 0){?>
                    <p><b>Não foram encontradas imagens para a pesquisa "<?=$query?>"</b></p>
                <?}?>
            </ul>

        </div>

        <div class="buttons">
            <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
        </div>
        <?php echo form_close() ?>
    </fieldset>
</div>