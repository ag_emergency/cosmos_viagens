<div class="form_inputs" id="user-details-tab">
    <fieldset id="address">
        <ul>
            <li class="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>Morada</th>
                        <th>Cod Postal</th>
                        <th>Localidade</th>
                        <th>País</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?if(isset($list)){?>
                        <? foreach($list as $key=>$item){ ?>
                        <tr class="even">
                            <td><?= $item->address ?></td>
                            <td><?= $item->postalcode ?></td>
                            <td><?= $item->local ?></td>
                            <td><?= $countries[$item->country_id] ?></td>
                        </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </li>
        </ul>
    </fieldset>
</div>