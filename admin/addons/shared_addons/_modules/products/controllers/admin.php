<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{

	public 	$modulename = 'products', // nome do modulo
			$plural = 'Produtos', // item plural
			$single = 'Produto'; // item singular

	public function __construct(){

		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);

		$this->data->tabs = array(
			'Editar' => 'edit',
			'Google Image' => 'googleimage'
		);

		// Load all required classes
		$this->load->helper('debug');
	}

	public function create(){

		parent::create();
	}

	public function edit($id)
	{
		$this->session->set_userdata('return_url', 'admin/'.$this->module_name.'/edit/'.$id);
		$this->data->tab = 'edit';
		parent::edit($id);
	}


	public function googleimage($id, $type = "garcias.pt"){


		$module = new $this->{$this->module_name.'_m'}();
		$product = $module->get($id);
		$this->data->item =& $product;
		$this->data->type = $type;
		$this->data->tab = 'googleimage';

		if(!$product){
			redirect('admin/'.$this->module_name);
			exit;
		}

		if(isset($_POST["search"]) && $_POST["search"] != ""){
			$type = "all";
			$this->data->search = $_POST["search"];
		}else{
			$this->data->search = "";
		}

		//if post with seleccion
		if(isset($_POST["image"]) && sizeof($_POST["image"])>0){
			if( sizeof($_POST["image"])!= 1){
				$this->session->set_flashdata( 'error', 'Apenas pode seleccionar 1 imagem.');
			}else{

				$filename = md5($product->reference.date("Y-m-d H:i:s")) . "." . pathinfo($_POST["image"][0], PATHINFO_EXTENSION);
				$res = copy($_POST["image"][0], '../uploads/products/'.$filename);

				if($res){
					////update in bd
					$data = array('image' => $filename);
					$this->db->where('id', $product->id);
					$update = $this->db->update('default__products', $data);

					if($update){
						$this->session->set_flashdata( 'success', 'Imagem associada com sucesso.');
						redirect('admin/'.$this->module_name.'/edit/'.$product->id);
						exit;
					}else{
						unlink('../uploads/products/'.$filename);
					}

				}
				$this->session->set_flashdata( 'error', 'Erro ao copiar a imagem.');
			}

			redirect('admin/'.$this->module_name.'/googleimage/'.$product->id);
			exit;
		}

		$images = array();
		if($product->reference != ""){
			if($type == "garcias.pt"){
				$query = "garcias.pt: ".$product->reference;
				$extraurl = "&as_filetype=png&imgsz=large";
			}else{
				$query = "";
				if(isset($_POST["search"])){
					$query = urlencode($_POST["search"]);
				}
				$extraurl = "&as_filetype=png&imgsz=large";
			}

			$this->data->query = $query;
			$url = 'http://ajax.googleapis.com/ajax/services/search/images?v=1.0&rsz=8&q=';
			//enter your API key here
			$key = 'AIzaSyAdn0M4FoD1ns3bB4m_F19Q6vgNvoat5O0';
			$url .= urlencode($query).$extraurl.'&key='.$key;
			$curl = curl_init();

			curl_setopt($curl, CURLOPT_URL, $url);
			//curl_setopt($curl, CURLOPT_URL, "https://www.google.pt/search?q=garcias.pt:+93417884.png&source=lnms&tbm=isch&sa=X");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$data = curl_exec($curl);
			curl_close($curl);
			//decoding request
			$result = json_decode($data, true);

			//dump($url);
			//echo $url;

			if(isset($result["responseData"]) && isset($result["responseData"]["results"])){
				foreach($result["responseData"]["results"] as $img){
					//if($type == "all" || $img["visibleUrl"] == "www.garcias.pt"){
						$images[] = $img;
					//}
				}
			}
		}
		$this->data->images = $images;

		$this->template
			->add_action('back', $this->single)
			->build('rd/edit', $this->data);
		//dump($result);

		//$images = get_images("cars");
	}


	public function import(){

		$module = new $this->{$this->module_name.'_m'}();

		$products_in_list = array();
		$this->db->select('id, reference');
		$query = $this->db->get('default__products')->result();
		foreach($query as $prod){
			$products_in_list[$prod->reference] = $prod->id;
		}


		try{

			$xml = @simplexml_load_file('http://www.garcias.pt/wsPrimavera/Primavera.asmx/ListaArtigos', null, LIBXML_NOERROR);
			
			if($xml === false){throw new Exception("Error Processing Garcias Webserver Request: ".date("Y-m-d H:i:s") ."\n", 1);}

			$json = (string)$xml[0];
			$products = json_decode($json);

			$brans = array();
			$types = array();

			foreach($products as $product){
				if($product->PVP1 > 0){

					if(!isset($products_in_list[$product->Artigo])){

	    				$brand_id = 0;
	    				if($product->Marca != NULL){
	    					if(!isset($brans[url_title(strtolower($product->Marca))])){
	    						$brand = array(
			    					'title' => $product->Marca,
								'slug' => url_title(strtolower($product->Marca)),
			    					'active' => 1
			    				);
			    				$res = $this->db->insert('default__products_brands', $brand);
			    				$brand_id = $this->db->insert_id();
			    				$brans[url_title(strtolower($product->Marca))] = $brand_id;
	    					}else{
	    						$brand_id = $brans[url_title(strtolower($product->Marca))];
	    					}
		    			}

		    			$type_id = 0;
	    				if($product->Familia != NULL){
	    					if(!isset($types[url_title(strtolower($product->Familia))])){
			    				$type = array(
			    					'title' => $product->Familia,
								'slug' => url_title(strtolower($product->Familia)),
			    					'active' => 1
			    				);
			    				$res = $this->db->insert('default__products_types', $type);
			    				$type_id = $this->db->insert_id();
			    				$types[url_title(strtolower($product->Familia))] = $type_id;
			    			}else{
			    				$type_id = $types[url_title(strtolower($product->Familia))];
			    			}
		    			}

	    				$prod = array(
							'reference' => $product->Artigo,
							'name' => $product->Descricao,
							'brand_id' => $brand_id,
							'type_id' => $type_id,
							'capacity' => $product->Capacidade,
							'image' => null,
							'price' => $product->PVP1,
							'tax' => $product->Taxa,
							'promotion' => 0,
							'stock' => $product->STKActual,
							'unit' => $product->UnidadeBase,
							'created_date' => date("Y-m-d H:i:s")
						);

	    				$res = $this->db->insert('default__products', $prod);
						if($res){
							echo "Prod inserido: " .$product->Artigo;
						}
					}else{
						echo "just update";
					}

	    		}
	    	}
    	}catch(Exception $e){
    		file_put_contents("Logs/xml_error.txt", $e->getMessage(), FILE_APPEND | LOCK_EX);
    	}
		exit;

		//$list = file_get_contents("http://www.garcias.pt/wsPrimavera/Primavera.asmx/ListaArtigos");
		//$list = json_decode($list);
		//foreach($list as $k=>$i){
		//	$i->timestamp = strtotime($i->DataInicio);
		//}

		/*$module = new $this->{$this->module_name.'_m'}();
		$user = $module->get($id);
		$this->data->item =& $user;

		$this->data->tab = 'address';

		$this->db->where('user_id',$user->id);
		$this->db->order_by('id ASC');
		$this->data->list = $this->db->get('default__usersaddress')->result();

		$this->db->where('active','1');
		$countries = $this->db->get('default__countries')->result();
		foreach($countries as $country){
			$this->data->countries[$country->id] = $country->title;
		}

		$this->template
			->add_action('back', $this->single)
			->build('rd/edit', $this->data);*/
	}


	public function importtxt(){
		$list = file_get_contents("http://www.garcias.pt/wsPrimavera/Primavera.asmx/ListaArtigos");
		file_put_contents("teste.txt", $list);
		exit;
	}

}