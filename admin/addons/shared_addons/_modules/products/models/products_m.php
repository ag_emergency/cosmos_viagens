<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Products_m extends RD_Model {

	protected 	$modulename = 'products', // nome do modulo
				$tablename = '_products'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(
				'field'=>'id',
				'where'=>'uh',
				'type'=>'number'));

		$this->add_field(array(
				'field'=>'reference',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Referência',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'name',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Nome',
				'rules'=>'required'));

		$this->add_field(array( 'field'=>'brand_id',
                                'where'=>'cruf',
                                'type'=>'select',
                                'label'=>'Marca',
                                'rules'=>'required',
                                'values'=>$this->dropdown(
                                            'id',
                                            'title',
                                            array(  'table'=>'default__products_brands',
                                                    'where'=>'active = 1',
                                                    'sort_by'=>'title')) ));

		$this->add_field(array( 'field'=>'type_id',
                                'where'=>'cruf',
                                'type'=>'select',
                                'label'=>'Tipo',
                                'rules'=>'required',
                                'values'=>$this->dropdown(
                                            'id',
                                            'title',
                                            array(  'table'=>'default__products_types',
                                                    'where'=>'active = 1',
                                                    'sort_by'=>'title')) ));

		$this->add_field(array( 'field'=>'region_id',
                                'where'=>'cruf',
                                'type'=>'select',
                                'label'=>'Região',
                                'rules'=>'required',
                                'values'=>$this->dropdown(
                                            'id',
                                            'title',
                                            array(  'table'=>'default__products_regions',
                                                    'where'=>'active = 1',
                                                    'sort_by'=>'title')) ));

		$this->add_field(array( 'field'=>'description',
                                'where'=>'cuf',
                                'type'=>'textarea',
                                'label'=>'Descrição',
                                'rules'=>'max_length[500]'));

		$this->add_field(array(
				'field'=>'capacity',
				'where'=>'cuf',
				'type'=>'text',
				'label'=>'Capacidade',
				'rules'=>''));

		$this->add_field(array( 'field'=>'image',
                                'where'=>'rcu',
                                'label'=>'Imagem',
                                'path'=>'../uploads/products/',
                                'type'=>'image',
                                'url'=>'Download',
                                'allowed_types'=>'jpg|jpeg|gif|png'));

		$this->add_field(array(
				'field'=>'price',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Preço Euros (ex: 41.80)',
				'rules'=>'required|numeric'));

		$this->add_field(array(
				'field'=>'tax',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Taxa Iva % (ex: 23)',
				'rules'=>'required|numeric'));

		$this->add_field(array(
				'field'=>'promotion',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Promoção % (ex: 20) - Desconto de 20%',
				'default'=>'0',
				'rules'=>'numeric'));

		$this->add_field(array(
				'field'=>'unit',
				'where'=>'cuf',
				'type'=>'text',
				'label'=>'Unidade (ex: GF)',
				'rules'=>''));

		$this->add_field(array(
				'field'=>'stock',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Stock',
				'rules'=>'numeric'));

		$this->add_field(array( 'field'=>'is_gourmet',
                                'where'=>'cuf',
                                'type'=>'checkbox',
                                'value'=>'1',
                                'label'=>'Gourmet'));

		$this->add_field(array( 'field'=>'is_limited',
                                'where'=>'cuf',
                                'type'=>'checkbox',
                                'value'=>'1',
                                'label'=>'Edição Limitada'));

		$this->add_field(array( 'field'=>'is_prized',
                                'where'=>'cuf',
                                'type'=>'checkbox',
                                'value'=>'1',
                                'label'=>'Premiada'));


		$this->add_field(array( 'field'=>'active',
				'where'=>'rcuf',
				'type'=>'select',
				'label'=>'Estado',
				'default'=>'0',
				'values'=>array('1'=>'Activo', '0'=>'Desactivo')));

		$this->add_field(array(
				'field'=>'created_date',
				'where'=>'cuhf',
				'type'=>'date',
				'default'=>date('Y-m-d H:i:s')));

		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url
		#$this->add_action_common('Exportar','link',"admin/{$this->_module_name}/export/{id}"); // accao, tipo, url

	}

}