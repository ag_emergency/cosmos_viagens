<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */
 
class Activityreport_m extends RD_Model {
	
	protected 	$modulename = 'activityreport', // nome do modulo
				$tablename = '_activityreport'; // nome da tabela
			
	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);
		
		// Load all required classes
		$this->load->helper('debug');
		
		// same old same old... quando tiver tempo faco documentacao mais detalhada

		$this->add_field(array(	'field'=>'id',
								'where'=>'uh',
								'type'=>'number'));
								
		$this->add_field(array(	'field'=>'name', 
								'where'=>'rcuf',
								'type'=>'text',
								'label'=>'Nombre',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'file_url', 
								'where'=>'rcu',
								'label'=>'Expediente', 
								'path'=>'../uploads/activityreport/', 
								'type'=>'file', 
								'url'=>'Download', 
								'allowed_types'=>'zip|pdf|doc|xls|txt',
								'rules'=>''));

		$this->add_field(array(	'field'=>'availability_date', 
								'where'=>'rcuf',
								'type'=>'date',
								'label'=>'Fecha de disponibilidad',
								'rules'=>'required'));

		$this->add_field(array(	'field'=>'active',
								'where'=>'cuf',
								'type'=>'select',
								'label'=>'Activo',
								'default'=>'1',
								'values'=>array('1'=>'Sí',
												'0'=>'No')));
							
		
		// adicionar links para accoes
		$this->add_action('Activo','toggle',"admin/{$this->_module_name}/action/activeToggle/{id}",array('col'=>'active')); // title, tipo, url, coluna
		//se tem "actions:"Editar é porque está na lang
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Eliminar','link',"admin/{$this->_module_name}/delete/{id}",array("confirm"=>TRUE));

	}
	
	public function get_all($limit=0, $offset=0, $filters=FALSE){
		$this->db->order_by('availability_date DESC');
		return parent::get_all($limit,$offset,$filters);
  	}
	
	public function activeToggle($id){
		$this->db->where($this->get_primaryKey(), $id);
		$this->db->set('active', '1-active', false);
		return $this->db->update($this->_table);
	}
	
}