<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Newsletter_m extends RD_Model {

	protected 	$modulename = 'newsletter', // nome do modulo
				$tablename = '_newsletter'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(
				'field'=>'id',
				'where'=>'uh',
				'type'=>'number'));

		$this->add_field(array(
				'field'=>'email',
				'where'=>'cruf',
				'type'=>'email',
				'label'=>'Email',
				'rules'=>'required|valid_email'));

		 $this->add_field(array(
		 		'field'=>'lang_id',
				'where'=>'cruf',
				'type'=>'select',
				'label'=>'Língua',
				'rules'=>'required',
				'default'=>1,
				'values'=>$this->dropdown(
					'id',
					'title',
					array(  'table'=>'default__langs',
						'where'=>'active = 1',
						'sort_by'=>'title')) ));

		$this->add_field(array(
				'field'=>'created_date',
				'where'=>'cruhf',
				'type'=>'date',
				'default'=>date('Y-m-d H:i:s')));

		// adicionar links para accoes
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url
		#$this->add_action_common('Exportar','link',"admin/{$this->_module_name}/export/{id}"); // accao, tipo, url

	}

}