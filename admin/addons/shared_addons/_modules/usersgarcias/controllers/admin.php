<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{

	public 	$modulename = 'usersgarcias', // nome do modulo
			$plural = 'Utilizadores', // item plural
			$single = 'Utilizador'; // item singular

	public function __construct(){

		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);

		$this->data->tabs = array(
			'Editar' => 'edit',
			'Password' => 'password',
			'Gestão de Moradas' => 'address',
			'Favoritos' => 'favorites');

		// Load all required classes
		$this->load->helper('debug');
		//$this->session->unset_userdata('return_url');
	}

	public function create(){

		if(isset($_POST['email'])){
			$_POST['hash'] = md5(date("Y-m-d H:i:s").$_POST["email"]);
		}
		if(isset($_POST['pwd'])){
			$_POST['pwd'] = md5($_POST['pwd']);
		}

		parent::create();
	}

	public function edit($id)
	{
		$this->data->tab = 'edit';
		$this->session->set_userdata('return_url', 'admin/usersgarcias/edit/'.$id);
		parent::edit($id);
	}


	public function password($id)
	{
		$module = new $this->{$this->module_name.'_m'}();
		$user = $module->get($id);
		$this->data->item =& $user;

		$this->data->tab = 'password';

		$config = array(
               array(
                     'field'   => 'pwd',
                     'label'   => 'Password',
                     'rules'   => 'trim|required|min_length[6]|max_length[12]|matches[pwdrep]'
                  ),
               array(
                     'field'   => 'pwdrep',
                     'label'   => 'Confirmação de Password',
                     'rules'   => 'trim|required|min_length[6]|max_length[12]'
                  )
            );

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run()){

			$this->db->where('id', $user->id);
			$user->pwd = md5($this->input->post('pwd'));
			$res = $this->db->update('default__users', $user);
			if($res){
				//success message
				$this->session->set_flashdata( 'success', 'Password alterada com sucesso.');
			}else{
				//error message
				$this->session->set_flashdata( 'error', 'Erro ao alterar a password.');
			}
			redirect('admin/'.$this->module_name.'/password/'.$user->id);
		}

		$this->template
			->add_action('back', $this->single)
			->build('rd/edit', $this->data);
	}


	public function address($id){

		$module = new $this->{$this->module_name.'_m'}();
		$user = $module->get($id);
		$this->data->item =& $user;

		$this->data->tab = 'address';

		$this->db->where('user_id',$user->id);
		$this->db->order_by('id ASC');
		$this->data->list = $this->db->get('default__usersaddress')->result();

		$this->db->where('active','1');
		$countries = $this->db->get('default__countries')->result();
		foreach($countries as $country){
			$this->data->countries[$country->id] = $country->title;
		}

		$this->template
			->add_action('back', $this->single)
			->build('rd/edit', $this->data);
	}

	public function favorites($id){

		$module = new $this->{$this->module_name.'_m'}();
		$user = $module->get($id);
		$this->data->item =& $user;

		$this->data->tab = 'favorites';

		$this->db->select('p.*');
		$this->db->from('default__users_favoriteprod as uf');
		$this->db->join('default__products as p', 'p.id = uf.product_id');
		$this->db->where('uf.user_id',$user->id);
		$this->db->order_by('uf.id ASC');
		$this->data->list = $favorites = $this->db->get()->result();

		$this->template
			->add_action('back', $this->single)
			->build('rd/edit', $this->data);
	}




}