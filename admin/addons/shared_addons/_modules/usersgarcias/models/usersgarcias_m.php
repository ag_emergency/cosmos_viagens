<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * The press module enables users to create news, upload articles and manage their existing news.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Press Module
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Usersgarcias_m extends RD_Model {

	protected 	$modulename = 'usersgarcias', // nome do modulo
				$tablename = '_users'; // nome da tabela

	public function __construct()
	{
		parent::__construct($this->modulename, $this->tablename);

		// Load all required classes
		$this->load->helper('debug');

		// same old same old... quando tiver tempo faco documentacao mais detalhada
		$this->add_field(array(
				'field'=>'id',
				'where'=>'ruh',
				'type'=>'number'));

		$this->add_field(array(
				'field'=>'name',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Nome',
				'rules'=>'required'));

		$this->add_field(array(
				'field'=>'email',
				'where'=>'cruf',
				'type'=>'email',
				'label'=>'Email',
				'rules'=>'required|valid_email'));//|is_unique[default__usersgarcias.email]|

		$this->add_field(array(
				'field'=>'nif',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'NIF',
				'rules'=>'required'));//

		$this->add_field(array(
				'field'=>'phone',
				'where'=>'cruf',
				'type'=>'text',
				'label'=>'Telemóvel',
				'rules'=>''));

		$this->add_field(array(
				'field'=>'date_birth',
				'where'=>'cruf',
				'type'=>'TEXT',
				'label'=>'Data Nascimento',
				'rules'=>''));

		$this->add_field(array(
				'field'=>'auth_email',
				'where'=>'cuf',
				'type'=>'checkbox',
				'value'=>'1',
				'label'=>'Autorização Email'));

		$this->add_field(array(
				'field'=>'auth_sms',
				'where'=>'cuf',
				'type'=>'checkbox',
				'value'=>'1',
				'label'=>'Autorização SMS'));


		$this->add_field(array(
				'field'=>'pwd',
				'where'=>'c',
				'type'=>'password',
				'label'=>'Password',
				'rules'=>'min_length[6]|max_length[12]'));

		$this->add_field(array(
				'field'=>'hash',
				'where'=>'cuh',
				'type'=>'text',
				'label'=>'Hash',
				'rules'=>'require',
				'default'=>""));

		$this->add_field(array(
				'field'=>'created_date',
				'where'=>'cruhf',
				'type'=>'date',
				'default'=>date('Y-m-d H:i:s')));

		// adicionar links para accoes
		$this->add_action_common('Editar','link',"admin/{$this->_module_name}/edit/{id}"); // accao, tipo, url
		$this->add_action_common('Remover','link',"admin/{$this->_module_name}/delete/{id}",array('confirm'=>TRUE)); // accao, tipo, url
		#$this->add_action_common('Exportar','link',"admin/{$this->_module_name}/export/{id}"); // accao, tipo, url

	}

}