<div class="form_inputs" id="user-details-tab">
    <fieldset id="address">
            <h2>Favoritos</h2>
            <p>Esta lista contém todos os produtos que o utilizador adicionou como favorito.</p>

        <ul>
            <li class="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>Imagem</th>
                        <th>Referência</th>
                        <th>Nome</th>
                        <th>Preço Un.</th>
                        <th>Taxa.</th>
                        <th>Promo.</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?if(isset($list)){?>
                        <? foreach($list as $key=>$item){ ?>
                        <tr class="even">
                            <td><img src="<?= base_url() . 'admin/' . MODULE_NAME . '/thumb/?path=../uploads/products/'. $item->image .'&width=60&height=60&crop=1'?>"/></td>
                            <td><?= $item->reference ?></td>
                            <td><?= $item->name ?></td>
                            <td><?= $item->price ?></td>
                            <td><?= $item->tax ?></td>
                            <td><?= $item->promotion ?></td>
                        </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </li>
        </ul>
    </fieldset>
</div>