<div class="form_inputs" id="user-details-tab">
    <fieldset id="edit">
        <?php echo form_open_multipart() ?>

        <div class="form_inputs">

            <ul>
                <?php
                    $i = 0;
                    foreach($module_fields as $f){
                        echo rdform_create($f,$item,$i);
                        if(!$f['_hidden']) $i++;
                    }
                ?>
            </ul>

        </div>

        <div class="buttons">
            <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
        </div>
        <?php echo form_close() ?>
    </fieldset>
</div>