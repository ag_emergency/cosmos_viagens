<div class="form_inputs" id="user-details-tab">
    <fieldset>
        <?php echo form_open_multipart() ?>

        <div class="form_inputs">
            <h2>Alteração de password</h2>
            <p>Depois de inserir a nova password, esta irá substituir a antiga. Não será possível repor a password antiga.</p>

            <ul style="height:auto">
                <li class="">
                    <label for="f_create_pwd">Nova Password <span>*</span></label>
                    <div class="input"><input type="password" name="pwd" value="" id="f_create_pwd"></div>
                </li>
                <li class="">
                    <label for="f_create_pwdrep">Confirmar Password <span>*</span></label>
                    <div class="input"><input type="password" name="pwdrep" value="" id="f_create_pwdrep"></div>
                </li>
            </ul>

        </div>

        <div class="buttons">
            <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
        </div>
        <?php echo form_close() ?>
    </fieldset>
</div>