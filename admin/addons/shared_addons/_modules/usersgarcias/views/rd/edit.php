<section class="title">
	<h4>Editar <?=$single?></h4>
</section>

<section class="item">
<div class="content">

	<?php
		$this->load->view('rd/partials/tab');
		if($tab == 'password'){
			$this->load->view('rd/tab/password');
		}else if($tab == 'address'){
			$this->load->view('rd/tab/moradas');
		}else if($tab == 'favorites'){
			$this->load->view('rd/tab/favorites');
		}else{
			$this->load->view('rd/tab/detalhes');
		}
	?>

</div>
</section>