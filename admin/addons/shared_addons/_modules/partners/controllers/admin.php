<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The Client module enables users to create news, upload articles and manage their existing files.
 *
 * @author 		Actualsales
 * @package 	PyroCMS
 * @subpackage 	Client Module
 * @category 	Modules
 * @license 	Apache License v2.0
 **/
class Admin extends RD_Controller
{
	
	public 	$modulename = 'partners', // nome do modulo
			$plural = 'Aliados', // item plural
			$single = 'Aliado'; // item singular
	
	public function __construct()
	{
		//error_reporting(E_ALL);
		parent::__construct($this->modulename, $this->plural, $this->single);
			
		// Load all required classes
		$this->load->helper('debug');
	}
	
}