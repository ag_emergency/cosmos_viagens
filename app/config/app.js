var appname = 'main';
var dependencies = ['ui.router', 'pascalprecht.translate', 'validators', 'angulartics', 'angulartics.google.analytics', 'ngDialog'];

angular.module(appname, dependencies);
var app = angular.module(appname);