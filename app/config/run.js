var runFn = function ($rootScope, $state, CONSTANTS, preloaderF, $timeout, ngDialog) {

    $rootScope.CONSTANTS = CONSTANTS;

    var parseSection = function (toState) {
        var sectionArray = toState.name.split('.');
        var section = {};
        /*if (!isEmpty(sectionArray[0])) section.main = sectionArray[0];
        if (!isEmpty(sectionArray[1])) section.sub = sectionArray[1];
        if (!isEmpty(sectionArray[2])) section.partial = sectionArray[2];*/
        /*IGNORE ROOT LANG*/
        if (!isEmpty(sectionArray[1])) section.main = sectionArray[1];
        if (!isEmpty(sectionArray[2])) section.sub = sectionArray[2];
        if (!isEmpty(sectionArray[3])) section.partial = sectionArray[3];

        return section;
    }

    // on stateChangeStart
    var stateChangeStart = function (event, toState, toParams, fromState, fromParams) {
        preloaderF.show();
    };

    $rootScope.$on('$stateChangeStart', stateChangeStart);

    // on stateChangeError
    var stateChangeError = function (event, toState, toParams, fromState, fromParams, error) {
        //console.log(error);
        preloaderF.hide();
    };

    $rootScope.$on('$stateChangeError', stateChangeError);

    // on stateChangeSuccess -- ONREADY
    var stateChangeSuccess = function (event, toState, toParams, fromState, fromParams) {
        $rootScope.section = parseSection(toState);
        preloaderF.hide();

        $("header").removeClass('open');
        $.shifter("close");

        /*$.shifter("close");

        //if mobile
        if($rootScope.mobile){
            var $gotop = $('.gotop')
                .bind("click",function(){
                    $('html,body').stop().animate({scrollTop : 0}, 300);
                });
            $(window).scroll(function(){
                if($(window).scrollTop()>($(window).height()/2)){
                    $gotop.stop().slideDown("fast");
                }
                else{
                    $gotop.hide();
                }
            });
        }
        var from = fromState.name.split('.')[0];
        var to = toState.name.split('.')[0];
        if(from != to){
            window.scrollTo(0, 0);
        }else{
            var top = $("section .main").position().top - $("header").height();
            window.scrollTo(0, top);
        }

        $timeout(function(){
            if(window.FB){
                FB.XFBML.parse();
            }
        },500);*/
    };

    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

    $rootScope.mobile = isMobile();
    $rootScope.device_path = $rootScope.mobile?"mob_":"";
    //$rootScope.device_path = "";

    $(window).on("resize",function(){

        $timeout(function(){
            $rootScope.mobile = isMobile();
            $rootScope.device_path = $rootScope.mobile?"mob_":"";
        },500);
    });

    /*FB = null;
    //facebook
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v2.4&appId=936204796450650";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));*/
}

app.run(runFn);
