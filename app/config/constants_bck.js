var constants = {
    'API': 'api',
    'IMG': 'app/public/img',
    'VERSION': '0.0.1',
    'FB': 'http://www.facebook.com/viagenscosmos',
    'TWITTER': 'http://www.twitter.com/viagenscosmos',
    'PINTEREST': 'https://www.linkedin.com/company/cosmos---viagens-e-turismo-sa'
};

app.constant('CONSTANTS', constants);

var translate = {
    'PT': {
        SEO_TITLE:'COSMOS | Viagens personalizadas para si e para a sua empresa',
        SEO_DESCRIPTION:'É uma empresa prestadora de serviços de viagens, especializada no segmento de turismo desportivo de alta competição, viagens de negócios (Corporate Travel Management), viagens de incentivo e viagens de lazer. Saiba mais aqui!',

        COOKIE_TEXT:'Este site utiliza cookies. Ao navegar no site está a consentir a sua utilização.',
        COOKIE_YES_BUTTON:'OK. Não mostrar novamente esta mensagem.',
        COOKIE_POLICY:'Saiba mais... ',
        COOKIE_MODAL_TEXT:'<h1>Cookies</h1>\
            <p>Os cookies são pequenos ficheiros de texto com informação relevante que o seu dispositivo de acesso (computador, telemóvel/smartphone ou tablet) carrega, através do navegador de internet (browser), quando um site é visitado pelo utilizador.\
            <br /><br />\
            A colocação de cookies não só ajudará o site a reconhecer o dispositivo do utilizador na próxima vez que este o visita mas também será imprescindível para o funcionamento do mesmo. Os cookies usados pela Cosmos Viagens, em todos os seus sites, não recolhem informações pessoais que permitam identificar o utilizador. Guardam informações genéricas, designadamente a forma ou local/país de acesso dos utilizadores e o modo como usam os sites, entre outros. Os cookies retêm apenas informação relacionada com as suas preferências e navegabilidade.\
            </p>',

        INFO_MODAL_TEXT:'<h1>Informação ao Consumidor</h1>\
            <p>Nos termos da Lei n.º 144/2015 de 8 de Setembro informamos que o Cliente poderá recorrer às seguintes</p>\
            <p>Entidades de Resolução Alternativa de Litígios de Consumo: Provedor do Cliente das Agências de Viagens e Turismo</p>\
            <p>em <a href="http://www.provedorapavt.com" target="_blank">www.provedorapavt.com</a>, Comissão Arbitral do Turismo de Portugal em <a href="http://www.turismodeportugal.pt" target="_blank">www.turismodeportugal.pt</a> ou a</p>\
            <p>qualquer uma das entidades devidamente indicadas na lista disponibilizada pela Direcção Geral do Consumidor em</p>\
            <p><a href="http://www.consumidor.pt/" target="_blank">http://www.consumidor.pt/</a> cuja consulta desde já aconselhamos.</p>',

        ERROR_PAGE_TITLE:'Erro 404<br />O conteúdo que procura não foi encontrado.',
        ERROR_PAGE_TEXT:'Não se preocupe,<br />o site vai “voltar à pista”.',

        CLICK2CALL:'NÓS LIGAMOS!',

        WHOWEARE:'QUEM SOMOS',
        SPORTS:'DESPORTO',
        CORPORATE:'CORPORATE',
        INCENTIVES:'INCENTIVOS',
        LEISURE: 'LAZER',
        ABOUTUS:'Sobre nós',
        MISSION_VISION:'Missão & Visão',
        CLIENTS_PARTNERS:'Clientes & Parceiros',
        LOCATIONS:'Localização',
        SERVICES: 'Serviços',
        TEAM_SUPPORT: 'Equipa & Apoio',
        TESTIMONIALS: 'Testemunhos',
        PACKAGES: 'Pacotes de Viagem',
        HIGHLIGHTS: 'Destaques',
        CONTACT:'Contacto',
        COOKIES: 'Cookies',
        CUSTOMER_INFORMATION: 'Informação ao Consumidor',

        SCHEDULE: '(Válido para Portugal. Outros contactos, clique <a class="link" href="#/pt/aboutus/locations">aqui</a>.)<br />Dias úteis das 9:30 às 18:30!',
        FOOTER_SUBSCRIBE_PLACEHOLDER: 'Subscreva a Newsletter',
        FOOTER_SUBSCRIBE_SUCCESS_MESSAGE: 'Obrigado por subscrever a nossa newsletter.',
        FOOTER_SUBSCRIBE_ERROR_MESSAGE: 'Email inválido. ',

        CONTACT_HEADER: '<h2>ENTRE EM CONTACTO</h2>\
            <p>Estamos empenhados em dar uma resposta rápida e eficiente aos nossos clientes 24h / 365d!<br />\
            Para entrar em contacto, por favor preencha os espaços abaixo ou ligue-nos.</p>',
        NAME:'Nome',
        PHONE:'Telefone',
        EMAIL:'Email',
        CONTACT_TERMS: 'CONCORDO COM OS TERMOS E CONDIÇÕES.',
        CONTACT_ERROR_REQUIRE_MESSAGE: 'TODOS OS CAMPOS SÃO DE PREENCHIMENTO OBRIGATÓRIO.',
        CONTACT_SUCCESS_MESSAGE: 'O seu pedido foi submetido corretamente.<br />Será contatado brevemente.',
        CONTACT_SUBMIT_BT: 'PEDIDO DE CONTACTO',

        HOME_SLIDER1: 'Viagens personalizadas à sua medida,<br /> para si e para sua empresa',
        HOME_SLIDER1_BT:'Trate connosco',

        HOME_SLIDER2: 'Marque presença nos seus eventos desportivos favoritos.<br /> Viva momentos únicos e experiências inesquecíveis!',
        HOME_SLIDER2_BT: 'Obtenha cotação',

        HOME_SLIDER3: 'Incremente a motivação e performance<br /> das suas equipas!',
        HOME_SLIDER3_BT: 'Soluções de Incentivos',

        HOME_SLIDER4: 'Com as nossas soluções à medida e serviços personalizados,<br /> transforme as suas férias de sonho numa realidade!',
        HOME_SLIDER4_BT: 'Reserve agora',

        HOME_HEADER: '<h2>EMPRESA ESPECIALIZADA EM TRAVEL MANAGEMENT</h2>\
            <h1>CRIAR UMA <b>EXPERIÊNCIA ÚNICA</b> PARA A SUA EQUIPA COMEÇA AQUI</h1>\
            <p>Viagens planeadas por consultores especializados. <br />Atendimento personalizado 24h/365 dias</p>',

        HOME_WE_ARE_LEADERS: '<h1>SOMOS LÍDERES EM SERVIÇO</h1>\
            <p>A estreita relação que criamos com os nossos clientes, a utilização de tecnologia avançada, processos estruturados e recursos especializados, permitem-nos superar expetativas em cada viagem.</p>',

        ABOUTUS_DISCOVER: 'Descubra mais sobre nós',

        ABOUTUS_WEARETOGETHER: 'ESTAMOS <b>JUNTOS</b>',
        ABOUTUS_DESCRIPTION: 'A COSMOS é uma empresa prestadora de serviços de viagens, especializada em viagens de negócios (Corporate Travel Management), turismo desportivo, viagens de incentivo e de lazer.\
            <a href="#" class="bt truncatemobile">read more</a>\
            <div class="hidemobile">\
                <br />\
                Fundada em 1986, a COSMOS ocupa um lugar de destaque na indústria de viagens, tendo estabelecido ao longo dos quase 30 anos de experiência um diferenciado posicionamento de mercado caracterizado por elevados níveis de serviço e proximidade com os clientes que premiaram a COSMOS ao longo destes anos com a sua preferência.\
                <br /><br />\
                Com um volume de negócios consolidado superior a 50 milhões de euros anuais, a COSMOS tem atualmente operação em Portugal (Lisboa, Porto) e Angola (Luanda, Benguela), oferecendo aos seus clientes uma ampla gama de soluções de "value for money", com um elevado nível de personalização e foco nos objetivos específicos de cada situação, 24 horas por dia, 365 dias por ano.\
                <br /><br />\
                A COSMOS é acionista do grupo nacional <a class="text" href="http://www.go4travel.pt/" target="_blank">GO4TRAVEL</a>, um dos 3 primeiros operadores do mercado nacional, com um volume de negócios agregado de 315M €. Esta dimensão confere aos seus associados o acesso a condições muito competitivas, reforçando valor na relação com os seus clientes.\
                <br /><br />\
                A COSMOS integra ainda a prestigiada rede internacional <a class="text" href="http://www.twintravelmanagement.com" target="_blank">TRAVELSAVERS</a>, presente em mais de 30 países, com um volume de faturação superior a 20 Biliões USD, contando com um vasto leque de serviços e soluções tecnológicas para a gestão integrada de viagens de negócio dos seus clientes, a nível global.\
                <br /><br />\
                Conta com mais de 90 profissionais, altamente treinados e motivados, que partilham valores em que a busca incessante da satisfação do cliente é o fator-chave de sucesso e competitividade da empresa, base essencial da sua proposta de valor.\
            </div>',

        ABOUTUS_WETRAVELTOGETHER: 'VIAJAMOS <b>JUNTOS</b>',
        ABOUTUS_MISSION: '<h2 class="txt-center">MISSÃO</h2>\
            <p>Tendo iniciado recentemente uma nova fase no seu percurso, com a ambição e objetivos de crescimento renovadas nas atuais geografias e em novos mercados, a COSMOS pretende continuar a apoiar o processo de internacionalização das empresas na área das viagens e a promover Portugal nos mercados internacionais, elegendo o cliente como filosofia central de toda a atividade.</p>',
        ABOUTUS_VISION: '<h2 class="txt-center">VISÃO</h2>\
            <p>Olhando para o futuro, a COSMOS está focada em construir relações de longo prazo baseadas na excelência profissional, respeito e criação sustentada de valor.</p>',

        ABOUTUS_WEGROWTOGETHER: '<b>CRESCEMOS</b> JUNTOS',
        ABOUTUS_WEGROWTOGETHER_TXT: '<p>O nosso sucesso depende diretamente da relação duradoura e sustentável com os nossos <b>clientes, fornecedores</b> e <b>parceiros</b>.</p>',


        ABOUTUS_LOCATIONS_TITLE: 'LOCALIZAÇÃO',
        ABOUTUS_LOCATIONS_OPORTO: '<b>PORTO</b>\
                <br /><br />\
                Edifício Jornal de Notícias<br />Rua Gonçalo Cristóvão,<br />nº 195, 4º andar, 4049-011 <br />Telefone:+351 22 209 7600 <br />Fax:+351 22 209 7650<br />\
                <a href="https://www.google.pt/maps/place/COSMOS+-+Viagens+E+Turismo,+S.A./@41.1533817,-8.6108337,17z/data=!4m7!1m4!3m3!1s0xd2464fbc665c4f7:0x907f8648e6b6d651!2sRua+de+Gon%C3%A7alo+Crist%C3%B3v%C3%A3o+195,+4000-413+Porto!3b1!3m1!1s0xd2464fbc665c4f7:0x61801cf8aa84c835" target="_blank">Ver no Google Maps</a>\
                <br /><br />\
                Estádio do Dragão<br />Lado Nascente, Porta 15, 4350-145 <br />Telefone:+351 22 557 49 10 <br />Fax:+351 22 502 5984<br />\
                <a href="https://www.google.pt/maps/place/Est%C3%A1dio+do+Drag%C3%A3o/@41.16177,-8.5857797,17z/data=!3m2!4b1!5s0xd246462af0b2f99:0x7f69ad272311f9c!4m2!3m1!1s0xd246502a2a20bf7:0x1ad7c8d540f6cbe8" target="_blank">Ver no Google Maps</a>',
        ABOUTUS_LOCATIONS_LISBON: '<b>LISBOA</b>\
                <br /><br />Rua Abranches Ferrão <br />nº 10, andar 10º F, 1600 – 001  <br />Telefone:+351 21 724 8360  <br />Fax:+351 21 723 7050 <br />\
                <a href="https://www.google.pt/maps/place/R.+Abranches+Ferr%C3%A3o+10,+1600-001+Lisboa/@38.7506044,-9.1736249,17z/data=!3m1!4b1!4m2!3m1!1s0xd193320ee4f0695:0xaee57f9955aa7628" target="_blank">Ver no Google Maps</a>',
        ABOUTUS_LOCATIONS_LUANDA: '<b>LUANDA</b>\
                <br /><br />Condomínio Garça, nº 28 - Belas – Gamek, Luanda, Angola<br />Telefone: +244 935 682 515<br />Telemóveis: +244 949 487 047 e<br /> +244 937 921 221<br />\
                <a href="https://www.google.pt/maps/place/Condom%C3%ADnio+Veredas+das+Flores/@-8.9684728,13.2973745,13z/data=!4m2!3m1!1s0x0:0xf19b620f695c7fc6" target="_blank">Ver no Google Maps</a>\
                <br /><br />\
                Hotel Praia Morena<br />R. José Estevan, 25, Benguela , Angola<br />Telefone:  +244 934 454 009<br />\
                <a href="https://www.google.pt/maps/place/Praia+Morena+Hotel/@-12.581665,13.4092924,13.87z/data=!4m6!1m3!3m2!1s0x1bafd174eee689c3:0xfd26967b5a4f476d!2sPraia+Morena+Hotel!3m1!1s0x1bafd174eee689c3:0xfd26967b5a4f476d?hl=en" target="_blank">Ver no Google Maps</a>',

        SPORTS_INTRO1: '<p>\
                A COSMOS é líder no segmento do Turismo Desportivo.<br />\
                Com 30 anos de experiência neste segmento, organizou inúmeras operações desportivas, nomeadamente Mundiais de Futebol e Competições Europeias, que representam o transporte de cerca de 35.000 passageiros, em mais de 160 aviões fretados.<br />\
                Oferecemos um serviço integrado que inclui o planeamento e execução da logística de transporte, alojamento e hospitalidade para equipas desportivas, clubes, adeptos, patrocinadores e imprensa.<br />\
                A COSMOS orgulha-se de estabelecer estreitas relações com vários clubes nacionais e internacionais, federações e outras entidades do desporto, nomeadamente a MATCH/FIFA e UEFA e de ser a agência oficial da Federação Portuguesa de Futebol.\
            </p>',
        SPORTS_INTRO2:'<p>Tudo o que fazemos visa superar as expectativas dos nossos clientes, enquanto asseguramos igualmente a excelência operacional:</p>\
            <ul>\
                <li>Serviços premium que valorizam a singularidade e segurança;</li>\
                <li>Know-how, experiência e reputação na gestão e logística de eventos desportivos;</li>\
                <li>Planeamento a longo-prazo orientado para os resultados;</li>\
                <li>Alta eficiência e flexibilidade na busca de soluções;</li>\
                <li>Seleção exigente e estreita relação com os fornecedores;</li>\
                <li>Política de otimização de custos;</li>\
                <li>Relatórios periódicos e reuniões de acompanhamento para analisar e garantir a melhor evolução dos resultados.</li>\
            </ul>',


        SPORTS_TEAM_1: 'Gestores exclusivos e dedicados',
        SPORTS_TEAM_2: 'Contacto direto e permanente com clubes, ligas e federações',
        SPORTS_TEAM_3: 'Assistência flexível e personalizada: COSMOS 24H|365D',
        SPORTS_TEAM_4: 'Tecnologia avançada para o melhor apoio técnico',
        SPORTS_TEAM_5: 'Colaboradores multilingues, disponíveis em todos os aeroportos',
        SPORTS_TEAM_6: 'Acompanhamento por assistentes de viagem',

        SPORTS_QUOTE1: 'OS NOSSOS CLIENTES VIVEM <br />EXPERIÊNCIAS EMOCIONAIS <b>ÚNICAS</b>',
        SPORTS_QUOTE2: 'ATINGIMOS <b>OS MELHORES RESULTADOS</b> DEVIDO À NOSSA CONSOLIDADA EXPERIÊNCIA<br /> E À RELAÇÃO SUSTENTÁVEL COM PARCEIROS',

        SPORTS_NEWS_1:'<h2>23 YEARS<br />AS THE OFFICIAL AGENCY OF<br />FEDERAÇÃO PORTUGUESA DE FUTEBOL</h2>',

        SPORTS_CLIENTS_HEADER:'PROMOVEMOS NO MUNDO O TURISMO DESPORTIVO NACIONAL',
        SPORTS_CLIENTS_TXT:'Organize Sports Tours em qualquer ponto do Globo <span class="bullet"></span> Crie os seus próprios torneios <span class="bullet"></span> Prepare-se para uma nova época Desportiva com os nossos Sports Camps',
        SPORTS_CLIENTS_BUTTON:'Visite a COSMOS ACTIVE',

        CORPORATE_INTRO1: '<p>A <b>COSMOS</b>, como líder em corporate travel management, fornece serviços personalizados de viagens para empresas que:</p>\
            <ul>\
                <li>Pretendam otimizar custos de viagens, respeitar orçamentos e alcançar resultados.</li>\
                <li>Exijam o máximo de qualidade e esperam ter um serviço Premium;</li>\
                <li>Pretendam um parceiro fiável e flexível;</li>\
                <li>Valorizam experiência e disponibilidade;</li>\
            </ul>\
            <p>Oferecemos tarifas competitivas devido aos acordos de longo prazo com os principais operadores internacionais. Utilizamos as mais recentes ferramentas tecnológicas na procura dos melhores resultados, comparação de preços e no controlo da qualidade.</p>',
        CORPORATE_INTRO2:'<p>Seremos o seu parceiro na definição e de uma política de viagens eficiente. <br />O nosso modelo de gestão otimiza resultados em qualidade e em poupança. Sugerimos também uma definição conjunta de objetivos anuais, a fim de alcançar uma otimização sustentável e constante dos principais indicadores-chave de desempenho durante o ano.</p>\
            <br />\
            <p>A sua empresa pode contar com:</p>\
            <ul>\
                <li>Rapidez e eficiência;</li>\
                <li>Assistência telefónica 24H;</li>\
                <li>Múltiplas soluções para os seus pedidos;</li>\
                <li>Procura da melhor oferta de mercado;</li>\
                <li>Acompanhamento permanente.</li>\
            </ul>',

        CORPORATE_TEAM_SUPPORT:'<h2>EQUIPA E APOIO</h2>\
            <p>\
                Para alcançar um excelente nível de serviço, a nossa equipa empenha-se em antecipar as necessidades dos clientes.\
                <br />\
                Preocupamo-nos com o detalhe e temos como principal objetivo adequar os nossos serviços aos perfis e preferências dos nossos viajantes.\
                <br /><br />\
                Centralizamos a nossa operação num International Business Travel Center (BTC), o que permite supervisionar e controlar os processos  de consulta, reserva e emissão de documentações com rigor.\
                <br />\
                A sua empresa pode contar com:\
            </p>',
        CORPORATE_TEAM_1:'<b>Key Account Manager</b><br />\
            Monitoriza e identifica os fatores chave do negócio que optimizem a prestação de serviços, com impacto na redução de custos.<br />\
            Apresenta relatórios de gestão e presta apoio na negociação com fornecedores e serviço pós-venda.',
        CORPORATE_TEAM_2:'<b>Consultor de viagem</b><br />\
            Com experiência em atendimento de contas corporate, asseguram que o fluxo diário de pedidos se processa de acordo com os procedimentos acordados entre ambas as partes. Efetuam o controlo de qualidade das reservas.',
        CORPORATE_TEAM_3:'<b>Supervisor operacional</b><br />\
            Coordena a equipa de consultores de viagem, garante o cumprimento dos níveis de serviço acordados e a politica de viagens, bem como o correto funcionamento da prestação de serviços.',
        CORPORATE_TEAM_4:'<b>COSMOS 24H</b><br />\
            Pode contactar-nos em qualquer lugar e em qualquer momento.<br />\
            Uma equipa será atribuída para lhe dar assistência antes, durante e depois de viajar com a COSMOS<br />\
            + 351 910 899 814',

        INCENTIVES_INTRO1: '<p>\
                Damos forma ao mundo que imaginou!<br />\
                Desenvolvemos eventos à medida e soluções que criam momentos únicos.<br />\
                Se quiser embarcar numa viagem que ficará para sempre na sua memória a COSMOS é o parceiro certo para si.\
            </p>',
        INCENTIVES_INTRO2: '<p>\
        Com 30 anos de experiência em viagens corporativas e de incentivo, somos o parceiro de negócio que precisa para:</p>\
            <ul>\
                <li>Gerar o maior retorno para a sua empresa / rentabilização do orçamento de marketing;</li>\
                <li>Aumentar vendas e criar valor para os seus produtos;</li>\
                <li>Remunerar os seus colaboradores com emoções únicas e financeiramente imensuráveis;</li>\
                <li>Gerar experiências cujo valor intangível durará para sempre;</li>\
            </ul>',

        INCENTIVES_PACKAGES_HEADER: '<b>Desenvolver eventos</b> que podem alavancar o seu negócio!',
        INCENTIVES_PACKAGES_INTERNAL: ' <h4>Ações internas</h4>\
            <ul>\
                <li>Fomente a consolidação motivacional e alinhamento das suas equipas;</li>\
                <li>Premeie o desempenho dos seus colaboradores e incremente os seus resultados;</li>\
                <li>Garanta a retenção de colaboradores-chave.</li>\
            </ul>',
        INCENTIVES_PACKAGES_EXTERNAL: '<h4>Ações externas</h4>\
            <ul>\
                <li>Desenvolva o seu negócio / lançamento de novos produtos (B2B ou B2C);</li>\
                <li>Promova a relação com os seus melhores clientes;</li>\
                <li>Convide os seus parceiros estratégicos para a próxima viagem de incentivos;</li>\
            </ul>',

        INCENTIVES_TEAM_SUPPORT:'<h2>EQUIPA & APOIO</h2>\
            <h5>Damos forma ao Mundo que imaginou. Contate-nos!</h5>\
            <p>\
                Criatividade <span class="bullet"></span> Disponibilidade <span class="bullet"></span> Conhecimento <span class="bullet"></span> Experiência <span class="bullet"></span> Originalidade <span class="bullet"></span> Confidencialidade <span class="bullet"></span> Competência <span class="bullet"></span> Dedicação\
            </p>',
        INCENTIVES_HOW_TXT: '<ul>\
                <li>Escutamos os seus objetivos, definimos conceitos únicos e realizamos projetos exclusivos para a sua marca;</li>\
                <li>Idealizamos e produzimos os materiais corporativos físicos e digitais necessários para o evento;</li>\
                <li>Disponibilizamos colaboradores multilingues para assistência local;</li>\
                <li>Estamos disponíveis durante toda a viagem. </li>\
            </ul>',

        LEISURE_INTRO1:'<p>\
                Os nossos 30 anos de experiência permitem-nos garantir o melhor serviço na conceção das suas viagens de férias.\
                <br />\
                Dedicamo-nos a descobrir pequenos segredos, regiões longínquas ou lugares onde a natureza, arte, ciência ou energia guardam intacta a capacidade de nos surpreender.\
                <br />\
                Cada viagem é desenhada à medida por uma equipa altamente especializada e flexível, que garante um acompanhamento personalizado e exclusivo, desde o primeiro momento.\
            </p>',
        LEISURE_INTRO2:'<p>Um plano de viagem à sua medida que pode satisfazer todos os seus sonhos e que corresponderá ao seu orçamento! Sonhe, que nós concretizamos.</p>\
            <ul>\
                <li>Um jato particular para jantar em Paris!</li>\
                <li>Um passeio a bordo de um iate de luxo na Polinésia Francesa?</li>\
                <li>24/7 mordomo de serviço?</li>\
            </ul>\
            <p>\
                Cuidamos de cada detalhe do início ao fim! <br />\
                Aconselhamos com as melhores soluções e oferecemos a excelência no atendimento.\
            </p>',

        LEISURE_TEAM_SUPPORT:'<h2>EQUIPA & APOIO</h2>\
            <h5>Quer a viagem que sonhou ? Connosco poderá obtê-la:</h5>\
            <p>\
                Dedicação <span class="bullet"></span> Profissionalismo <span class="bullet"></span> Honestidade <span class="bullet"></span> Confidencialidade <span class="bullet"></span> Disponibilidade <span class="bullet"></span> Detalhe <span class="bullet"></span> Simpatia <span class="bullet"></span> Confiança\
            </p>',

        LEISURE_QUOTE1:'VAMOS TORNAR OS <em>seus sonhos</em> REALIDADE',
        LEISURE_QUOTE2:'<em>para</em> DOIS',
        LEISURE_QUOTE3:'<em>para</em> uma FAMÍLIA',
        LEISURE_QUOTE4:'<em>para</em> AMIGOS',
        LEISURE_QUOTE5:'<em>para</em> SI',
    },
    'EN': {
        SEO_TITLE:'COSMOS | Customized traveling experiences for you and your company',
        SEO_DESCRIPTION:'A travel services company, specialist in sports tourism segment of high competition, business travel (Corporate Travel Management), corporate incentives and leisure. Click here to know more.',

        COOKIE_TEXT:'This website uses cookies. By using the website you consent to their use.',
        COOKIE_YES_BUTTON:'OK. Don’t show this message again.',
        COOKIE_POLICY:'Know more...',
        COOKIE_MODAL_TEXT:'<h1>Cookies</h1>\
            <p>Cookies are small text files with essential information that your access device (computer, mobile phone / smartphone or tablet) carries through the browser, when a site is visited by any user.\
            <br /><br />\
            The placement of cookies will not only help the site to recognize the user\'s device in the next time that this visit takes place, but will also be very important to its functioning. The cookies used by Cosmos in all their sites do not collect personal information to identify the user. General information is saved, including the way or location / country of access by the users and how they use the sites, among others. Cookies retain only information related to navigability and about your preferences.\
            </p>',

        INFO_MODAL_TEXT:'<h1>Customer Information</h1>\
            <p>According to the law nº 144/2015 of 8 September we inform that the Customer may use the following</p>\
            <p>Alternative Resolution Entities Consumer disputes: “Provedor do Cliente” of Travel and Tourism Agencies</p>\
            <p>in <a href="http://www.provedorapavt.com" target="_blank">www.provedorapavt.com</a>, “Comissão Arbitral do Turismo de Portugal” in <a href="http://www.turismodeportugal.pt" target="_blank">www.turismodeportugal.pt</a> or</p>\
            <p>any of the entities presented in the list provided by the “Direção Geral do Consumidor” in</p>\
            <p><a href="http://www.consumidor.pt/" target="_blank">http://www.consumidor.pt/</a> we advise an earlier consultation to this website.</p>',


        ERROR_PAGE_TITLE:'Error 404<br />The content you request could not be found.',
        ERROR_PAGE_TEXT:'Don’t worry, the website<br />will be back on track.',

        CLICK2CALL:'WE CALL!',

        WHOWEARE:'WHO WE ARE',
        SPORTS:'SPORTS',
        CORPORATE:'CORPORATE',
        INCENTIVES:'INCENTIVES',
        LEISURE: 'LEISURE',
        ABOUTUS:'About Us',
        MISSION_VISION:'Mission & Vision',
        CLIENTS_PARTNERS:'Clients & Partners',
        LOCATIONS:'Locations',
        SERVICES: 'Services',
        TEAM_SUPPORT: 'Team & Support',
        TESTIMONIALS: 'Testimonials',
        PACKAGES: 'Packages',
        HIGHLIGHTS: 'Highlights',
        CONTACT:'Contact',
        COOKIES: 'Cookies',
        CUSTOMER_INFORMATION: 'Customer Information',

        SCHEDULE: '(Valid for Portugal. More contacts click <a class="link" href="#/en/aboutus/locations">here</a>)<br />Week days from 9.30 am to 6.30 pm!',
        FOOTER_SUBSCRIBE_PLACEHOLDER: 'Subscribe Newsletter',
        FOOTER_SUBSCRIBE_SUCCESS_MESSAGE: 'Thank you for subscribing our newsletter.',
        FOOTER_SUBSCRIBE_ERROR_MESSAGE: 'Invalid email.',

        CONTACT_HEADER: '<h2>GET IN CONTACT</h2>\
            <p>\
            We are commited to give a fast and efficient answer to our clients 24h / 365d!<br />\
            To get in contact please fill the spaces below or call us.</p>',
        NAME:'Name',
        PHONE:'Phone',
        EMAIL:'Email',
        CONTACT_TERMS: 'I AGREE TO THE TERMS AND CONDITIONS.',
        CONTACT_ERROR_REQUIRE_MESSAGE: 'ALL FIELDS ARE REQUIRED.',
        CONTACT_SUCCESS_MESSAGE: 'Your request was submitted properly.<br />Soon you will be contacted.',
        CONTACT_SUBMIT_BT: 'REQUEST CONTACT',


        HOME_SLIDER1: '<b>Customized travelling <br />experiences</b> for you and your business.',
        HOME_SLIDER1_BT:'Start planning',

        HOME_SLIDER2: 'Be there on your favorite sports\' events.<br />Live them to be most!',
        HOME_SLIDER2_BT: 'Get quote',

        HOME_SLIDER3: 'We help you leverage the motivation<br/>of your team’s top performers!',
        HOME_SLIDER3_BT: 'Start rewarding',

        HOME_SLIDER4: 'Your dream holidays can come to life <br />with our exclusive taylor-made solutions.',
        HOME_SLIDER4_BT: 'Book it',

        HOME_HEADER: '<h1>COMPANY SPECIALISED IN TRAVEL MANAGEMENT<br />CREATING A UNIQUE <b>EXPERIENCE</b> FOR YOUR TEAM STARTS HERE.</h1>\
            <p>Trips planned by specialized consultants.<br />Personalized service 24 hours / 365 days.</p>',

        HOME_WE_ARE_LEADERS: '<h1>LEADING SERVICE</h1>\
            <p>Experience and a close relationship with our partners will match your best expectations concerning transportation logistics,<br />accomodation and the comfort of your team, club, followers, sponsors or press.</p>',

        ABOUTUS_DISCOVER: 'Discover more about us',

        ABOUTUS_WEARETOGETHER: 'WE <b>ARE</b> TOGETHER',
        ABOUTUS_DESCRIPTION: 'COSMOS is a company providing travel services, business travel specialists (Corporate Travel Management) , sport related tourism, corporate incentives and leisure travel.\
            <br /><br />\
            Founded in 1986, COSMOS occupies a prominent place in the travel industry, having established over the nearly 30 years of experience in a differentiated positioning market marked by high levels of service and proximity to customers that distinguishes it with your preference.\
            <br /><br />\
            With an anual consolidated business volume higher than 50 million euros, COSMOS currently has operations in Portugal (Lisbon, Oporto) and Angola (Luanda, Benguela), offering its clients a wide range of solutions "value for money" with a high level of customization and focus on the specific objectives of each situation, 24 hours a day, 365 days a year.\
            <br /><br />\
            COSMOS is a shareholder of the national group <a class="text" href="http://www.go4travel.pt/" target="_blank">GO4TRAVEL</a>, one of the top 3 operators in the domestic market, with a turnover of 315M € added. This dimension gives its members access to highly competitive conditions, enhancing value in the relationship with their clients.\
            <br /><br />\
            COSMOS is also part of the prestigious international network <a class="text" href="http://www.twintravelmanagement.com" target="_blank">TRAVELSAVERS</a>, with presence in more than 30 countries, with a billing volume higher than 20 Billion USD, with a wide range of services and technological solutions for an integrated business travel management of its customers, globally.\
            <br /><br />\
            COSMOS relies in more than 90 professionals, highly trained and motivated, that share value ​​in the relentless pursuit of customer satisfaction, this is the key factor of success and competitiveness of the company, a major source of our value proposition.',

        ABOUTUS_WETRAVELTOGETHER: 'WE <b>TRAVEL</b> TOGETHER',
        ABOUTUS_MISSION: '<h2 class="txt-center">MISSION</h2>\
            <p>Having recently started a new phase of its journey, with ambition and growth objectives renovated in current geographies and in new emerging markets, COSMOS intends to continue to support the process of internationalization of Portuguese companies in the area of travel and to promote Portugal in international markets , leveraging new traffic moves to and from the countries where it operates.</p>',
        ABOUTUS_VISION: '<h2 class="txt-center">VISION</h2>\
            <p>Looking into the future, COSMOS is dedicated to building long-term relationships based on professional excellence, respect, trust and sustained value creation.</p>',

        ABOUTUS_WEGROWTOGETHER: 'WE <b>GROW</b> TOGETHER',
        ABOUTUS_WEGROWTOGETHER_TXT: '<p>Our success relies directly on the long term sustainable relashionship with our <b>clients, suppliers</b> and <b>partners</b>.</p>',

        ABOUTUS_LOCATIONS_TITLE: 'LOCATIONS',
        ABOUTUS_LOCATIONS_OPORTO: '<b>OPORTO</b>\
                <br /><br />\
                Edifício Jornal de Notícias<br />Rua Gonçalo Cristóvão,<br />nr. 195, 4º andar, 4049-011 <br />Phone:+351 22 209 7600 <br />Fax:+351 22 209 7650<br />\
                <a href="https://www.google.pt/maps/place/COSMOS+-+Viagens+E+Turismo,+S.A./@41.1533817,-8.6108337,17z/data=!4m7!1m4!3m3!1s0xd2464fbc665c4f7:0x907f8648e6b6d651!2sRua+de+Gon%C3%A7alo+Crist%C3%B3v%C3%A3o+195,+4000-413+Porto!3b1!3m1!1s0xd2464fbc665c4f7:0x61801cf8aa84c835" target="_blank">View in Google Maps</a>\
                <br /><br />\
                Estádio do Dragão<br />Lado Nascente, Porta 15, 4350-145 <br />Phone:+351 22 557 49 10 <br />Fax:+351 22 502 5984<br />\
                <a href="https://www.google.pt/maps/place/Est%C3%A1dio+do+Drag%C3%A3o/@41.16177,-8.5857797,17z/data=!3m2!4b1!5s0xd246462af0b2f99:0x7f69ad272311f9c!4m2!3m1!1s0xd246502a2a20bf7:0x1ad7c8d540f6cbe8" target="_blank">View in Google Maps</a>',
        ABOUTUS_LOCATIONS_LISBON: '<b>LISBON</b>\
                <br /><br />Rua Abranches Ferrão <br />nr. 10, andar 10º F, 1600 – 001  <br />Phone:+351 21 724 8360  <br />Fax:+351 21 723 7050 <br />\
                <a href="https://www.google.pt/maps/place/R.+Abranches+Ferr%C3%A3o+10,+1600-001+Lisboa/@38.7506044,-9.1736249,17z/data=!3m1!4b1!4m2!3m1!1s0xd193320ee4f0695:0xaee57f9955aa7628" target="_blank">View in Google Maps</a>',
        ABOUTUS_LOCATIONS_LUANDA: '<b>LUANDA</b>\
                <br /><br />Condomínio Garça, nº 28 - Belas – Gamek, Luanda, Angola<br />Phone: +244 935 682 515<br />Mobiles: +244 949 487 047 and<br /> +244 937 921 221<br />\
                <a href="https://www.google.pt/maps/place/Condom%C3%ADnio+Veredas+das+Flores/@-8.9684728,13.2973745,13z/data=!4m2!3m1!1s0x0:0xf19b620f695c7fc6" target="_blank">View in Google Maps</a>\
                <br /><br />\
                Hotel Praia Morena<br />R. José Estevan, 25, Benguela , Angola<br />Phone:  +244 934 454 009<br />\
                <a href="https://www.google.pt/maps/place/Praia+Morena+Hotel/@-12.581665,13.4092924,13.87z/data=!4m6!1m3!3m2!1s0x1bafd174eee689c3:0xfd26967b5a4f476d!2sPraia+Morena+Hotel!3m1!1s0x1bafd174eee689c3:0xfd26967b5a4f476d?hl=en" target="_blank">View in Google Maps</a>',


        SPORTS_INTRO1: '<p>\
                COSMOS is leading the Sports Tourism segment.With 30 years of experience in this segment, COSMOS hosted numerous sports operations, such as world championships and european competitions, representing the transport about 35,000 passengers on more than 160 chartered aircraft.<br />\
                COSMOS offer an integrated service that includes planning and execution of the transportation logistics, accommodation and hospitality for sports teams, clubs, fans, sponsors and the press.<br />\
                COSMOS is proud to establish close relations with several national and international clubs, federations and other sports bodies, including the MATCH / FIFA and UEFA, and to be the official agency of the Portuguese Football Federation. \
            </p>',
        SPORTS_INTRO2:'<p>Everything we do aims to exceed our stakeholders’ expectations while assuring operational excellence:</p>\
            <ul>\
                <li>Premium high quality services that value uniqueness and security;</li>\
                <li>Know- how, experience and reputation in logistics for sports’ events management;</li>\
                <li>Long-term and result oriented planning;</li>\
                <li>High flexibility and efficient solution solver;</li>\
                <li>Strict selection and close relationship with suppliers;</li>\
                <li>Cost optimization policy;</li>\
                <li>Periodic reports and meetings to analyze and assure the best evolution of results.</li>\
            </ul>',

        SPORTS_TEAM_1: 'Exclusive and dedicated managers',
        SPORTS_TEAM_2: 'Support connections and fine relationship with clubs, leagues and federations',
        SPORTS_TEAM_3: 'Flexible and personalized assistance: COSMOS 24H|365D',
        SPORTS_TEAM_4: 'Advanced technology for the best technical support',
        SPORTS_TEAM_5: 'Portuguese and/or english speaking staff available in all national airports',
        SPORTS_TEAM_6: 'Attending travel assistant',

        SPORTS_QUOTE1: 'OUR CLIENTS LIVE <b>EXCLUSIVE</b> EMOTIONAL EXPERIENCES',
        SPORTS_QUOTE2: '<b>WE SCORE HIGHER</b> DUE TO OUR CONSOLIDATED EXPERIENCE<br />AND SUSTAINABLE RELATIOSHIP WITH OUR PARTNERS.',

        SPORTS_NEWS_1:'<h2>23 YEARS<br />AS THE OFFICIAL AGENCY OF<br />FEDERAÇÃO PORTUGUESA DE FUTEBOL</h2>',

        SPORTS_CLIENTS_HEADER:'WE PROMOTE WORLDWIDE YOUR NATIONAL SPORT EVENTS',
        SPORTS_CLIENTS_TXT:'Organize Sports Tours Worldwide <span class="bullet"></span> Create Your Own Tournaments <span class="bullet"></span> Prepare for the Season in our Sports Camps',
        SPORTS_CLIENTS_BUTTON:'Visit COSMOS ACTIVE',

        CORPORATE_INTRO1: '\
            <p>As a leader in corporate travel management, COSMOS provides personalized services for companies that:</p>\
            <ul>\
                <li>Wish to optimize travel costs, respect budgets and achieve results.</li>\
                <li>Require the highest quality and expect to have a Premium service;</li>\
                <li>Wish to have an reliable and flexible partner;</li>\
                <li>Value experience and availability;</li>\
            </ul>\
            <p>COSMOS offer competitive rates due to long-term agreements with major international operators. Using the latest technological tools in the search for better results, comparing prices and quality control.</p>',

        CORPORATE_INTRO2:'<p>We will be your partner in the definition of an efficient travel policy.</p>\
            <p>Our management model optimizes results in quality and savings. We also suggest a joint establishment of annual goals, in order to achieve a sustainable and constant optimization of the main key performance indicators during the year.</p>\
            <br />\
            <p>Your company can count on:</p>\
            <ul>\
                <li>Quick and efficient response;</li>\
                <li>Hotline 24H;</li>\
                <li>Multiple solutions for your request;</li>\
                <li>Looking for the best the market as to offer;</li>\
                <li>Continuous support.</li>\
            </ul>',

        CORPORATE_TEAM_SUPPORT:'<h2>TEAM & SUPPORT</h2>\
            <p>\
                To achieve an excellent level of service, our team works on anticipating our clients’ needs and understanding their requirements.\
                <br />\
                We are detailed oriented and we aim to adjust our services to the profiles and preferences of the people involved.\
                <br />\
                <br />\
                We centralize our operations in a Business Travel Center (BTC) so we can supervise and control the most strict processes,\
                <br />\
                consultation procedures, reservations and ticketing. \
                <br />\
                You can count on:\
            </p>',
        CORPORATE_TEAM_1:'<b>Key Account Manager</b><br />\
            Monetizes business evolution and identifies the factors that can drive continuous improvement in service, with an impact in cost reduction. Elaborates, analyses and comments on management reports, dealswith suppliers and helps in post-sale services.',
        CORPORATE_TEAM_2:'<b>Traveler Consultant</b><br />\
            With previous corporate account experience, travel consultants ensure that daily requests are processed according to what was agreed with clients.',
        CORPORATE_TEAM_3:'<b>Supervisior Operacional</b><br />\
            Ensures that agreed levels of service and traveling policies are achieved as well as he supervises the overall operation proceedures. Coordinates the team of travel consultants.',
        CORPORATE_TEAM_4:'<b>COSMOS 24H</b><br />\
            You can contact us anywhere and at any time.<br />\
            A team will be assigned to give you the assistance you may need before, during and after travelling with COSMOS.<br />\
            + 351 910 899 814',


        INCENTIVES_INTRO1: '<p>\
                Our goal is to give a shape to the world you have imagined!<br />\
                We come up with tailor-made events and solutions that create unique moments.<br />\
                If you want to go on a trip that that will last forever COSMOS is the right partner for you.\
            </p>',
        INCENTIVES_INTRO2: '<p>With 30 years of experience in corporate travel and incentive, COSMOS is the business partner you need in order to:</p>\
            <ul>\
                <li>Generate the highest return for your business / profitability of the marketing budget;</li>\
                <li>Increase sales and create value for your products;</li>\
                <li>Reward  your employees with unique and financially immeasurable emotions;</li>\
                <li>Generate experiences whose intangible value last forever.</li>\
            </ul>',

        INCENTIVES_PACKAGES_HEADER: 'Develop events that can boost your business!',

        INCENTIVES_PACKAGES_INTERNAL: ' <h4>Internal actions</h4>\
            <ul>\
                <li>Promote the motivational consolidation and alignment of your teams;</li>\
                <li>Rewards the performance of your employees and boost your results;</li>\
                <li>Ensure retention of key employees.</li>\
            </ul>',
        INCENTIVES_PACKAGES_EXTERNAL: '<h4>External actions</h4>\
            <ul>\
                <li>Develop your business / launch of new products (B2B or B2C);</li>\
                <li>Promote the relationship with your best customers;</li>\
                <li>Invite your strategic partners for the next incentive trip.</li>\
            </ul>',

        INCENTIVES_TEAM_SUPPORT:'<h2>TEAM & SUPPORT</h2>\
            <h5>We create the world you have imagined! Contact us!</h5>\
            <p>\
                Creativity <span class="bullet"></span> Availability <span class="bullet"></span> Knowledge <span class="bullet"></span> Experience <span class="bullet"></span> Originality <span class="bullet"></span> Confidenciality <span class="bullet"></span> Competence <span class="bullet"></span> Dedication\
            </p>',
        INCENTIVES_HOW_TXT: '<ul>\
                <li>We plan and execute a project that is exclusive to your brand / company;</li>\
                <li>We create the key visuals and produce digital and physical support materials;</li>\
                <li>We offer multilingual staff for local service;</li>\
                <li>We are available throughout the journey.</li>\
            </ul>',


        LEISURE_INTRO1:'<p>\
                Our 30 years of experience allow us to guarantee the best service in the making of your holiday trips.<br />\
                We dedicate our time to discover little secrets, remote areas or places, where nature, art, science or energy keep intact the ability to surprise us.<br />\
                Each trip is designed by a highly skilled and flexible team, which guarantees a personalized and unique support, since the first moment.\
            </p>',
        LEISURE_INTRO2:'<p>A tailor-made travel plan that can meet your wildest dreams and fit in your budget! You dream it, we do it!</p>\
            <ul>\
                <li>A private jet for dinner in Paris!</li>\
                <li>A ride aboard of a luxury yacht in French Polynesia?</li>\
                <li>24/7 buttler service?</li>\
            </ul>\
            <p>\
                We take care of every single detail from beginning to end! <br />\
                We advise you with the best solutions and provide excellence in service.\
            </p>',

        LEISURE_TEAM_SUPPORT:'<h2>TEAM & SUPPORT</h2>\
            <h5>You want it? You get it:</h5>\
            <p>\
                Dedication <span class="bullet"></span>\
                Professionalism <span class="bullet"></span>\
                Honesty <span class="bullet"></span>\
                Confidentiality <span class="bullet"></span>\
                Availability <span class="bullet"></span>\
                Detail oriented <span class="bullet"></span>\
                Sympathy <span class="bullet"></span>\
                Trust\
            </p>',

        LEISURE_QUOTE1:'WE WILL MAKE <em>your dreams</em> COME TRUE',
        LEISURE_QUOTE2:'<em>for</em> TWO',
        LEISURE_QUOTE3:'<em>for</em> FAMILY',
        LEISURE_QUOTE4:'<em>for</em> FRIENDS',
        LEISURE_QUOTE5:'<em>for</em> YOU',


    }
}

app.constant('TRANSLATE', translate);

app.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);

        for (var i=1; i<=total; i++) {
          input.push(i);
        }

        return input;
    };
});
