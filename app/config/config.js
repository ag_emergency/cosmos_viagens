var configFn = function ($stateProvider, $urlRouterProvider, $httpProvider, CONSTANTS, TRANSLATE, $translateProvider) {


	/* other resolves */

    var translate = function($q , $state, $stateParams, $rootScope, $translate) {
        var deferred = $q.defer();

        $translateProvider.useSanitizeValueStrategy(null);

        $rootScope.lang = $translate.use();
        if ($stateParams.lang.length >= 1){
            $translate.use($stateParams.lang);
            $rootScope.lang = $stateParams.lang;
        }

        deferred.resolve(true);
        return deferred.promise;
    }

    var cookiebar = function($q , $state, $translate) {
        var deferred = $q.defer();



        $translate(['COOKIE_TEXT', 'COOKIE_YES_BUTTON', 'COOKIE_POLICY']).then(function (translate) {
            $("#cookie-bar").remove();
            $.cookieBar({
                message: translate.COOKIE_TEXT,
                acceptText: translate.COOKIE_YES_BUTTON,
                policyButton: true,
                policyText: translate.COOKIE_POLICY,
                //policyURL: '/privacy-policy/',
                policyURL: 'COOKIE_MODAL_TEXT',
                effect: 'slide',
                //domain: 'www.example.com',
                //referrer: 'www.example.com',
                element:'body'
            });
            deferred.resolve(true);
        });

        return deferred.promise;
    }



    var scrollCostumController = function($rootScope, $stateParams, $translate, $timeout){
        var vm = this;

        $timeout(function(){
            var $section = $("#"+$stateParams.section);
            if($section.length){
                $timeout(function(){
                    var top = $section.offset().top;
                    if($stateParams.section == "services"){
                        top=top-160;
                    }

                    $('html,body').animate({
                        scrollTop: top
                    }, 1000);
                },300);
            }else{
                $("html,body").scrollTop(0);
            }

            var haveContact = $("#contact").length;
            $(".callme").show();
            if(!haveContact){
                $(".callme").hide();
            }
        },200);
        return false;
    };

	/* states */
	$stateProvider

    .state('root', {
        url: '/{lang:(?:en|pt)}',
        resolve: {
            translate:translate,
            cookiebar:cookiebar
        }
    })
        .state('root.home', {
            url: '/home/:section',
            templateUrl: 'app/views/home.html',
            controllerAs: 'home',
            resolve: {},
            params: {
                section: {squash: true, value: null}
            },
            controller: scrollCostumController
        })
        .state('root.aboutus', {
            url: '/aboutus/:section',
            templateUrl: 'app/views/aboutus.html',
            resolve: {},
            params: {
                section: {squash: true, value: null}
            },
            controller: scrollCostumController
        })
        .state('root.corporate', {
            url: '/corporate/:section',
            templateUrl: 'app/views/corporate.html',
            resolve: {},
            params: {
                section: {squash: true, value: null}
            },
            controller: scrollCostumController
        })

        .state('root.sports', {
            url: '/sports/:section',
            templateUrl: 'app/views/sports.html',
            resolve: {},
            params: {
                section: {squash: true, value: null}
            },
            controller: scrollCostumController
        })

        .state('root.incentives', {
            url: '/incentives/:section',
            templateUrl: 'app/views/incentives.html',
            resolve: {},
            params: {
                section: {squash: true, value: null}
            },
            controller: scrollCostumController
        })
        .state('root.leisure', {
            url: '/leisure/:section',
            templateUrl: 'app/views/leisure.html',
            resolve: {},
            params: {
                section: {squash: true, value: null}
            },
            controller: scrollCostumController
        })

    	.state('root.error', {
    		url: '/error',
    		templateUrl: 'app/views/404.html',
            resolve: {},
            params: {
                section: {squash: true, value: null}
            },
            controller: scrollCostumController
    	})
	;

	/* redirects */
    var defaultLang = "pt";
    var home = '/'+defaultLang+'/home';

	$urlRouterProvider.when('', home);
	$urlRouterProvider.when('/', home);
	$urlRouterProvider.otherwise('/'+defaultLang+'/error');

	/* interceptor */
	//$httpProvider.interceptors.push('requestInterceptor');

    $translateProvider.translations('pt', TRANSLATE.PT);
    $translateProvider.translations('en', TRANSLATE.EN);
    $translateProvider.preferredLanguage(defaultLang);

};

app.config(configFn);


app.controller('TranslatorCrl', function ($scope, $translate, $rootScope, $state) {
  lang=$rootScope.lang;

  $scope.setLang = function (key) {
    $translate.use(key);
    $rootScope.lang = key;
    $state.go($state.current.name, {lang:key});
  };
  $scope.selectLang = function() {
    $translate.use($scope.lang);
    $rootScope.lang = $scope.lang;
    return true;
  };
});