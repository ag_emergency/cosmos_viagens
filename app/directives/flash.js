app.directive('flash', function() {
    return {
        restrict: 'A',
        templateUrl: 'app/directives/flash.html',
        replace: true,
        scope: false
    }
});