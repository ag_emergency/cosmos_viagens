app.directive('header', function($timeout, $state,$rootScope) {
    return {
        restrict: 'A',
        templateUrl: 'app/views/partials/header.html',
        replace: false,
        scope: false,
        link: function (scope, element, attr) {

            $timeout(function(){
                element
                .on('click', '.main', function(e) {
                    e.preventDefault();
                    if($("header .top:visible").length){
                        $(this).parents("header").toggleClass('open');
                        if($rootScope.section.main != "home" && $rootScope.section.main != "error" ){
                            $(this).parents("body").find(".callme").toggle();
                        }
                    }else{
                        $("html,body").scrollTop($(".shifter_wrapper").position().top);
                        $.shifter("open");
                    }
                    return false;
                })
                .on('click', '.logo', function(e) {
                    e.preventDefault();
                    $state.go('root.home');
                    return false;
                })
                .on('click', '.lang_wrapper', function(e) {
                    e.preventDefault();
                    $(this).toggleClass('open');
                    return false;
                });

                $.shifter();
                $.shifter("enable");

                $("body").on('click','.truncatemobile', function(e){
                    e.preventDefault();
                    $(this).hide().next().slideDown();
                });
            },300);

            /*var resizing;
            $(window).on("resize",function(){
                clearTimeout(resizing);
                resizing = setTimeout(function(){
                    console.log($("#cookie-bar").outerHeight());
                    if($("#cookie-bar").length>0){
                        $("header").css(
                            //{"top":-182+$("#cookie-bar").outerHeight()}
                            {"top":"-182px"}
                        );
                    }else{
                        $("header").css({"top":-182});
                    }
                }, 200);
            });*/
        }
    }
});
app.directive('footer', function($timeout) {
    return {
        restrict: 'A',
        templateUrl: 'app/views/partials/footer.html',
        replace: false,
        scope: true,
        controller: function ($scope, $http) {
            $timeout(function(){
                $scope.submit = function (isValid, input) {
                    if (!isValid) return;

                    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                    $http({
                        method: 'POST',
                        url: 'api/mailinglist',
                        data: input,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data) {
                        if(data.success){
                            var $form = $("#newsletter");
                            $form.find(".lead").hide();
                            $form.find(".success").show();
                        }
                    }).error(function (error) {
                        console.log(error);
                    });
                };
            });
        }
    }
});
app.directive('menu', function() {
    return {
        restrict: 'E',
        templateUrl: 'app/views/partials/menu.html',
        replace: true,
        scope: false
    }
});
app.directive('contactusform', function($timeout) {
    return {
        restrict: 'C',
        templateUrl: 'app/views/partials/contactus.html',
        replace: false,
        scope: true,
        controller: function ($scope,$http) {
            $timeout(function(){
                $scope.submit = function (isValid, input) {
                    if (!isValid) return;

                    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                        /*
                        $http.post('api/contacts', input)
                            .success(function (success) {console.log(success);})
                            .error(function (error) {console.log(error);});
                        */
                    var $form = $("#contact form");
                    
                    $form.parent().find(".lead").hide();
                    $form.find(".loading").removeClass('ng-hide');

                    $http({
                        method: 'POST',
                        url: 'api/contacts',
                        data: input,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data) {
                        if(data.success){
                            
                            $form.find(".lead").hide();
                            $form.find(".loading").addClass('ng-hide');
                            $form.find(".success").show();
                        }
                    }).error(function (error) {
                        console.log(error);
                    });
                };
            });
        }
    }
});
app.directive('sliderWrapper', function($timeout) {
    return {
        restrict: 'C',
        replace: false,
        scope: true,
        link: function (scope, element, attr) {
            var auto = (attr.auto != undefined && attr.auto == "true")?true:false;

            $timeout(function(){
                $(element).slippry({
                    // general elements & wrapper
                    slippryWrapper: '<div class="sy-box slider" />', // wrapper to wrap everything, including pager
                    elements: 'article', // elments cointaining slide content

                    // options
                    adaptiveHeight: true, // height of the sliders adapts to current
                    captions: 'bellow',

                    // pager
                    pager:false,
                    pagerClass: 'pager',

                    // transitions
                    transition: 'fade', // fade, horizontal, kenburns, false
                    speed: 1200,
                    pause: 7000,

                    continuous: false,
                    auto:auto,

                    // slideshow
                    autoDirection: 'next'
                });
            },200);

        }
    }
});

app.directive('sliderApiWrapper', function (imagesF, $timeout) {
    return {
        restrict: 'C',
        replace: false,
        scope: true,
        link: function (scope, element, attr) {

            imagesF.getSlides(attr.lang, attr.mobile, attr.category)
            .then(function (data) {
                scope.images = data;

                $timeout(function () {

                    $(element).slippry({
                        // general elements & wrapper
                        slippryWrapper: '<div class="sy-box slider" />', // wrapper to wrap everything, including pager
                        elements: 'article', // elments cointaining slide content

                        // options
                        adaptiveHeight: true, // height of the sliders adapts to current
                        captions: 'bellow',

                        // pager
                        pager:false,
                        pagerClass: 'pager',

                        // transitions
                        transition: 'horizontal', // fade, horizontal, kenburns, false
                        speed: 1200,
                        pause: 7000,

                        continuous: false,
                        auto:false,

                        // slideshow
                        autoDirection: 'next'
                    });

                });

            });
        }
    }
});

app.directive('testimonialsApiWrapper', function (imagesF, $timeout) {
    return {
        restrict: 'C',
        replace: false,
        scope: true,
        link: function (scope, element, attr) {

            imagesF.getTestimonials(attr.lang, attr.mobile, attr.category)
            .then(function (data) {
                scope.images = data;

                $timeout(function () {
                    //console.log("slide testemunhos");
                    //console.log(element);
                    $(element).slippry({
                        // general elements & wrapper
                        slippryWrapper: '<div class="sy-box slider" />', // wrapper to wrap everything, including pager
                        elements: 'article', // elments cointaining slide content

                        // options
                        adaptiveHeight: true, // height of the sliders adapts to current
                        captions: 'bellow',

                        // pager
                        pager:false,
                        pagerClass: 'pager',

                        // transitions
                        transition: 'horizontal', // fade, horizontal, kenburns, false
                        speed: 1200,
                        pause: 7000,

                        continuous: false,
                        auto:false,

                        // slideshow
                        autoDirection: 'next'
                    });

                });

            });
        }
    }
});


app.directive('sportsClientsApiWrapper', function (imagesF, $timeout) {
    return {
        restrict: 'C',
        replace: false,
        scope: true,
        link: function (scope, element, attr) {

            imagesF.getSportsClients(attr.lang, attr.mobile)
            .then(function (data) {
                scope.images = data;

                $timeout(function () {
                    //console.log("slide testemunhos");
                    //console.log(element);
                    $(element).slippry({
                        // general elements & wrapper
                        slippryWrapper: '<div class="sy-box slider" />', // wrapper to wrap everything, including pager
                        elements: 'article', // elments cointaining slide content

                        // options
                        adaptiveHeight: true, // height of the sliders adapts to current
                        captions: 'bellow',

                        // pager
                        pager:false,
                        pagerClass: 'pager',

                        // transitions
                        transition: 'horizontal', // fade, horizontal, kenburns, false
                        speed: 1200,
                        pause: 7000,

                        continuous: false,
                        auto:false,

                        // slideshow
                        autoDirection: 'next'
                    });

                });

            });
        }
    }
});


app.directive('slick', function (imagesF, $timeout) {
    return {
        restrict: 'C',
        replace: false,
        scope: true,
        link: function (scope, element, attr) {
            imagesF.getPartners()
            .then(function (data) {
                scope.partners = data;
                $timeout(function () {


                    $(element).slick({
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed:3000,
                        speed: 1000,
                        slidesToShow: 5,
                        slidesToScroll: 5,
                        arrows: true,
                        //prevArrow: '<a href="#" class="actions icon-arrow-left prev"></a>',
                        //nextArrow: '<a href="#" class="actions icon-arrow-right next"></a>',
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 4
                                }
                            },
                            {
                                breakpoint: 850,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 430,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    dots: false
                                }
                            }
                        ]
                    });
                });

            });
        }
    }
});

app.directive('ajustHeader', function($rootScope, $timeout){
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element) {

            $("img:first",element).bind("load",function(){
                $timeout(function(){
                    var adjustHeader = function(){
                        var $header = $(".header");
                        //console.log($header.find("img:first").attr("src"));
                        if($header.find("img:first").height() > 0){
                            $timeout(function(){
                                var delta = ($header.find("img:first").height() - $(window).height());
                                //console.log(delta);
                                if(delta>0){
                                    $(".ajustheader").height($(window).height())
                                        .find(".slider").css("top",-delta);
                                }else{
                                    $(".ajustheader").height("auto")
                                        .find(".slider").css("top","auto");
                                }
                            });
                        }else{
                            $header.find("img:first").load(function(){
                                $timeout(function(){
                                    var delta = ($header.find("img:first").height() - $(window).height());
                                    //console.log("load",delta);
                                    if(delta>0){
                                        $(".ajustheader").height($(window).height())
                                            .find(".slider").css("top",-delta);
                                    }else{
                                        $(".ajustheader").height("auto")
                                            .find(".slider").css("top","auto");
                                    }
                                });
                            });
                        }
                    }

                    adjustHeader();

                    var resizing;
                    $(window).on("resize",function(){
                        clearTimeout(resizing);
                        resizing = setTimeout(adjustHeader, 500);
                    });
                });
            });
        }
    }
});


app.directive('sportsAnimation',function($timeout){
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if(!scope.mobile){

                $timeout(function(){
                    var $clients = $("#clients");
                    $clients.find("[class*=icon-sports]").css({"visibility":"hidden"});

                    var waypoint_down = new Waypoint({
                        //element: $clients,
                        element: document.getElementById('clients'),
                        handler: function(direction) {
                            if(direction == "down"){
                                $(element).find(".icons span").removeClass("animated slideInLeft slideOutRight");
                                var timer = 1000;
                                $(element).find(".icons span").each(function(i,e){
                                    $timeout(function(){
                                        $(e).css({"visibility":"visible"}).addClass('animated slideInLeft');
                                    },timer=timer-100);
                                })
                            }
                        },
                        offset: "70%"
                    });

                },500);
            }
        }
    }
});

app.directive('socialbuttons', function() {
    return {
        restrict: 'C',
        templateUrl: 'app/views/partials/sociallinks.html',
        replace: false,
        scope: false
    }
});

app.directive('callme', function($timeout) {
    return {
        restrict: 'E',
        templateUrl: 'app/views/partials/callme.html',
        replace: true,
        scope: true,
        link: function (scope, element) {
            $timeout(function(){
                $(element).bind("click",function(e){
                    e.stopPropagation();

                    $timeout(function(){
                        $('html,body').animate({
                            scrollTop: $("#contact").offset().top
                        }, 1000);

                        $("#contact h2").html($(".callme p").text())
                    },200);
                });
            });
        }

    }
});


/*
app.directive('parallax', function(){
    return{
        restrict: 'C',
        link: function (scope, element) {
            if(!scope.mobile){
                $('.parallax').scrolly();
            }
        }
    }
});*/
app.directive('shifter', function($timeout){
    return{
        restrict: 'C',
        link: function (scope, element) {
            $timeout(function(){
                $.shifter({
                    //maxWidth: "768px"
                    maxWidth: "1260px"
                });
            });
        }
    }
});
app.directive('backButton', function($window) {
    return {
        restrict: 'A',
        link: function(scope, elem, attr) {
            elem.bind('click', function() {
                $window.history.back();
            });
        }
    };
});

app.directive('clicktoup', function($window, $timeout) {
    return {
        restrict: 'C',
        link: function(scope, elem, attr) {
            $timeout(function(){
                elem.bind('click', function(e) {
                    e.stopPropagation();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 1000);
                });
            });
        }
    };
});

app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('modalbox', function ($timeout, ngDialog) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {

            $timeout(function () {
                scope.openmodal = function(t){
                    scope.modal_text = t;
                    ngDialog.open(
                        {
                            template: 'app/views/partials/modalbox.html' ,
                            scope: scope
                        }
                    );
                }
                $("body").on("click", "#cookie-bar .cb-policy", function(e){
                    e.preventDefault();

                    scope.modal_text = $(this).attr("href");//COOKIE_MODAL_TEXT
                    ngDialog.open(
                        {
                            template: 'app/views/partials/modalbox.html' ,
                            scope: scope
                        }
                    );
                });
            });

        }
    }
});

/*
app.directive('lazy', function() {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element) {
            element.attr("src",scope.CONSTANTS.IMG + "/blank.png");
            $(window).load(function() {
                $("img[lazy]").each(function(i,e){
                    $(e).attr("src",$(e).attr("lazy"));
                })
            });
        }
    }
});
*/

