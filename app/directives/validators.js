var pattern = {
	name: /^[a-zA-Z\u00C0-\u017F]{2,}\s[a-zA-Z\u00C0-\u017F]{2,}$/,
	password: /^.{4,}$/,
	cc: /^\d{6,}$/,
	email: /^[A-Z0-9._%-]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z]{2,6}$/i,
	url: /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&amp;?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/,
	voucher: /^.+$/,
	number: /^\d+$/
};

angular.module('validators', [])

.directive('validateName', function() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$validators.validateName = function(value) {
				return pattern.name.test(value);
			};
		}
	};
})

.directive('validateNumber', function() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$validators.validateNumber = function(value) {
				return pattern.number.test(value);
			};
		}
	};
})

.directive('validatePassword', function() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$validators.validatePassword = function(value) {
				return pattern.password.test(value);
			};
		}
	};
})

.directive('validateCartaoCidadao', function() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$validators.validateCartaoCidadao = function(value) {
				return pattern.cc.test(value);
			};
		}
	};
})

.directive('validateEmail', function() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$validators.validateEmail = function(value) {
				return pattern.email.test(value);
			};
		}
	};
})

.directive('validateUrl', function() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$validators.validateUrl = function(value) {
				return pattern.url.test(value);
			};
		}
	};
})

;