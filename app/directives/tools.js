angular.module('gh.tools', [])

.directive('backButton', function($window) {

    return {
        restrict: 'A',
        link: function(scope, elem, attr) {
            elem.bind('click', function() {
                $window.history.back();
            });
        }
    };
})

.directive('clearInput', function() {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attr, ngModel) {
            elem.bind('click', function() {
                ngModel.$setViewValue(null);
            });
        }
    };
})

.directive('focus', function() {

    return {
        restrict: 'A',
        link: function(scope, element) {
            element[0].focus();
        }
    };
})
;