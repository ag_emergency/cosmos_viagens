var preloaderF = function ($rootScope) {
	$rootScope.preloader = {
		show: true
	};

    return {
        show: function() {
            // $rootScope.preloader.show = true;
            $("#preloader").show();
        },

        hide: function() {
           // $rootScope.preloader.show = false;
           $("#preloader").fadeOut(1000);
        }
    };
};

app.factory('preloaderF', preloaderF);