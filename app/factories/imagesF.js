var imagesF = function ($http, $q, CONSTANTS) {

    var service = {};

    service.getSlides = function (lang, mobile, category) {
        var deferred = $q.defer();

        // var images = [
        //     { title: 'test1', url: null, src: 'uploads/slides/2a3abb672a1f3474979c028178e80661.jpg'},
        //     { title: 'test2', url: null, src: 'uploads/slides/2a3abb672a1f3474979c028178e80661.jpg'}
        // ];

        // deferred.resolve(images);

        var url = CONSTANTS.API + '/slides/';
        url += lang + '/';
        url += mobile + '/';
        url += category + '/';

        $http.get(url)
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    service.getTestimonials = function (lang, mobile, category) {
        var deferred = $q.defer();

        // var images = [
        //     { title: 'test1', url: null, src: 'uploads/slides/2a3abb672a1f3474979c028178e80661.jpg'},
        //     { title: 'test2', url: null, src: 'uploads/slides/2a3abb672a1f3474979c028178e80661.jpg'}
        // ];

        // deferred.resolve(images);

        var url = CONSTANTS.API + '/testimonials/';
        url += lang + '/';
        url += mobile + '/';
        url += category + '/';

        $http.get(url)
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    service.getPartners = function () {
        var deferred = $q.defer();

        var url = CONSTANTS.API + '/partners/';

        $http.get(url)
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };


    service.getSportsClients = function (lang, mobile) {
        var deferred = $q.defer();

        var url = CONSTANTS.API + '/sportclients/';
        url += lang + '/';
        url += mobile + '/';

        $http.get(url)
        .success(function (success) {
            deferred.resolve(success);
        }).error(function (error) {
            deferred.reject(false);
        });

        return deferred.promise;
    };

    return service;
}


app.factory('imagesF', imagesF);