var requestInterceptor = function ($q, $localStorage, CONSTANTS, flashF) {

    var service = {};

    /* On request success */
    service.request = function (config) {
        config.headers = config.headers || {};

        if ($localStorage[CONSTANTS.TOKEN_NAME]) {
            config.headers['X-Auth-Token'] = $localStorage[CONSTANTS.TOKEN_NAME];
        }

        return config || $q.when(config);
    };

    /* On request failure */
    service.requestError = function (rejection) {

        return $q.reject(rejection);
    };

    /* On response success */
    service.response = function (response) {

        return response || $q.when(response);
    };

    /* On response failture */
    service.responseError = function (rejection) {
        flashF.show(rejection.data.status, false);

        if(rejection.status === 401 || rejection.status === 403) {
            delete $localStorage[CONSTANTS.TOKEN_NAME];
        }

        return $q.reject(rejection);
    };

    return service;

};

app.factory('requestInterceptor', requestInterceptor);