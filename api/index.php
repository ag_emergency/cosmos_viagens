<?php

//print_r($_REQUEST);
//$params = json_decode(file_get_contents('php://input'),true);
//print_r($params);

error_reporting(E_ALL);
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/vendor/rb.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

function dump($d){
    echo "<pre>";
    print_r($d);
    echo "</pre>";
}

function sendEmail($sendToCosmos, $subject, $body){
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host = EMAIL_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = EMAIL_USERNAME;
    $mail->Password = EMAIL_PASSWORD;
    $mail->Port = EMAIL_PORT;
    $mail->isHTML(true);
    $mail->setFrom('noreply@cosmos-viagens.pt', 'Cosmos Viagens');

    //$mail->addAddress("brestolho@gmail.com");
    $mail->addAddress($sendToCosmos);

    $mail->addBCC('testes@rd-agency.com');

    $mail->Subject = $subject;
    $mail->Body    = $body;

    return $mail->send();
}

try{
    $app = new Silex\Application();

    //config
    $app['debug'] = true;
    R::setup('mysql:host=localhost;dbname=cosmosgo_db','cosmosgo_user',')xpLD?(laiB;');
    // R::setup('mysql:host=localhost;dbname=cosmosgo_db','root','');

    //sendemail
    // define('EMAIL_HOST', 'mail.emergency-agency.com');
    // define('EMAIL_USERNAME', 'info@emergency-agency.com');
    // define('EMAIL_PASSWORD', '2NwoD6xX0Lvu');
    define('EMAIL_HOST', 'kinder.siteweb.pt');
    define('EMAIL_PORT', 587);
    define('EMAIL_USERNAME', 'contact@cosmos-viagens.pt');
    define('EMAIL_PASSWORD', '8I5-3EwC3SWE');


    //$app->get('/hello/{name}', function($name) use($app) {
    //    return 'Hello '.$app->escape($name);
    //});
    R::ext('xdispense', function( $type ){
        return R::getRedBean()->dispense( $type );
    });

    $app->post('/contacts', function() use($app) {
        $params = json_decode(file_get_contents('php://input'),true);


        $contact = R::xdispense("_contacts");
        $contact->import( $params );
        $id = R::store($contact);

        $error_message = array("success"=>false, "message"=>"error inserting lead");

        if($id){
            $cosmos_emails = array(
                "aboutus"=>"comercial@cosmos-viagens.pt",
                "corporate"=>"comercial@cosmos-viagens.pt",
                "sports"=>"desporto@cosmos-viagens.pt",
                "incentives"=>"incentivos@cosmos-viagens.pt",
                "leisure"=>"lazer@cosmos-viagens.pt"
            );
            $sendToCosmos = "comercial@cosmos-viagens.pt";
            if(isset($cosmos_emails[$params["section"]])){
                $sendToCosmos = $cosmos_emails[$params["section"]];
            }

            $subject = 'Contacto Website COSMOS';
            $body = 'Os dados submetidos pelo utilizador / potencial cliente:
            <br /><br />
            Nome: '.$params["name"].'<br />
            Telefone: '.$params["phone"].'<br />
            Email: '.$params["email"].'<br />
            Área: '.$params["section"];

            $sent = sendEmail($sendToCosmos, $subject, $body);
            if(!$sent) {
                $error_message["message"] = "Message could not be sent.";
            } else {
                $message = array("success"=>true, "message"=>"Lead inserted");
                return new Response(json_encode($message), 201);
            }
        }

        return new Response(json_encode($error_message), 409);
        exit;
    });

    $app->post('/mailinglist', function() use($app) {
        $params = json_decode(file_get_contents('php://input'),true);

        $contact = R::xdispense("_mailinglist");
        $contact->import( $params );
        $id = R::store($contact);

        if($id){
            $message = array("success"=>true, "message"=>"Lead inserted");
            return new Response(json_encode($message), 201);
        }
        $error_message = array("success"=>false, "message"=>"error inserting lead");
        return new Response(json_encode($error_message), 409);
        exit;
    });


    $app->get('/slides/{lang}/{mobile}/{section}/', function($lang, $mobile, $section) use($app) {
        $list = null;
        $list = R::getAll("SELECT id, title, url, concat('uploads/slides/',if(:mobile = 'false', image_url_big, image_url_small)) as src
            from _slides
            where
                section = :section and
                lang = :lang and
                active = :active
            order by ord asc, id asc",
            array("section"=>$section,"lang"=>$lang, "active"=>1, "mobile"=>$mobile));

        return new Response(json_encode($list), 200);
        exit;
    })
    ->assert('lang', '^(pt|en)$')
    ->assert('mobile', '^(true|false)$')
    ->assert('section', '^(leisure|incentives)$');

    $app->get('/testimonials/{lang}/{mobile}/{section}/', function($lang, $mobile, $section) use($app) {
        $list = null;
        $list = R::getAll("SELECT id, title, url, concat('uploads/testimonials/',if(:mobile = 'false', image_url_big, image_url_small)) as src
            from _testimonials
            where
                section = :section and
                lang = :lang and
                active = :active
            order by ord asc, id asc",
            array("section"=>$section, "lang"=>$lang, "active"=>1, "mobile"=>$mobile));

        return new Response(json_encode($list), 200);
        exit;
    })
    ->assert('lang', '^(pt|en)$')
    ->assert('mobile', '^(true|false)$');

    $app->get('/partners/', function() use($app) {
        $list = null;
        $list = R::getAll("SELECT id, title, url, concat('uploads/partners/',image_url) as src from _partners
            where
                active = :active
            order by ord asc, id asc",
            array("active"=>1));

        return new Response(json_encode($list), 200);
        exit;
    });

    $app->get('/sportclients/{lang}/{mobile}/', function($lang, $mobile) use($app) {
        $list = null;
        $list = R::getAll("SELECT id, title, url, concat('uploads/sportclients/',if(:mobile = 'false', image_url_big, image_url_small)) as src
            from _sportclients
            where
                lang = :lang and
                active = :active
            order by ord asc, id asc",
            array("lang"=>$lang, "active"=>1, "mobile"=>$mobile));

        return new Response(json_encode($list), 200);
        exit;
    })
    ->assert('lang', '^(pt|en)$')
    ->assert('mobile', '^(true|false)$');


    $app->run();
}catch(Exception $e){
    dump($e);
}
