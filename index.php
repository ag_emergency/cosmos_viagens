<?
    $actualLink = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?><!DOCTYPE html>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="main">
<head>

	<!-- Meta Tags -->
    <meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta http-equiv="content-language" content="pt"/>
	<meta name="author" content="">
	<meta name="copyright" content="">
	<meta http-equiv="desription" content="A travel services company, specialist in sports tourism segment of high competition, business travel (Corporate Travel Management), corporate incentives and leisure. Click here to know more."/>
	<meta http-equiv="keywords" content=""/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Custom Meta Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<!-- Cache -->
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="COSMOS | Customized traveling experiences for you and your company">
    <meta itemprop="description" content="A travel services company, specialist in sports tourism segment of high competition, business travel (Corporate Travel Management), corporate incentives and leisure. Click here to know more.">
    <meta itemprop="image" content="<?=$actualLink?>app/public/img/share.jpg">
    <meta name="twitter:card" value="summary">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Cosmos" />
    <meta property="og:description" content="A travel services company, specialist in sports tourism segment of high competition, business travel (Corporate Travel Management), corporate incentives and leisure. Click here to know more." />
    <meta property="og:image" content="<?=$actualLink?>app/public/img/share.jpg" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:locale" content="pt_PT" />
    <meta property="og:url" content="<?=$actualLink?>"/>

	<!-- Title -->
	<title>COSMOS | Customized traveling experiences for you and your company</title>

	<!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="app/public/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="app/public/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="app/public/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="app/public/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="app/public/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="app/public/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="app/public/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="app/public/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="app/public/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="app/public/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="app/public/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="app/public/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="app/public/favicon/favicon-16x16.png">
    <link rel="manifest" href="app/public/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="app/public/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Styles -->
    <link async href="app/public/css/main.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

	<!--[if IE 9]>
		<script src="app/vendor/js/modernizr.custom.73251.js?v=0.0.5"></script>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Safari -->
	<meta name="apple-mobile-web-app-capable" content="yes">

	<!-- Prevent Indexing -->
	<!-- <meta name="robots" content="noindex">-->
	

</head>

<body class="shifter">

    <callme></callme>

    <div id="preloader"><span><img src="app/public/img/preloader.svg" /></span></div>

    <div class="shifter_wrapper">
    	<div class="shifter-page">

            <header header class=""></header>
    		<section ui-view id="{{ section.main }}" class="container">
                <div ui-view></div>
            </section>
    		<div footer></div>
    	</div>

    	<!--a href="#" class="gotop" data-scrollto="0">Voltar ao topo <span class="icon-seta_faqs_up"></span></a>-->

    	<div class="shifter-navigation">
    		<menu />
    	</div>
    </menu>

	<div id="fb-root"></div>

	<!-- <script async type="text/javascript" src="app/public/js/main.min.js"></script> -->

    <script type="text/javascript" src="sources/js/vendor/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="sources/js/vendor/jquery.scrolly.js"></script>
    <script type="text/javascript" src="sources/js/vendor/jquery.fs.shifter.js"></script>
    <script type="text/javascript" src="sources/js/vendor/slippry.min.js"></script>
    <script type="text/javascript" src="sources/js/vendor/slick.min.js"></script>
    <script type="text/javascript" src="sources/js/vendor/jquery.inview.js"></script>
    <script type="text/javascript" src="sources/js/vendor/nbw-parallax.js"></script>
    <script type="text/javascript" src="sources/js/vendor/jquery.cookiebar.js"></script>
    <script type="text/javascript" src="sources/js/vendor/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="app/libs/functions.js"></script>
    <script type="text/javascript" src="sources/js/ng/angular.min.js"></script>
    <script type="text/javascript" src="sources/js/ng/angular-locale_pt-pt.js"></script>
    <script type="text/javascript" src="sources/js/ng/angular-ui-router.js"></script>
    <script type="text/javascript" src="sources/js/ng/angular-translate.js"></script>
    <script type="text/javascript" src="sources/js/ng/angulartics.min.js"></script>
    <script type="text/javascript" src="sources/js/ng/angulartics-ga.min.js"></script>
    <script type="text/javascript" src="sources/js/ng/ngDialog.js"></script>
    <script type="text/javascript" src="app/config/app.js"></script>
    <script type="text/javascript" src="app/config/config.js"></script>
    <script type="text/javascript" src="app/config/constants.js"></script>
    <script type="text/javascript" src="app/config/run.js"></script>
    <script type="text/javascript" src="app/factories/preloaderF.js"></script>
    <script type="text/javascript" src="app/factories/imagesF.js"></script>
    <script type="text/javascript" src="app/directives/common.js"></script>
    <script type="text/javascript" src="app/directives/validators.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-45024536-1', 'auto');
        //ga('send', 'pageview');
    </script>
    <!--<script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-37751997-5']);
      _gaq.push(['_setDomainName', 'cosmos-viagens.pt']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script><script type="text/javascript">var goweb = jQuery.noConflict();</script>-->

</body>
</html>